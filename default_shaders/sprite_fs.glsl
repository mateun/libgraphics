#version 430 core

layout(binding = 0) uniform sampler2D sampler;

out vec4 color;
in vec2 fs_uvs;
in vec3 fs_normals;


void main() {
	color = texture(sampler, fs_uvs);
}
