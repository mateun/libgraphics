#include "mapeditor.h"
#include "../include/graphics.h"
#include "../Font.h"
#include "../gui.h"
#include "../texture_cache.h"
#include "../camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define PRIV static

PRIV VAOData gridVAOData;
PRIV Camera* editorMainCam;
PRIV Camera* camOrtho;
PRIV Font* consolasFont;
PRIV float orthoZoom = 1;
PRIV int cellSize = 32;
PRIV int pageCellsH = 20;
PRIV int pageCellsV = 12;
extern int getCellsH();
extern int getCellsV();

PRIV int getViewportHeight() {
	return getScreenHeight()-96;
}


PRIV void initCameras() {
	glm::mat4 vTopDown = glm::lookAt(glm::vec3(0, 10, 5), glm::vec3(0, 2, -1), glm::vec3(0, 1, 0));
	glm::mat4 pTopDown = glm::perspective(1.04f, (float)(getScreenWidth() / getScreenHeight()), 0.01f, 300.0f);

	glm::mat4 vOrtho = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	glm::mat4 pOrtho = glm::ortho<float>(0, getScreenWidth(), 0, getScreenHeight(), .01, 100);

	editorMainCam = new Camera(vTopDown, pTopDown);
	camOrtho = new Camera(vOrtho, pOrtho);

}
PRIV void setupGrid() {
	initCameras();

	consolasFont = new Font("consolas", "consolas-font");
	
	std::vector<float> vertices;
	std::vector<int> indices;

	pageCellsH = getCellsH();
	pageCellsV = getCellsV();


	int startH = -pageCellsH/2;
	int endH = pageCellsH/2 ;
	int startV = -pageCellsV/2;
	int endV = pageCellsV/2;

	int indexCounter = 0;
	for (int i = 0; i < (endH - startH); i++) {
		vertices.push_back(startH + i);
		vertices.push_back(startV);
		vertices.push_back(0);
		vertices.push_back(startH + i);
		vertices.push_back(endV);
		vertices.push_back(0);

		indices.push_back(indexCounter);
		indices.push_back(indexCounter + 1);
		indexCounter += 2;
	}

	gridVAOData = createVAO(vertices, {}, {}, indices);
}

PRIV void renderGrid() {
	RenderInfo ri;
	ri.vaodata = gridVAOData;
	ri.shader = getShaderCache()->get("default");
	ri.nrOfVertices = gridVAOData.number_of_indices;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = camOrtho->view();
	camOrtho->setProjectionMatrix(glm::ortho<float>(0 , (float) getScreenWidth() * orthoZoom, 0 , (float) getViewportHeight() * orthoZoom, .01, 400));
	ri.projectionMatrix = camOrtho->proj();
	ri.texture = 0;
	ri.fixColored = true;
	ri.alpha = 1;
	
	ri.drawMode = GL_LINES;

	glViewport(0, 32, 1024, getViewportHeight());


	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(getScreenWidth()/2 * orthoZoom, getScreenHeight()/2 * orthoZoom, 0));
	
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(cellSize, cellSize, 1));
	//ri.modelMatrix = glm::rotate(ri.modelMatrix, glm::pi<float>() / 2.0f, glm::vec3(0, 0, 1));
	ri.tint = { 0.7, 0, 0 };
	drawVAO(&ri);

	//ri.modelMatrix = glm::scale(glm::mat4(1), glm::vec3(cellSize / 2, cellSize / 2, 1));
	//ri.modelMatrix = glm::rotate(ri.modelMatrix, glm::pi<float>() / 2.0f, glm::vec3(0, 0, 1));
	//ri.tint = { 0.3, 0.1, 0.9 };
	//drawVAO(&ri);

	ri.modelMatrix = glm::translate(glm::mat4(1), glm::vec3(getScreenWidth() / 2* orthoZoom, getScreenHeight() / 2 * orthoZoom, 0));
	ri.modelMatrix = glm::rotate(ri.modelMatrix, glm::pi<float>() / 2.0f, glm::vec3(0, 0, 1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(cellSize, cellSize, 1));
	
	ri.tint = { 0.1, 0.4, 0.1 };
	drawVAO(&ri);

	/*glViewport(490, 32, 480, 320);
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(1, 1, 1));

	ri.alpha = 1;
	ri.tint = { 0.7, 0, 0 };
	ri.drawMode = GL_LINES;

	ri.viewMatrix = editorMainCam->view();
	ri.projectionMatrix = editorMainCam->proj();

	drawVAO(&ri);
	ri.modelMatrix = glm::rotate(glm::mat4(1), glm::pi<float>() / 2.0f, glm::vec3(0, 1, 0));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(1, 1, 1));
	ri.tint = { 0.3, 0.1, 0.1 };
	drawVAO(&ri);

	ri.modelMatrix = glm::rotate(glm::mat4(1), glm::pi<float>() / 2.0f, glm::vec3(0, 1, 0));
	ri.tint = { 0.1, 0.3, 0.1 };
	drawVAO(&ri);*/

	glViewport(0, 0, getScreenWidth(), getScreenHeight());
}

// Lower left corner is the origin.
void getCellCoordinate(int mx, int my, int& cellX, int& cellY) {
	cellX = (mx - getScreenWidth() / 2) / (cellSize/orthoZoom);
	cellY = (my - 32 - (getViewportHeight() + 96) /2)/ (cellSize/orthoZoom);
}

std::string getViewport(int mx, int my) {
	if (mx > 5 && mx < 481 && my > 32 && my < 362) {
		return "top";
	}
	else if (mx > 489 && mx < 489 + 490 && my > 32 && my < 362) {
		return "3D";
	}
	else
		return  "";
}

void renderToolBar() {
	if (doButton("btnEditorZoomIn", "ui-atlas", 330, getScreenHeight() - 32, 1 * 32, 1 * 32 , { 512, 512, 6 * 32, 7 * 32, 1 * 32, 1 * 32 })) {
		
	}

	if (doButton("btnEditorZoomOut", "ui-atlas", 390, getScreenHeight() - 32, 1 * 32, 1 * 32, { 512, 512, 7 * 32, 7 * 32, 1 * 32, 1 * 32 })) {
		
	}
}

void renderStatusBar() {
	int mx, my;
	mouseXY(mx, my);
	drawText2D(std::to_string(mx) + "/" + std::to_string(my), 5, 5, *consolasFont, 1);
	drawText2D(getViewport(mx, my), 70, 5, *consolasFont, 1);
	drawText2D("Zoom: " + std::to_string(orthoZoom), 110, 5, *consolasFont, 1);
	int cX, cY;
	getCellCoordinate(mx, my, cX, cY);
	drawText2D("Cell: " + std::to_string(cX) + "/" + std::to_string(cY), 180, 5, *consolasFont, 1);
}

void updateMapDetails(float dt, std::vector<SDL_Event>& frameEvents) {
	for (auto fe : frameEvents) {
		if (fe.type == SDL_MOUSEWHEEL) {
			if (fe.wheel.y > 0 && orthoZoom < 4) {
				orthoZoom += 0.25;
			}
			if (fe.wheel.y < 0 && orthoZoom > 0.25) {
				
				orthoZoom -= 0.25;
				
			}
		}

	}
}


void renderEditorDetails() {
	if (!editorMainCam) {
		setupGrid();
	}
	renderGrid();
	renderStatusBar();
	renderToolBar();
}

void cleanupMapDetails() {
	deleteVAOData(gridVAOData);
	if (editorMainCam) {
		delete(editorMainCam);
		editorMainCam = nullptr;
	}
	
}