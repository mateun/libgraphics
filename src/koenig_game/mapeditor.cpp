#include "mapeditor.h"
#include "../include/graphics.h"
#include "../Font.h"
#include "../gui.h"
#include "../texture_cache.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define PRIV static

PRIV std::string cellsH = "50";
PRIV std::string cellsV = "50";

int getCellsH() {
	return std::stoi(cellsH);
}


int getCellsV() {
	return std::stoi(cellsV);
}


PRIV enum class EditorMenuState {
	Closed,
	FileSubMenuOpen,
	AboutOpen

};

PRIV enum class EditorState {
	Empty,
	NewMapWizard,
	InMap

};

PRIV EditorMenuState menuState = EditorMenuState::Closed;
PRIV EditorState editorState = EditorState::Empty;

extern void renderEditorDetails();
extern void renderStatusBar();
extern void updateMapDetails(float dt, std::vector<SDL_Event>& frameEvents);
extern void cleanupMapDetails();

PRIV void renderNewMapWizard() {
	doText("lblCells", "Cells per page:", 10, getScreenHeight() - 128, -0.99f, { 0.9, 0.9, 0.9 });
	doTextInput("txtCellsH", &cellsH, 125, getScreenHeight() - 128, 100, 32);
	doTextInput("txtCellsV", &cellsV, 232, getScreenHeight() - 128, 100, 32);

	if (doMenu("menuSaveMapWizard", "Save", 400, getScreenHeight() - 275, 120, 32, -0.97f)) {
		editorState = EditorState::InMap;
	}

}

void renderFileSubMenu(std::function< void()> closeCallback) {
	if (doMenu("menuNewMap", "New Map", 10, getScreenHeight() - 64, 120, 32, -0.97f)) {
		menuState = EditorMenuState::Closed;
		editorState = EditorState::NewMapWizard;
		SDL_Log("set to newmapwizard");
	
	}

	if (doMenu("meunuSaveMap", "Save Map", 10, getScreenHeight() - 96, 120, 32, -0.97f)) {
		menuState = EditorMenuState::Closed;
	}

	if (doMenu("meunuExit", "Exit", 10, getScreenHeight() - 128, 120, 32, 1.97f)) {
		menuState = EditorMenuState::Closed;
		editorState = EditorState::Empty;
		cleanupMapDetails();
		closeCallback();
	}
}

void updateMapEditor(float dt, std::vector<SDL_Event>& frameEvents)
{
	updateMapDetails(dt, frameEvents);
}

void renderMapEditor(std::function< void()> closeCallback) {

	
	if (doMenu("menuEditorFile", "File", 0, getScreenHeight() - 32, 80, 32)) {
		if (menuState == EditorMenuState::FileSubMenuOpen) {
			SDL_Log("going closed menu state");
			menuState = EditorMenuState::Closed;
		}
		else {
			menuState = EditorMenuState::FileSubMenuOpen;
			SDL_Log("going new map sub menu state");
		}
	}
	
	if (editorState == EditorState::NewMapWizard) {
		renderNewMapWizard();
	}

	if (editorState == EditorState::InMap) {
		renderEditorDetails();
		
	}


	if (menuState == EditorMenuState::FileSubMenuOpen) {
		renderFileSubMenu(closeCallback);
	}
	
}


void cleanupMapEditor() {
	
}