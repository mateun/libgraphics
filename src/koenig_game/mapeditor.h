#pragma once
#include "koenig_global.h"
#include <functional>
#include <vector>
#include <SDL2/SDL.h>

void updateMapEditor(float dt, std::vector<SDL_Event>& frameEvents);
void renderMapEditor(std::function< void()>);
