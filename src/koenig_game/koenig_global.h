#pragma once



enum Koenig_GameState {
	GS_None,
	GS_Splash,
	GS_Menu,
	GS_InGame,
	GS_InHostingConfig,
	GS_WaitingForServerList,
	GS_InServerList,
	GS_GeneratingMap,
	GS_InLobby,
	GS_InNetworkedGame,
	GS_InLoginScreen,
	GS_InEditor,
	GS_InMiniGamesMenu,
	GS_InPiggle,

};

