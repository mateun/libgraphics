#pragma once
#include <stdlib.h>
#include <string>


enum TerrainType {
	Water,
	Land
};

struct Map {
	int width; 
	int height;
	TerrainType* terrainTypeLayer;
};

struct MapGenerationProgress {
	int maxSteps;
	int currentStep;
	std::string stepName;
	Map* finishedMap = nullptr;
};

MapGenerationProgress* pollMapGenerationProgress();

MapGenerationProgress* generateMap(int w, int h);

