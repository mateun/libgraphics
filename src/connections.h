#pragma once

#include "networking.h"

struct Client {
	uint16_t id;
	IPaddress address;
};

void serviceConnectionManager(UDPNode* server);
