#include "shader_cache.h"
#include <GL/glew.h>

// Our singleton shader cache instance
static ShaderCache* sc;

ShaderCache* getShaderCache() {
	if (!sc) {
		sc = new ShaderCache();
	}
	return sc;
}

GLuint ShaderCache::get(const std::string& name) {
	return _shaders[name];	

}


void ShaderCache::put(const std::string& name, GLuint shaderProg) {
	_shaders[name] = shaderProg;

}
