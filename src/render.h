#pragma once
#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <string>

class Font;
class Image;
class SDL_Rect;



struct VAOData {
	GLuint vb_pos;
	GLuint vb_uvs;
	GLuint vb_normals;
	GLuint vb_indices;
	GLuint number_of_indices;
	GLuint vao;
};

struct RenderInfo {
	VAOData vaodata;
	GLuint shader;
	GLuint texture;
	int nrOfVertices;
	int nrOfIndices;
	bool lit = false;
	bool fixColored = false;
	float alpha = 1;
	glm::vec3 tint = glm::vec3(0);
	glm::vec3 sunDirection = glm::vec3(-0.2, -0.5, -0.2);
	glm::mat4 modelMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
	glm::mat4 viewMatrixLight;
	glm::mat4 projectionMatrixLight;
	GLuint shadowMapTexture;
	GLenum drawMode = GL_TRIANGLES;

};


struct SpriteAtlasItemDescriptor {
	float atlasWidth;
	float atlasHeight;
	float itemX;
	float itemY;
	float itemW;
	float itemH;

	std::vector<float> getUVs() {
		float uvX = itemX / atlasWidth;
		float uvY = itemY / atlasHeight;
		float uvW = itemW / atlasWidth;
		float uvH = itemH / atlasHeight;

		return { uvX, uvY, uvX + uvW, uvY, uvX, uvY + uvH, uvX + uvW, uvY + uvH };
	}
};


void deleteVAOData(VAOData bufs);

VAOData getUnitQuadVAO();
VAOData getUnitQuadVAO(const std::vector<float> uvs);
VAOData getUnitQuadVAOInstance(const std::vector<float> uvs);
VAOData createVAO(const std::vector<float>& positions, const std::vector<float>& uvs, const std::vector<float>& normals, const std::vector<int>& indices);
GLuint createFramebuffer();
void drawVAO(const RenderInfo* renderInfo);
void drawText2D(const std::string& text, int x, int y, const Font& font, float scale = 1, glm::vec3 color = { 1, 1, 1 }, float depth = 1.0f);
void drawImage2D(SDL_Rect targetRect, const Image& image, SpriteAtlasItemDescriptor sad);
void clearBackBuffer(float r, float g, float b);
