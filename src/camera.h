#pragma once
#include <glm/glm.hpp>

class Camera {

public:
	Camera(glm::mat4 view, glm::mat4 proj);
	Camera(glm::vec3 position, glm::vec3 forward, glm::vec3 right, glm::mat4 proj);

	void updateTransform(glm::vec3 position, glm::vec3 forward, glm::vec3 right);
	void setViewMatrix(glm::mat4 v) { this->_viewMatrix = v; }
	void setProjectionMatrix(glm::mat4 p) { this->_projMatrix = p; }

	glm::mat4 view() const;
	glm::mat4 proj() const;
	glm::vec3 position() { return _position; }
	glm::vec3 up();
	glm::vec3 fwd() { return glm::normalize(this->_forward); }
	glm::vec3 right() { return glm::normalize(this->_right); }
	glm::vec3 rightCalculated() { return glm::cross(this->_forward, this->up()); }

private:
	glm::mat4 _viewMatrix;
	glm::mat4 _projMatrix;
	glm::vec3 _position;
	glm::vec3 _forward;
	glm::vec3 _right;

};

