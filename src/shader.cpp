#include "shader.h"
#include <GL/glew.h>
#include <string>
#include <fstream> 
#include <iostream> 
#include <sstream>
#include <vector>

static const std::string readCodeFromFile(const std::string& file) {
	std::ifstream inFile(file, std::ios::in);
	if (!inFile) {
		return 0;
	}

	std::ostringstream code;
	while (inFile.good()) {
		int c = inFile.get();
		if (!inFile.eof()) code << (char)c;
	}
	inFile.close();

	return code.str();
}


GLuint createShaderFromCode(const std::string& code, GLint type) {
    GLuint vshader = glCreateShader(type);
    const GLchar* vssource_char = code.c_str();

    glShaderSource(vshader, 1, &vssource_char, NULL);
    glCompileShader(vshader);
    GLint compileStatus;
    glGetShaderiv(vshader, GL_COMPILE_STATUS, &compileStatus);
    printf("shader compile status: %d", compileStatus);
    if (GL_FALSE == compileStatus) {
        printf("problem with shader!!");
        printf("shader source: %s", vssource_char);

        GLint logSize = 0;
        glGetShaderiv(vshader, GL_INFO_LOG_LENGTH, &logSize);
        std::vector<GLchar> errorLog(logSize);
        glGetShaderInfoLog(vshader, logSize, &logSize, &errorLog[0]);
        printf("error: %s", errorLog.data());
        glDeleteShader(vshader);

        exit(1);
    }

    return vshader;


}

GLuint createShader(const std::string& fileName, GLint type) {
	const std::string vsstring = readCodeFromFile(fileName);
    return createShaderFromCode(vsstring, type);
}

GLuint createVertexShader(const std::string& vsFileName) {
    return createShader(vsFileName, GL_VERTEX_SHADER);	
}

GLuint createFragmentShader(const std::string& fsFileName) {
	return createShader(fsFileName, GL_FRAGMENT_SHADER);	

}


GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
	GLuint p= glCreateProgram();
	glAttachShader(p, vertexShader);
	glAttachShader(p, fragmentShader);
	glLinkProgram(p);
	GLint compileStatus;
	glGetProgramiv(p, GL_LINK_STATUS, &compileStatus);

	if (GL_FALSE == compileStatus) {
		printf("problem with shaderlinking!!");

		GLint maxLength = 0;
		glGetProgramiv(p, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(p, maxLength, &maxLength, &infoLog[0]);

		printf("link error: %s", infoLog.data());

		// We don't need the program anymore.
		glDeleteProgram(p);
		// Don't leak shaders either.
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		exit(1);
	}
	
	GLenum err = glGetError();
	if (err != 0) {
		printf("error: %d", err);
		exit(1);
	}
	printf("linkstatus: %d", compileStatus);
	return p;

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

}

GLuint createVertexShaderFromCode(const std::string &vsCode) {
    return createShaderFromCode(vsCode, GL_VERTEX_SHADER);
}

GLuint createFragmentShaderFromCode(const std::string &psCode) {
    return createShaderFromCode(psCode, GL_FRAGMENT_SHADER);
}
