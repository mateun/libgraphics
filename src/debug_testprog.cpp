#include "../include/graphics.h"
#include "stdio.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "math.h"
#include <vector>
#include "game_object.h"
#include "texture_cache.h"
#include "camera.h"
#include "ImageImporter.h"
#include "Font.h"

#define PRIV static

PRIV const int ScreenWidth = 800;
PRIV const int ScreenHeight = 600;

PRIV Font* notoFont = nullptr;
PRIV Font* consolasFont = nullptr;
PRIV ShaderCache* shaderCache = nullptr;
PRIV std::vector<GameObject*>* gameObjects = nullptr;
PRIV GLuint shadowFbo;
PRIV Camera* topDownCamera = nullptr;
PRIV Camera* shadowMapCamera = nullptr;
PRIV float camPanX = 0.0f;
PRIV float camPanZ = 0.0f;
PRIV float camPanY = 20.0f;
PRIV float camYaw = 0.0f;
PRIV float camPitch = 0.0f;
PRIV float camRoll = 0.0f;
PRIV bool debugMode = false;



PRIV bool showDebugGui() {
	return true;
}


PRIV void initFramebuffers() {
	shadowFbo = createFramebuffer();
	GLuint shadowDepthTex = createDepthTextureInMem(1024, 1024);
	(*getTextureCache())["shadowDepth"] = shadowDepthTex;
	attachDepthTextureToFramebuffer(shadowFbo, shadowDepthTex);
}

PRIV void initShaders() {
	GLuint vs = createVertexShader("../test_shaders/vs.glsl");
	GLuint fs = createFragmentShader("../test_shaders/fs.glsl");
	GLuint shaderProg = createShaderProgram(vs, fs);

	GLuint shadowmap_vs = createVertexShader("../test_shaders/shadowmap-vs.glsl");
	GLuint shadowmap_fs = createFragmentShader("../test_shaders/shadowmap-fs.glsl");
	GLuint shadowMapshaderProg = createShaderProgram(shadowmap_vs, shadowmap_fs);

	shaderCache = getShaderCache();
	shaderCache->put("default", shaderProg);
	shaderCache->put("shadowmap", shadowMapshaderProg);
}

PRIV void initCameras() {
	glm::mat4 vTopDown = glm::lookAt(glm::vec3(-17.7, 20, 1), glm::vec3(-17.7, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pTopDown = glm::perspective(1.65f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);
	
	// Matrices for rendering the shadow map.
	// We use an orthographic projection to simulate sunlight with parallel rays.
	glm::mat4 vShadowMap = glm::lookAt(glm::vec3(200, 900, 300), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pShadowMap = glm::ortho<float>(-90, 90, -90, 90, 0.1, 1000);

	topDownCamera = new Camera(vTopDown, pTopDown);
	shadowMapCamera = new Camera(vShadowMap, pShadowMap);
}

PRIV void initTextures() {
	GLuint stoneTexture = createTexture("../test_assets/generic_texture.bmp");
	(*getTextureCache())["stone"] = stoneTexture;

	GLuint redTexture = createTexture("../test_assets/red.bmp");
	(*getTextureCache())["red"] = redTexture;

	GLuint blueTexture = createTexture("../test_assets/blue.bmp");
	(*getTextureCache())["blue"] = blueTexture;

	ImageData* notoSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/noto.png", notoSurface);
	GLuint notoTexture = createTextureFromSurface(notoSurface, true);
	(*getTextureCache())["noto-font"] = notoTexture;
	notoFont = new Font("noto", "noto-font");
	delete(notoSurface->pixels);
	delete(notoSurface);

	ImageData* consolasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/consolas.png", consolasSurface);
	GLuint consolasTexture = createTextureFromSurface(consolasSurface, true);
	(*getTextureCache())["consolas-font"] = consolasTexture;
	consolasFont = new Font("consolas", "consolas-font");
	delete(consolasSurface->pixels);
	delete(consolasSurface);

	ImageData* spriteSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/sprite.png", spriteSurface);
	GLuint sprite = createTextureFromSurface(spriteSurface, true);
	(*getTextureCache())["sprite"] = sprite;
	delete(spriteSurface->pixels);
	delete(spriteSurface);

	ImageData* engineSplashSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/piglet-engine-splash.png", engineSplashSurf);
	GLuint engineSplash = createTextureFromSurface(engineSplashSurf, true);
	(*getTextureCache())["engine-splash"] = engineSplash;
	delete(engineSplashSurf->pixels);
	delete(engineSplashSurf);
}

PRIV void init() {
	
	initTextures();

	Mesh* am = importToVAO("../test_assets/arena.obj");
	Mesh* tm = importToVAO("../test_assets/tank1.obj");

	initShaders();

	
	GameObject *enemy1 = new GameObject(0, "go", glm::vec3(-1, 3, 4), tm);
	GameObject *enemy2 = new GameObject(0, "go", glm::vec3(-8, 3, 0), tm);
	GameObject *arena = new GameObject(0, "go", glm::vec3(0, 0, 0), am);
	GameObject* arenaLeft = new GameObject(0, "go", glm::vec3(-10, 0, 0), am);
	GameObject* arenaTop = new GameObject(0, "go", glm::vec3(0, 0, -10), am);

	enemy1->_textureName = "red";
	enemy2->_textureName = "blue";
	arena->_textureName = "stone";
	arenaLeft->_textureName = "stone";
	arenaTop->_textureName = "stone";

	//enemy1->addUpdateComponent(new RotatorThingie());
	//enemy2->addUpdateComponent(new RotatorThingie());
	//ground->addUpdateComponent(new RotatorThingie());

	
	arena->scale(glm::vec3(4));
	arenaLeft->scale(glm::vec3(4));
	arenaTop->scale(glm::vec3(4));

	gameObjects = new std::vector<GameObject*>();
	gameObjects->push_back(enemy1);
	gameObjects->push_back(enemy2);
	gameObjects->push_back(arena);
	gameObjects->push_back(arenaLeft);
	gameObjects->push_back(arenaTop);

	initFramebuffers();

	initCameras();

}

PRIV void moveDebugCamera() {
	const Uint8* state = SDL_GetKeyboardState(nullptr);
	if (state[SDL_SCANCODE_RIGHT]) {
		camPanX += 0.1f;
	}

	if (state[SDL_SCANCODE_LEFT]) {
		camPanX -= 0.1f;
	}

	if (state[SDL_SCANCODE_UP]) {
		camPanZ += 0.1f;
	}

	if (state[SDL_SCANCODE_DOWN]) {
		camPanZ -= 0.1f;
	}

	if (state[SDL_SCANCODE_KP_PLUS]) {
		camPanY += 0.1f;
	}

	if (state[SDL_SCANCODE_KP_MINUS]) {
		camPanY -= 0.1f;
	}

	for (auto gameObject : *gameObjects) {
		gameObject->update(0.16f);
	}

}

PRIV void update(float dt, std::vector<SDL_Event>& frameEvents) {

	for (auto fe : frameEvents) {
		if (fe.type == SDL_KEYDOWN) {
			// if F12 is pressed, we toggle the debug mode
			SDL_Keymod mod = SDL_GetModState();
				if (mod & KMOD_RCTRL && fe.key.keysym.sym == SDLK_0) {
					debugMode = !debugMode;
				}
			
		}
	}

	if (debugMode) {
		moveDebugCamera();
	}

}

PRIV void renderEngineSplash(RenderInfo ri) {
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(400, 300, -1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(800, 600, 1));

	drawVAO(&ri);

}

PRIV void render2D(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	// To render in 2D we can use the default shader, 
	// which is the same we use for 3D rendering.
	// In fact we just need to change the projection matrix
	// for an orthographic projection.
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(-1, 1, -1, 1, .01, 100);
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;

	// We need transparency for text and sprite rendering
	glEnable(GL_BLEND);

	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["sprite"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(64, 100, -3));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(128, 128, 1));
	
	drawVAO(&ri);

	ri.texture = (*getTextureCache())["consolas-font"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(100, 100, -2));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(256));
	
	drawVAO(&ri);
	glDisable(GL_BLEND);

	
	if (showDebugGui()) {
		drawText2D("ft gross:" + std::to_string((double)frameTimeGross), 580, 500, *consolasFont, 1);
		drawText2D("ft net:" + std::to_string((double)frameTimeNet), 580, 520, *consolasFont, 1);
	}
	
	//renderEngineSplash(ri);

}

PRIV void render3D() {
	// Setup some basic view and projection matrices which we use in
		// different cameras.
	//glm::mat4 vTop = glm::lookAt(glm::vec3(0, 50, 7), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//glm::mat4 vFps = glm::lookAt(glm::vec3(0, 1, 47), glm::vec3(0, 1, -1), glm::vec3(0, 1, 0));
	//glm::mat4 pTop = glm::perspective(1.65f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);
	//glm::mat4 pFps = glm::perspective(1.35f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);

	//// Matrices for rendering the shadow map.
	//// We use an orthographic projection to simulate sunlight with parallel rays.
	//glm::mat4 vShadowMap = glm::lookAt(glm::vec3(-100, 200, 100), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//glm::mat4 pShadowMap = glm::ortho<float>(-70, 70, -70, 70, 0.1, 1000);

	//// TODO: remove this
	//const Camera* topDownCamera = new Camera(vTop, pTop);
	//const Camera* shadowMapCamera = new Camera(vShadowMap, pShadowMap);

	glm::mat4 vTop = glm::lookAt(glm::vec3(camPanX, 10 + camPanY, camPanZ), glm::vec3(camPanX, 0, camPanZ - 0.01), glm::vec3(0, 1, 0));
	//topDownCamera->_viewMatrix = vTop;

	// Render our shadow map
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFbo);
	glViewport(0, 0, 1024, 1024);
	GLfloat depthClear = 1;
	glClearBufferfv(GL_DEPTH, 0, &depthClear);
	for (auto gameObject : *gameObjects) {
		gameObject->render(shadowMapCamera, shadowMapCamera, "shadowmap", glm::vec3(0), false);
	}

	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.05, 0.01, 0.01);
	for (auto gameObject : *gameObjects) {
		gameObject->render(topDownCamera, shadowMapCamera, glm::vec3(0), false);
	}
}

PRIV void render(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.07, 0.03, 0.03);
	render3D();
	render2D(frameTimeGross, frameTimeNet);
}

PRIV int main(int argc, char** args) {

	createWindow(ScreenWidth, ScreenHeight, false);

	init();

	SDL_Event event;
	bool gameRunning = true;
	std::vector<SDL_Event> frameEvents;
	
	Uint64 frameTimeGross = 0;
	Uint64 frameTimeNet = 0;
	Uint64 startCounterGross = 0;
	while (gameRunning) {
		Uint64 startCounterNet = SDL_GetPerformanceCounter();
		frameEvents.clear();
		SDL_Event event;
		
		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				gameRunning = false;
				break;
			}


			if (event.type == SDL_KEYDOWN) {
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					gameRunning = false;
					break;
				}
			}

			frameEvents.push_back(event);
		}

		
		update(frameTimeGross, frameEvents);
		Uint64 renderTimeStart = SDL_GetPerformanceCounter();
		render(frameTimeGross, frameTimeNet);
		Uint64 renderTimeMicros = ((double)(SDL_GetPerformanceCounter() - renderTimeStart) / SDL_GetPerformanceFrequency()) * 1000 * 1000;
		frameTimeNet = ((double)(SDL_GetPerformanceCounter() - startCounterNet) / SDL_GetPerformanceFrequency()) * 1000 * 1000;
		
		
		flipBuffer();
		frameTimeGross = ((double)(SDL_GetPerformanceCounter() - startCounterGross) / SDL_GetPerformanceFrequency()) * 1000 * 1000;
		startCounterGross = SDL_GetPerformanceCounter();
	}

	return 0;
}


