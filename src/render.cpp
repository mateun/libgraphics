#include "render.h"
#include <stdio.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

static VAOData unitQuadVAO = {};

VAOData getUnitQuadVAOInstance(const std::vector<float> uvs) {
	
	const std::vector<float> vertices = {
		 -0.5, -0.5, 0,
		 0.5, -0.5, 0,
		 -0.5, 0.5,0,
		 0.5, .5, 0

	};

	const std::vector<float> normals = {
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1
	};

	const std::vector<int> indices = {
		0, 1, 2,
		2, 1, 3
	};

	VAOData unitQuadVAOInstance = createVAO(vertices,
		uvs, normals, indices);

	return unitQuadVAOInstance;
}


VAOData getUnitQuadVAO(const std::vector<float> uvs) {
	if (unitQuadVAO.vao != 0) {
		return unitQuadVAO;
	}

	const std::vector<float> vertices = {
		 -0.5, -0.5, 0,
		 0.5, -0.5, 0,
		 -0.5, 0.5,0,
		 0.5, .5, 0

	};

	const std::vector<float> normals = {
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1
	};

	const std::vector<int> indices = {
		0, 1, 2,
		2, 1, 3
	};

	unitQuadVAO = createVAO(vertices,
		uvs, normals, indices);

	return unitQuadVAO;
}

VAOData getUnitQuadVAO() {
	const std::vector<float> uvs = {
		 0, 0,
		 1, 0,
		 0, 1,
		 1, 1
	};

	return getUnitQuadVAO(uvs);
}

GLuint createFramebuffer() {
	GLuint fb;
	glGenFramebuffers(1, &fb);
	return fb;
	
}

void drawVAO(const RenderInfo* renderInfo) {
	GLuint glError = 0;
	
	if ((glError = glGetError()) != 0)
	{
		printf("gl error0: %d", glError);
	}

	
	glUseProgram(renderInfo->shader);

	glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, renderInfo->texture);

    if ((glError = glGetError()) != 0)
    {
        printf("gl error001.0: %d\n", glError);
        exit(1);
    }

    if (renderInfo->shadowMapTexture != 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, renderInfo->shadowMapTexture);

        if ((glError = glGetError()) != 0)
        {
            printf("gl error01.0: %d\n", glError);
            exit(1);
        }
    }


	GLuint modelIndex = glGetUniformLocation(renderInfo->shader, "mat_model");
	GLuint viewIndex = glGetUniformLocation(renderInfo->shader, "mat_view");
	GLuint projectIndex = glGetUniformLocation(renderInfo->shader, "mat_proj");
	GLuint isLitIndex = glGetUniformLocation(renderInfo->shader, "isLit");
	GLuint isFixColoredIndex = glGetUniformLocation(renderInfo->shader, "isFixColored");
	GLuint alphaIndex = glGetUniformLocation(renderInfo->shader, "alpha");
	GLuint tintIndex = glGetUniformLocation(renderInfo->shader, "tint");
	GLuint sunDirectionIndex = glGetUniformLocation(renderInfo->shader, "sunDirection");
    GLuint viewIndexLight = 0;
    GLuint projectIndexLight = 0;
    if (renderInfo->shadowMapTexture != 0) {
        viewIndexLight = glGetUniformLocation(renderInfo->shader, "mat_view_light");
        projectIndexLight = glGetUniformLocation(renderInfo->shader, "mat_proj_light");
    }

    if ((glError = glGetError()) != 0)
    {
        printf("gl error1.0: %d\n", glError);
        exit(1);
    }
	
	glUniformMatrix4fv(modelIndex, 1, false, glm::value_ptr(renderInfo->modelMatrix));
	glUniformMatrix4fv(viewIndex, 1, false, glm::value_ptr(renderInfo->viewMatrix));
	glUniformMatrix4fv(projectIndex, 1, false, glm::value_ptr(renderInfo->projectionMatrix));
	if (renderInfo->shadowMapTexture != 0) {
        glUniformMatrix4fv(viewIndexLight, 1, false, glm::value_ptr(renderInfo->viewMatrixLight));
        glUniformMatrix4fv(projectIndexLight, 1, false, glm::value_ptr(renderInfo->projectionMatrixLight));
	}

	const GLint lit = renderInfo->lit ? 1 : 0;
	glUniform1iv(isLitIndex, 1, &lit);
	const GLint fixColored = renderInfo->fixColored ? 1 : 0;
	glUniform1iv(isFixColoredIndex, 1, &fixColored);
	glUniform1fv(alphaIndex, 1, &renderInfo->alpha);
	glUniform3fv(tintIndex, 1, glm::value_ptr(renderInfo->tint));
	glUniform3fv(sunDirectionIndex, 1, glm::value_ptr(renderInfo->sunDirection));


	if ((glError = glGetError()) != 0)
	{
		printf("gl error1: %d\n", glError);
		exit(1);
	}

	
	glBindVertexArray(renderInfo->vaodata.vao);

	if ((glError = glGetError()) != 0)
	{
		printf("gl error2: %d\n", glError);
		exit(1);
	}
	
	glDrawElements(renderInfo->drawMode, renderInfo->nrOfVertices, GL_UNSIGNED_INT, (void*)0);
	
	if((glError = glGetError()) != 0)
	{
		printf("gl error3: %d", glError);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}


void deleteVAOData(VAOData bufs) {
	glDeleteBuffers(1, &bufs.vb_pos);
	glDeleteBuffers(1, &bufs.vb_uvs);
	glDeleteBuffers(1, &bufs.vb_normals);
	glDeleteBuffers(1, &bufs.vb_indices);
	glDeleteVertexArrays(1, &bufs.vao);
}


VAOData createVAO(const std::vector<float>& positions, const std::vector<float>& uvs, const std::vector<float>& normals, const std::vector<int>& indices) {
	VAOData vd;

	glCreateVertexArrays(1, &vd.vao);
	glBindVertexArray(vd.vao);

	glGenBuffers(1, &vd.vb_pos);
	glBindBuffer(GL_ARRAY_BUFFER, vd.vb_pos);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(GL_FLOAT), positions.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
	glEnableVertexAttribArray(0);

	if (!uvs.empty()) {
		glGenBuffers(1, &vd.vb_uvs);
		glBindBuffer(GL_ARRAY_BUFFER, vd.vb_uvs);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(GL_FLOAT), uvs.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
		glEnableVertexAttribArray(1);
	}

	if (!normals.empty()) {
		glGenBuffers(1, &vd.vb_normals);
		glBindBuffer(GL_ARRAY_BUFFER, vd.vb_normals);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GL_FLOAT), normals.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
		glEnableVertexAttribArray(2);

	}


	glGenBuffers(1, &vd.vb_indices);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vd.vb_indices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GL_UNSIGNED_INT), indices.data(), GL_STATIC_DRAW);
	vd.number_of_indices = indices.size();

	GLuint glError = 0;

	if ((glError = glGetError()) != 0)
	{
		printf("gl error0: %d\n", glError);
		exit(1);
	}

	return vd;

}

/*
GLuint createVAO(const std::vector<float>& positions, const std::vector<float>& uvs, const std::vector<float>& normals, const std::vector<int>& indices) {

	GLuint vao;
	glCreateVertexArrays(1, &vao);
	glBindVertexArray(vao);

	
	GLuint vb_pos;
	glGenBuffers(1, &vb_pos);
	glBindBuffer(GL_ARRAY_BUFFER, vb_pos);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(GL_FLOAT), positions.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
	glEnableVertexAttribArray(0);

	GLuint vb_uvs;
	glGenBuffers(1, &vb_uvs);
	glBindBuffer(GL_ARRAY_BUFFER, vb_uvs);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(GL_FLOAT), uvs.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
	glEnableVertexAttribArray(1);

	GLuint vb_normals;
	glGenBuffers(1, &vb_normals);
	glBindBuffer(GL_ARRAY_BUFFER, vb_normals);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GL_FLOAT), normals.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)nullptr);
	glEnableVertexAttribArray(2);

	GLuint vb_indices;
	glGenBuffers(1, &vb_indices);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vb_indices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GL_UNSIGNED_INT), indices.data(), GL_STATIC_DRAW);
	
	GLuint glError = 0;

	if ((glError = glGetError()) != 0)
	{
		printf("gl error0: %d\n", glError);
		exit(1);
	}
	
	return vao;

}*/


void clearBackBuffer(float r, float g, float b) {
	glClearColor(r, g, b, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


