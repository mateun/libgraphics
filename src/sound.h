#pragma once
#include <dsound.h>

struct Sound {
	LPDIRECTSOUNDBUFFER buf;
};

class SoundMachine {

public:
	SoundMachine(HWND hwnd);
	~SoundMachine();
	Sound* createDummySound(float duration, int freq);
	void playSound(Sound*, bool loop=false);

private:
	LPDIRECTSOUND8 dsound8;
	LPDIRECTSOUNDBUFFER sbPrimary;
};
