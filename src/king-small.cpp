#include "../include/graphics.h"
#include "stdio.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "math.h"
#include <vector>
#include "game_object.h"
#include "texture_cache.h"
#include "camera.h"
#include "ImageImporter.h"
#include "Font.h"
#include "gui.h"
#include <set>
#include "game_lib.h"
#include "scene_management.h"
#include "sound.h"
#include <tgmath.h>
#include <algorithm>

#define PRIV static
#define MeshCache (*getMeshCache())


PRIV enum Koenig_GameState {
	GS_None,
	GS_Splash,
	GS_Menu,
	GS_InGame
};



PRIV int ScreenWidth = 1024;
PRIV int ScreenHeight = 768;

PRIV SoundMachine* soundMachine = nullptr;
PRIV Sound* shotSound = nullptr;
PRIV Sound* deepSound = nullptr;
PRIV Font* notoFont = nullptr;
PRIV Font* consolasFont = nullptr;
PRIV ShaderCache* shaderCache = nullptr;
PRIV GLuint fbo;
PRIV GLuint shadowFbo;
PRIV GLuint pickMapFbo;
PRIV bool debugMode = false;
PRIV int frames = 0;
PRIV Koenig_GameState gameState = GS_None;
PRIV bool gameRunning = true;
PRIV Timer timer_render;
PRIV SpriteAtlasItemDescriptor waterTileDesc = {};
PRIV SpriteAtlasItemDescriptor grassTileDesc = {};
PRIV std::vector<GameObject*> gameObjects;
PRIV Camera* topDownCamera = nullptr;
PRIV Camera* shadowMapCamera = nullptr;
PRIV Camera* debugCamera = nullptr;
PRIV Camera* activeMainCam = nullptr;
PRIV glm::vec3 sunDirection = glm::vec3(-0.5, -0.6, -.3);

PRIV bool showDebugGui() {
	return true;
}

PRIV void initGameObjects() {
	GameObject* drone_body_gm = new GameObject(1, "droneBody", glm::vec3(-1, 3, 4), MeshCache["droneBody"]);
	drone_body_gm->_textureName = "lp-palette";
	drone_body_gm->scale(glm::vec3(1, 1, 1));
	drone_body_gm->setPosition({ 0, 0, 0 });
	gameObjects.push_back(drone_body_gm);

}

PRIV void initShaders() {
	GLuint vs = createVertexShader("../test_shaders/vs.glsl");
	GLuint fs = createFragmentShader("../test_shaders/fs.glsl");
	GLuint shaderProg = createShaderProgram(vs, fs);

	GLuint shadowmap_vs = createVertexShader("../test_shaders/shadowmap-vs.glsl");
	GLuint shadowmap_fs = createFragmentShader("../test_shaders/shadowmap-fs.glsl");
	GLuint shadowMapshaderProg = createShaderProgram(shadowmap_vs, shadowmap_fs);

	GLuint fsText = createFragmentShader("../test_shaders/fs-text.glsl");
	GLuint textRenderShaderProg = createShaderProgram(vs, fsText);

	shaderCache = getShaderCache();
	shaderCache->put("default", shaderProg);
	shaderCache->put("shadowmap", shadowMapshaderProg);
	shaderCache->put("text", textRenderShaderProg);
}

PRIV void initCameras() {
	glm::mat4 vTopDown = glm::lookAt(glm::vec3(0, 30, 0), glm::vec3(0, 2, 0), glm::vec3(0, 1, 0));
	glm::mat4 pTopDown = glm::perspective(1.04f, (float)(ScreenWidth / ScreenHeight), 0.01f, 300.0f);

	glm::mat4 vDebugCam = glm::lookAt(glm::vec3(0, 1.8f, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pDebugCam = glm::perspective(1.04f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);

	// Matrices for rendering the shadow map.
	// We use an orthographic projection to simulate sunlight with parallel rays.
	glm::mat4 vShadowMap = glm::lookAt(sunDirection * -20.0f, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pShadowMap = glm::ortho<float>(-65, 65, -65, 65, 0.1, 1000);

	//topDownCamera = new Camera(vTopDown, pTopDown);
	topDownCamera = new Camera(glm::vec3(0, 35, 0), glm::vec3(0, -1, 0), { 1, 0,0 }, pTopDown);
	shadowMapCamera = new Camera(vShadowMap, pShadowMap);
	debugCamera = new Camera(vDebugCam, pDebugCam);

	activeMainCam = topDownCamera;
}

PRIV void initTextures() {
	GLuint stoneTexture = createTexture("../test_assets/generic_texture.bmp");
	(*getTextureCache())["stone"] = stoneTexture;

	GLuint redTexture = createTexture("../test_assets/red.bmp");
	(*getTextureCache())["red"] = redTexture;

	GLuint blueTexture = createTexture("../test_assets/blue.bmp");
	(*getTextureCache())["blue"] = blueTexture;

	ImageData* notoSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/noto.png", notoSurface);
	GLuint notoTexture = createTextureFromSurface(notoSurface, true);
	(*getTextureCache())["noto-font"] = notoTexture;
	notoFont = new Font("noto", "noto-font");
	delete(notoSurface->pixels);
	delete(notoSurface);

	ImageData* consolasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/consolas.png", consolasSurface);
	GLuint consolasTexture = createTextureFromSurface(consolasSurface, true);
	(*getTextureCache())["consolas-font"] = consolasTexture;
	consolasFont = new Font("consolas", "consolas-font");
	delete(consolasSurface->pixels);
	delete(consolasSurface);

	ImageData* spriteSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/sprite.png", spriteSurface);
	GLuint sprite = createTextureFromSurface(spriteSurface, true);
	(*getTextureCache())["sprite"] = sprite;
	delete(spriteSurface->pixels);
	delete(spriteSurface);

	ImageData* engineSplashSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/piglet-engine-splash.png", engineSplashSurf);
	GLuint engineSplash = createTextureFromSurface(engineSplashSurf, true);
	(*getTextureCache())["engine-splash"] = engineSplash;
	delete(engineSplashSurf->pixels);
	delete(engineSplashSurf);

	ImageData* mainMenuSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/drone_wars_title.png", mainMenuSurface);
	GLuint mainMenuTexture = createTextureFromSurface(mainMenuSurface, true);
	(*getTextureCache())["title"] = mainMenuTexture;
	delete(mainMenuSurface->pixels);
	delete(mainMenuSurface);

	ImageData* uiAtlasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/ui-sprite-atlas.png", uiAtlasSurface);
	GLuint uiAtlasTexture = createTextureFromSurface(uiAtlasSurface, true);
	(*getTextureCache())["ui-atlas"] = uiAtlasTexture;
	delete(uiAtlasSurface->pixels);
	delete(uiAtlasSurface);

	ImageData* lpPalSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/lp-palette.png", lpPalSurface);
	GLuint lpPalTexture = createTextureFromSurface(lpPalSurface, true);
	(*getTextureCache())["lp-palette"] = lpPalTexture;
	delete(lpPalSurface->pixels);
	delete(lpPalSurface);


	ImageData* testFigureSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/figure_diffuse.png", testFigureSurface);
	GLuint fdTexture = createTextureFromSurface(testFigureSurface, true);
	(*getTextureCache())["testfigure-diffuse"] = fdTexture;
	delete(testFigureSurface->pixels);
	delete(testFigureSurface);

	ImageData* greenSemiSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/green_semitransparent.png", greenSemiSurf);
	GLuint greenSemiTexture = createTextureFromSurface(greenSemiSurf, true);
	(*getTextureCache())["green-semitransparent"] = greenSemiTexture;
	delete(greenSemiSurf->pixels);
	delete(greenSemiSurf);

	ImageData* kingSpritesSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/king_sprite_sheet.png", kingSpritesSurface);
	GLuint kingSpritesTexture = createTextureFromSurface(kingSpritesSurface, true);
	(*getTextureCache())["king-sprites"] = kingSpritesTexture;
	delete(kingSpritesSurface->pixels);
	delete(kingSpritesSurface);

	waterTileDesc = { 256, 256, 0, 7 * 32, 32, 32 };
	grassTileDesc = { 256, 256, 32, 7 * 32, 32, 32 };


}


PRIV void initFrameBuffers() {
	fbo = createFramebuffer();
	GLuint ct = createColorTextureInMem(512, 512);
	GLuint dt = createDepthTextureInMem(512, 512);
	(*getTextureCache())["offscreen"] = ct;
	attachColorAndDepthTexturesToFramebuffer(fbo, ct, dt);


	shadowFbo = createFramebuffer();
	GLuint shadowDepthTex = createDepthTextureInMem(1024, 1024);
	(*getTextureCache())["shadowDepth"] = shadowDepthTex;
	attachDepthTextureToFramebuffer(shadowFbo, shadowDepthTex);

	pickMapFbo = createFramebuffer();
	GLuint pickmapColTex = createColorTextureInMem(ScreenWidth, ScreenHeight);
	GLuint pickmapDepthText = createDepthTextureInMem(ScreenWidth, ScreenHeight);
	(*getTextureCache())["pickMapCol"] = pickmapColTex;
	attachColorAndDepthTexturesToFramebuffer(pickMapFbo, pickmapColTex, pickmapDepthText);
}

PRIV void init() {

	Mesh* droneBodyMesh = importToVAO("../test_assets/drone_body.obj");

	MeshCache["droneBody"] = droneBodyMesh;
	
	initFrameBuffers();
	initTextures();
	initShaders();
	initCameras();
	initGameObjects();
	

}


PRIV void update(float dt, std::vector<SDL_Event>& frameEvents) {
	for (auto fe : frameEvents) {
		if (fe.type == SDL_KEYDOWN) {
			// if F10 is pressed, we toggle the debug mode

			// Not used now: listen for CTRL-0
			// using the key modifiers.
			//SDL_Keymod mod = SDL_GetModState();
			//if (mod & KMOD_RCTRL && fe.key.keysym.sym == SDLK_0) {
			if (fe.key.keysym.sym == SDLK_F10) {
				debugMode = !debugMode;
				//activeMainCam = debugCamera;
			}
		}

		if (fe.type == SDL_MOUSEBUTTONDOWN) {
			if (fe.button.button == SDL_BUTTON_LEFT) {

			}
			else if (fe.button.button == SDL_BUTTON_RIGHT) {

			}

		}
	}
}



PRIV double splashTimePassed = 0;
PRIV double splashTimeDuration = 2;
PRIV float splashAlpha = 1;
PRIV void renderEngineSplash(Uint64 frameTimeGross) {
	// We just wait some frames to warm up
	if (frames < 10) {
		return;
	}
	double ftInSeconds = (double)frameTimeGross / 1000 / 1000;

	(double)splashTimePassed += ftInSeconds;
	if (splashTimePassed >= splashTimeDuration) {
		gameState = Koenig_GameState::GS_Menu;
		return;
	}

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(400, 300, -1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(800, 600, 1));
	splashAlpha -= 0.2 * ftInSeconds;
	ri.alpha = splashAlpha;

	drawVAO(&ri);

}

PRIV void renderMainMenu() {

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["title"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(512, 384, -2));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(1024, 768, 1));
	ri.alpha = 1;

	drawVAO(&ri);

	if (doButton("btnStart", "ui-atlas", 300, 250, 3 * 64, 1 * 64, { 512, 512, 0, 7 * 64, 3 * 64, 1 * 64 })) {
		gameState = Koenig_GameState::GS_InGame;
	}

	if (doButton("btnExit", "ui-atlas", 300, 180, 3 * 64, 1 * 64, { 512, 512, 3 * 64, 7 * 64, 3 * 64, 1 * 64 }) == true) {
		gameRunning = false;
	}

}

PRIV void renderPickMap() {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);

	// For this test we use a texture called offsreen
	// which is an in memory texture used as a render target for 
	// offscreen rendering, hence the name :) 
	ri.texture = (*getTextureCache())["pickMapCol"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(100, 50, 1.5f));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(300, 200, 1));

	ri.lit = false;
	ri.alpha = 1;
	drawVAO(&ri);
}



PRIV void renderDebugInfo(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	if (showDebugGui()) {

		// frame statistics
		drawText2D("ft gross:" + std::to_string((double)frameTimeGross), 5, ScreenHeight - 30, *consolasFont, 1, { 0, 0.7, 0 });
		drawText2D("ft render:" + std::to_string((double)timer_render.get_last_measure(microseconds)), 5, ScreenHeight - 15, *consolasFont, 1);
		drawText2D("ft net:" + std::to_string((double)frameTimeNet), 5, ScreenHeight - 45, *consolasFont, 1);

		// draw mouse coords
		int mx, my;
		mouseXY(mx, my);
		drawText2D(std::to_string(mx) + "/" + std::to_string(my), 5, ScreenHeight - 60, *consolasFont, 1);

		// selected game object
		//std::string goName = lastSelectedGameObject != nullptr ? lastSelectedGameObject->name() : " -- none --";
		drawText2D("Sel.: --", 5, ScreenHeight - 75, *consolasFont, 1);


	}
}


PRIV void renderColoredRect(SDL_Rect dest, glm::vec3 color, float zOrder = -1) {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.fixColored = true;
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(dest.x, dest.y, zOrder));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(dest.w, dest.h, 1));
	ri.alpha = 1;
	ri.tint = color;

	drawVAO(&ri);

}


PRIV void renderTopStatusBar() {
	renderColoredRect({ ScreenWidth / 2, ScreenHeight - 24, ScreenWidth, 48 }, { 0.6, 0.3, 0.1 }, -3);
	Image image((*getTextureCache())["ui-atlas"], 32, 32);
	drawImage2D({ 550, 730, 32, 32 }, image, { 512, 512, 12 * 32, 15 * 32, 32, 32 });
	drawImage2D({ 700, 730, 32, 32 }, image, { 512, 512, 13 * 32, 15 * 32, 32, 32 });
	drawImage2D({ 850, 730, 32, 32 }, image, { 512, 512, 14 * 32, 15 * 32, 32, 32 });
}

PRIV void renderInGameHud() {
	renderTopStatusBar();
	//drawText2D("turret angle:" + std::to_string((double)turretAngle), 5, ScreenHeight - 80, *consolasFont, 1, { 0.2, 0.1, 0 });
	//std::string ipstr = std::to_string(intersectionPoint.x) + "/" + std::to_string(intersectionPoint.y) + "/" + std::to_string(intersectionPoint.z);
	//drawText2D("intersection point:" + ipstr, 5, ScreenHeight - 100, *consolasFont, 1, { 0.1, 0.2, 0 });

}

PRIV void renderWorldToOffscreenTexture() {
	// We render chunks of our world into distinct textures, 
	// which are about screensized.
	// This is much more efficient than to draw a large number 
	// of small tiles with separate draw calls.
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glViewport(0, 0, 1024, 1024);
	GLfloat c[] = { 0.0f, 0.1f, 0.9f, 1 };
	glClearBufferfv(GL_COLOR, 0, c);
	GLfloat d = 1;
	glClearBufferfv(GL_DEPTH, 0, &d);

//	Image waterImage((*getTextureCache())["king-sprites"], 32, 32);
//	drawImage2D({ 200, 250, 32, 32 }, waterImage, grassTileDesc);

	// First we do an offscreen render pass
		// from a top down view for test purposes.
	for (auto gameObject : gameObjects) {
		gameObject->render(activeMainCam, shadowMapCamera, glm::vec3(0), false);
	}


	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 7; y++) {
		//	drawImage2D({ 200, 250, 32, 32 }, waterImage, grassTileDesc);
//			drawImage2D({ 582, 440, 32, 32 }, image, grassTileDesc);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

PRIV void renderWorld() {
	//renderColoredRect({ ScreenWidth / 2, ScreenHeight - 24, ScreenWidth, 48 }, { 0.6, 0.3, 0.1 }, -3);
	Image waterImage((*getTextureCache())["offscreen"], 512, 512);
	Image grassImage((*getTextureCache())["king-sprites"], 32, 32);
	drawImage2D({ 0, 0, 256, 256 }, waterImage, waterTileDesc);
	drawImage2D({ 582, 440, 32, 32 }, grassImage, grassTileDesc);
}


PRIV void render2D(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	if (gameState == Koenig_GameState::GS_Splash) {
		renderEngineSplash(frameTimeGross);
	}

	if (gameState == Koenig_GameState::GS_Menu) {
		renderMainMenu();
	}

	if (gameState == Koenig_GameState::GS_InGame) {
		renderWorld();
		renderInGameHud();
		renderPickMap();
	}

	renderDebugInfo(frameTimeGross, frameTimeNet);
}

PRIV void render3D() {
	
	// Pitch
	
	// Render our shadow map
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFbo);
	glViewport(0, 0, 1024, 1024);
	GLfloat depthClear = 1;
	glClearBufferfv(GL_DEPTH, 0, &depthClear);
	for (auto gameObject : gameObjects) {
		gameObject->render(shadowMapCamera, shadowMapCamera, "shadowmap", glm::vec3(0), false);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, pickMapFbo);
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	GLfloat c[] = { 0, 0, 0, 1 };
	glClearBufferfv(GL_COLOR, 0, c);
	GLfloat d = 1;
	glClearBufferfv(GL_DEPTH, 0, &d);
	for (auto gameObject : gameObjects) {
		float val = (gameObject->id() / 255.0f);
		gameObject->renderWithColorUnlit(activeMainCam, glm::vec3(val, val, val));
	}

	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.05, 0.01, 0.01);

	for (auto gameObject : gameObjects) {
		gameObject->render(activeMainCam, shadowMapCamera, getShaderCache()->get("default"), sunDirection, glm::vec3(0), false);
	}

}

PRIV void render(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.07, 0.03, 0.03);
	timer_render.start();
	render3D();
	render2D(frameTimeGross, frameTimeNet);
	timer_render.stop();
}


int mainsk1(int argc, char** args) {

	createWindow(ScreenWidth, ScreenHeight, false, true);
	HWND hwnd = getNativeWindow();
	soundMachine = new SoundMachine(hwnd);
	shotSound = soundMachine->createDummySound(0.15, 440);
	deepSound = soundMachine->createDummySound(0.30, 110);
	soundMachine->playSound(deepSound);

	init();


	gameState = Koenig_GameState::GS_Splash;

	SDL_Event event;

	std::vector<SDL_Event> frameEvents;

	Timer timer_gross;
	Timer timer_net;

	renderWorldToOffscreenTexture();
	
	timer_gross.start();
	
	while (gameRunning) {
	
		frameEvents.clear();
		
		timer_net.start();
		
		SDL_Event event;
		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				gameRunning = false;
				break;
			}

			if (event.type == SDL_KEYDOWN) {
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					gameRunning = false;
					break;
				}
			}

			frameEvents.push_back(event);
		}
		
		updateUIState(frameEvents);
		update(timer_gross.get_last_measure(microseconds), frameEvents);
		timer_net.stop();
		
		render(timer_gross.get_last_measure(microseconds), timer_net.get_last_measure(microseconds));
		
		flipBuffer();
		frames++;
		timer_gross.stop();
		timer_gross.start();

	}

	return 0;
}


