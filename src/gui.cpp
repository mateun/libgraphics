#include "gui.h"
#include <map>
#include <SDL2/SDL.h>
#include "../include/graphics.h"
#include "texture_cache.h"
#include "render.h"
#include "shader_cache.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "texture_cache.h"
#include "Font.h"

#define PRIV static
#define TexCache (*getTextureCache())

PRIV Font* font = nullptr;
PRIV std::string hotItem;
PRIV std::string activeItem;
PRIV std::string focusedItem;
PRIV std::map<std::string, WidgetData*> uiWidgetMap;
PRIV std::vector<WidgetData*> uiWidgets;
PRIV int mouseX, mouseY;
PRIV float cursorTimer = 0;
PRIV bool showCursor = false;

extern int ScreenWidth, ScreenHeight;



PRIV void fixMouseCoords() {
	mouseY = ScreenHeight - mouseY;
}

PRIV void renderRect(SDL_Rect dest, glm::vec3 color, float zOrder = -1) {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = getShaderCache()->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, getScreenWidth(), 0, getScreenHeight(), .01, 100);
	//ri.texture = TexCache["engine-splash"];
	ri.texture = 0;
	ri.fixColored = true;
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(dest.x, dest.y, zOrder));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(dest.w, dest.h, 1));

	ri.alpha = 1;
	ri.tint = color;

	drawVAO(&ri);
	
}

int calculateCursorXCoord(const std::string& text, Font* font, int cursor) {
	int xPos = 0;
	for (int i = 0; i < cursor; i++) {
		FontInfo fi = font->getFontInfo(text[i]);
		xPos += fi.xadvance;

	}
	return xPos;
}

void doText(const std::string id, std::string text, int x, int y, float depth, glm::vec3 color) {
	if (uiWidgetMap[id] == nullptr) {

		WidgetData* wd = new WidgetData{ x, y, (int) text.size() * 8, 32, nullptr, id, depth };
		uiWidgetMap[id] = wd;
		uiWidgets.push_back(wd);
	}

	if (!font) {
		font = new Font("consolas", "consolas-font");
	}

	drawText2D(text, x + 2, y + 5, *font, 1, color, depth);

}

void doTextInput(const std::string id, std::string* initialText, int x, int y, int w, int h, float depth) {
	if (uiWidgetMap[id] == nullptr) {

		WidgetData* wd = new WidgetData{ x, y, w, h, new TextData{(int)(*initialText).size(), initialText}, id, depth };
		uiWidgetMap[id] = wd;
		uiWidgets.push_back(wd);
	}

	if (!font) {
		font = new Font("consolas", "consolas-font");
	}

	glm::vec3 color = { 0.92, 0.92, 0.92 };
	if (hotItem == id) {
		color = color * 0.8f;
	}

	SDL_Rect r;
	r.x = (double)w / 2 + x;
	r.y = (double)h / 2 + y;
	r.w = w;
	r.h = h;
	

	SDL_Rect rCursor;
	rCursor.x = x + 2 + calculateCursorXCoord((*uiWidgetMap[id]->textData->text), font, uiWidgetMap[id]->textData->cursor);
	rCursor.y = r.y - 2;
	rCursor.w = 2;
	rCursor.h = 20;

	renderRect(r, color, -0.88f);
	drawText2D((*uiWidgetMap[id]->textData->text), x + 2, y + 5, *font, 1, { 0.2, 0.2, 0.2 }, -0.69f);
	if (id == focusedItem) {
		if (showCursor) {
			renderRect(rCursor, { 0.8, 0.4, 0.3 }, -0.59f);
		}
	}
	
}

bool doMenu(const std::string id, const std::string& text, int x, int y, int w, int h,  
		float depth, 
		glm::vec3 color) {

	SDL_Rect r;
	r.x = (double)w / 2 + x;
	r.y = (double)h / 2 + y;
	r.w = w;
	r.h = h;



	if (uiWidgetMap[id] == nullptr) {
		WidgetData* wd = new WidgetData{ x, y, w, h, nullptr, id, depth };
		uiWidgetMap[id] = wd;
		uiWidgets.push_back(wd);
	}

	if (!font) {
		font = new Font("consolas", "consolas-font");
	}

	if (hotItem == id) {
		color = color * 0.9f;
	}

	
	renderRect(r, color, depth);
	drawText2D(text, x+10, y+5, *font, 1, { 0.95, 0.95, 0.95 }, depth + 0.1f);

	if (activeItem == id) {
		return true;
	}
	else {
		return false;
	}

}

bool doButton(const std::string id, const std::string& texture, int x, int y, 
	int w, int h, SpriteAtlasItemDescriptor sad, 
	glm::vec3 normalColor, glm::vec3 activeColor, float depth) {

	if (uiWidgetMap[id] == nullptr) {
		WidgetData* wd = new WidgetData{ x, y, w, h, nullptr, id, depth };
		uiWidgetMap[id] = wd;
		uiWidgets.push_back(wd);
	}

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAOInstance(sad.getUVs());
	ri.shader = getShaderCache()->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())[texture];

	int posX = (double)w / 2 + x;
	int posY = (double)h / 2 + y;

	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(posX, posY, -1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(w, h, 1));
	ri.alpha = 1;

	if (hotItem == id) {
		ri.tint = activeColor;
		ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(0.9, 0.9, 1));
	}

	

	glEnable(GL_BLEND);
	drawVAO(&ri);
	glDisable(GL_BLEND);
	deleteVAOData(ri.vaodata);

	if (activeItem == id) {
		return true;
	}
	else {
		return false;
	}

	
}



bool isHovering(const std::string& id)
{
	return hotItem == id;
}

void updateUIState(std::vector<SDL_Event> frameEvents, uint64_t dt) {
	SDL_GetMouseState(&mouseX, &mouseY);
	fixMouseCoords();

	hotItem = "";
	for (auto w : uiWidgets) {
		if (mouseX > w->x && mouseX < (w->x + w->w) &&
			mouseY > w->y && mouseY < (w->y + w->h)) {
			if (hotItem != "") {
				if (w->depth > uiWidgetMap[hotItem]->depth) {
					hotItem = w->id;
				}
			}
			else {
				hotItem = w->id;
			}
			
		}
	}

	activeItem = "";
	// Check if the mouse is over a ui widget.
	// If yes, its the hot one.
	if (hotItem != "") {
		for (auto e : frameEvents) {
			if (e.type == SDL_MOUSEBUTTONUP) {
				activeItem = hotItem;
				focusedItem = hotItem;
			}
		}
	}



	// Handle textInput 
	WidgetData* wd = uiWidgetMap[focusedItem];
	if (wd && wd->textData) {
		for (auto e : frameEvents) {
			if (e.type == SDL_TEXTINPUT) {
				if (e.text.text[0] < 128 && e.text.text[0] >= 0) {
	
					if ((*wd->textData->text).size() <= wd->textData->cursor) {
						wd->textData->cursor++;
						(*wd->textData->text) += e.text.text[0];
					}
					else {
						(*wd->textData->text)[wd->textData->cursor++] = e.text.text[0];
					}
				}
			}

			if (e.type == SDL_KEYDOWN) {
				if (e.key.keysym.sym == SDLK_BACKSPACE) {
					if (wd->textData->cursor > 0) {
						wd->textData->cursor--;
					}
					(*wd->textData->text)[wd->textData->cursor] = 0;
				}

				if (e.key.keysym.sym == SDLK_LEFT) {
					if (wd->textData->cursor > 0) {
						wd->textData->cursor--;
					}
				}

				if (e.key.keysym.sym == SDLK_RIGHT) {
					if (wd->textData->cursor < 30) {
						wd->textData->cursor++;
					}
				}
			}
			
		}
	}

	// Cursor timing
	float dts = (float)dt / 1000 / 1000;
	cursorTimer += dts;
	if (cursorTimer >= 0.45) {
		showCursor = !showCursor;
		cursorTimer = 0;
	}

}

void mouseXY(int& x, int& y) {
	x = mouseX;
	y = mouseY;
}
