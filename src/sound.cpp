#include "sound.h"
#include <SDL2/SDL.h>


SoundMachine::SoundMachine(HWND hwnd)
{
	HRESULT hr = DirectSoundCreate8(NULL, &dsound8, NULL);
	if (FAILED(hr)) {
		SDL_Log("sound error: %d", hr);
		exit(99);
	}

	dsound8->SetCooperativeLevel(hwnd, DSSCL_PRIORITY);
	DSBUFFERDESC dd;
	memset(&dd, 0, sizeof(DSBUFFERDESC));
	dd.dwSize = sizeof(DSBUFFERDESC);
	dd.dwFlags = DSBCAPS_PRIMARYBUFFER;
	dd.dwBufferBytes = 0;
	dd.lpwfxFormat = NULL;

	hr = dsound8->CreateSoundBuffer(&dd, &sbPrimary, NULL);
	if (FAILED(hr)) {
		SDL_Log("sound buffer error %d", hr);
		exit(100);
	}

	WAVEFORMATEX wfx;
	memset(&wfx, 0, sizeof(WAVEFORMATEX));
	wfx.wFormatTag = WAVE_FORMAT_PCM;
	wfx.nChannels = 2;
	wfx.nSamplesPerSec = 44100;
	wfx.wBitsPerSample = 16;
	wfx.nBlockAlign = wfx.wBitsPerSample / 8 * wfx.nChannels;
	wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;

	hr = sbPrimary->SetFormat(&wfx);
	if (FAILED(hr)) {
		SDL_Log("sound buffer format error %d", hr);
		exit(101);
	}

}

SoundMachine::~SoundMachine() {
	if (sbPrimary) {
		sbPrimary->Release();
		sbPrimary = nullptr;
	}

	if (dsound8) {
		dsound8->Release();
		dsound8 = nullptr;
	}
}

Sound* SoundMachine::createDummySound(float duration, int freq)
{
	WAVEFORMATEX wfx;
	memset(&wfx, 0, sizeof(WAVEFORMATEX));
	wfx.wFormatTag = WAVE_FORMAT_PCM;
	wfx.nChannels = 2;
	wfx.nSamplesPerSec = 48000;
	wfx.wBitsPerSample = 16;
	wfx.nBlockAlign = wfx.wBitsPerSample / 8 * wfx.nChannels;
	wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;


	LPDIRECTSOUNDBUFFER buf;
	DSBUFFERDESC dd;
	memset(&dd, 0, sizeof(DSBUFFERDESC));
	dd.dwSize = sizeof(DSBUFFERDESC);
	dd.dwFlags = DSBCAPS_STATIC ;
	dd.dwBufferBytes = wfx.nSamplesPerSec * 2 * duration;		// duraction secs of CD quality, stereo
	dd.lpwfxFormat = &wfx;

	HRESULT hr = dsound8->CreateSoundBuffer(&dd, &buf, NULL);
	if (FAILED(hr)) {
		SDL_Log("secondary sound buffer error %d", hr);
		exit(110);
	}

	uint16_t* audio_ptr_1;
	uint16_t* audio_ptr_2;
	DWORD audio_l_1;
	DWORD audio_l_2;

	hr = buf->Lock(0, dd.dwBufferBytes, (void**) &audio_ptr_1, &audio_l_1, (void**) &audio_ptr_2,  &audio_l_2, DSBLOCK_ENTIREBUFFER);
	if (FAILED(hr)) {
		SDL_Log("secondary sound buffer lock error %d", hr);
		exit(111);
	}
	bool up = true;
	for (int sample = 0; sample < audio_l_1/2; sample += 2) {
		if (sample % (wfx.nSamplesPerSec / freq)/2 == 0) {
			up = !up;
		}

		if (up) {
			audio_ptr_1[sample] = 50000;
			audio_ptr_1[sample + 1] = 50000;
		}
		else {
			audio_ptr_1[sample] = 0;
			audio_ptr_1[sample + 1] = 0;
		}

	}

	// Write the meaty sound here!!
	/*for (int sample = 0; sample < 44100 * 2; sample++) {
		if (sample < 22000) {
			if (sample % 2000 == 0) {
				audio_ptr_1[sample] = 50000;
				audio_ptr_1[sample + 1] = 50000;
			}
			else {
				audio_ptr_1[sample] = 0;
				audio_ptr_1[sample + 1] = 0;
			}

		}
		else {
			if (sample < 40000 ) {
				if (sample % 300 == 0) {
					audio_ptr_1[sample] = 50000;
					audio_ptr_1[sample + 1] = 50000;
				}
				else {
					audio_ptr_1[sample] = 0;
					audio_ptr_1[sample + 1] = 0;
				}
			}
			
		}
		
	}*/


	hr = buf->Unlock(audio_ptr_1, audio_l_1, (void**) audio_ptr_2, audio_l_2);
	if (FAILED(hr)) {
		SDL_Log("secondary sound buffer lock error %d", hr);
		exit(111);
	}

	Sound* sound = new Sound();
	sound->buf = buf;
	return sound;

}

void SoundMachine::playSound(Sound* sound, bool loop)
{
	if (loop) {
		sound->buf->Play(0, 0, DSBPLAY_LOOPING);
	}
	else {
		sound->buf->Play(0, 0, 0);
	}
	
	
	
	
}
