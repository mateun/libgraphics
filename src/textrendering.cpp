//
// Created by martin on 19.09.20.
//
#include "render.h"
#include "Font.h"
#include "shader_cache.h"
#include "texture_cache.h"
#include "window.h"
#include <GL/glew.h>
#include <string>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define PRIV static

void fillNormals(std::vector<float>& ns);
PRIV int fillPos(std::vector<float>& vector, int letter, FontInfo fontInfo, int textureWidth, int lineHeight, int lastRight);
void fillIndices(std::vector<int>& is, int letter);
void fillUvs(std::vector<float> &vector, int letter, FontInfo info, int textureWidth);
PRIV void renderText(GLuint vao, Font font, int x, int y, int nrVertices, float scale = 1, glm::vec3 color = { 1, 1, 1 }, float depth = 1.0f);

extern int ScreenWidth, ScreenHeight;

void drawText2D(const std::string& text, int x, int y, const Font& font, float scale, glm::vec3 color, float depth ) {
    GLuint fontTexture = font.texture();

    std::vector<float> vs;
    std::vector<float> ts;
    std::vector<float> ns;
    std::vector<int> is;
    
    int letter = 0;
    int lastRight = 0;
    
    for (char c : text) {
         FontInfo fi = font.getFontInfo(c);

        lastRight = fillPos(vs, letter, fi, font.textureWidth, font.lineHeight(), lastRight);
        fillIndices(is, letter);
        fillNormals(ns);
        fillUvs(ts, letter, fi, font.textureWidth);
        letter++;
    }

    
    VAOData textVAOData = createVAO(vs, ts, ns, is);
    renderText(textVAOData.vao, font, x, y, is.size(), scale, color, depth);
    deleteVAOData(textVAOData);
}


PRIV void renderText(GLuint vao, Font font, int x, int y, int nrVertices, float scale, glm::vec3 color, float depth) {
    RenderInfo ri;
    ri.vaodata.vao = vao;
    ri.shader = getShaderCache()->get("text");
    ri.nrOfVertices = nrVertices;
    ri.lit = false;
    ri.shadowMapTexture = 0;
    ri.tint = color;
    ri.fixColored = true;
    ri.alpha = 1;

    glEnable(GL_BLEND);

    ri.texture = font.texture();
    ri.viewMatrix = glm::lookAt(glm::vec3(0,0,5), glm::vec3(0), glm::vec3(0, 1, 0));
    ri.projectionMatrix = glm::ortho<float>(0, getScreenWidth(), 0, getScreenHeight(), .01, 100);
    ri.modelMatrix = glm::translate(glm::mat4(1), glm::vec3(x, y, depth));
    double ar = (double) getScreenWidth() / getScreenHeight();
    ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(scale, scale, 1));
    drawVAO(&ri);

    glDisable(GL_BLEND);
}

/*
 * default uvs
        0, 0,
        1, 0,
        0, 1,
        1, 1
*/

void fillUvs(std::vector<float> &uvs, int letter, FontInfo info, int textureWidth) {
    float uvX = (float) info.x / textureWidth;
    float relY = (float) info.y / textureWidth;
    float uvW = (float) info.width / textureWidth;
    float uvH = (float) info.height / textureWidth;
    float uvY = 1 - uvH - relY;
    uvs.push_back(uvX);
    uvs.push_back(uvY);
    uvs.push_back(uvX + uvW);
    uvs.push_back(uvY);
    uvs.push_back(uvX);
    uvs.push_back((uvY+uvH));
    uvs.push_back(uvX + uvW);
    uvs.push_back((uvY + uvH));

     /*
    uvs.push_back(0.5);
    uvs.push_back(1-0.1);
    uvs.push_back(0.52);
    uvs.push_back(1-0.1);
    uvs.push_back(0.5);
    uvs.push_back(1);
    uvs.push_back(0.52);
    uvs.push_back(1);
     */

}

void fillIndices(std::vector<int>& is, int letter) {
    is.push_back(letter * 4 + 0);
    is.push_back(letter * 4 + 1);
    is.push_back(letter * 4 + 2);
    is.push_back(letter * 4 + 2);
    is.push_back(letter * 4 + 1);
    is.push_back(letter * 4 + 3);

}

/**
*   This function creates the vertices of the quad which is used 
*   to render a single letter. 
*   We are creating the vertices in pixel space as this gave the 
*   best results in terms of the quality of the rendered text.
*   When doing normalized rendering, i.e. not pixel accurate, 
*   the quality was very poor.
*
*/
int fillPos(std::vector<float>& vs, int letter, FontInfo fontInfo, int textureWidth, int lineHeight, int lastRight) {

    float left = letter;
    float right = left + lastRight;
    float bottom = 0;
    float top = lineHeight;

    if (true) {
        int yOffset = fontInfo.yoffset;
        int sizeMod = fontInfo.height;
        int widthMod = fontInfo.width;

        left = lastRight;
        right = left + widthMod;

        top = lineHeight - yOffset;
        bottom = top - sizeMod;

    }
   
    vs.push_back(left);
    vs.push_back(bottom);
    vs.push_back(0);

    vs.push_back(right);
    vs.push_back(bottom);
    vs.push_back(0);

    vs.push_back(left);
    vs.push_back(top);
    vs.push_back(0);

    vs.push_back(right);
    vs.push_back(top);
    vs.push_back(0);

    return left + fontInfo.xadvance ;

}

void fillNormals(std::vector<float>& ns) {
    ns.push_back(0);
    ns.push_back(0);
    ns.push_back(1);
    ns.push_back(0);
    ns.push_back(0);
    ns.push_back(1);
    ns.push_back(0);
    ns.push_back(0);
    ns.push_back(1);

}


