#include "camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(glm::mat4 view, glm::mat4 proj) 
	: _viewMatrix(view), _projMatrix(proj) {

	_position = glm::vec3(0, 2, 5);
	_forward = glm::vec3(0, 0, -1);
	_right = glm::vec3(1, 0, 0);
}

Camera::Camera(glm::vec3 position, glm::vec3 forward, glm::vec3 right, glm::mat4 proj): _position(position), _forward(forward), _right(right), _projMatrix(proj)
{
	//updateTransform(position, forward, right);
}

glm::vec3 Camera::up() {
	return glm::cross(glm::normalize(_right), glm::normalize(this->_forward));
}

void Camera::updateTransform(glm::vec3 position, glm::vec3 forward, glm::vec3 right)
{
	this->_position = position;
	this->_forward = forward;
	this->_right = right;
	glm::vec3 up = glm::cross(glm::normalize(right), glm::normalize(forward));

	glm::vec3 center = _position + glm::normalize(_forward);
	_viewMatrix = glm::lookAt(_position, center, up);
}

glm::mat4 Camera::view() const
{
	return _viewMatrix;
	
}

glm::mat4 Camera::proj() const
{
	return _projMatrix;
}
