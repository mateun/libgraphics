#pragma once
#include <string>
#include <map>
#include <GL/glew.h>
#include "math.h"


struct Mesh {

	GLuint vao;
	int numberOfVertices;
	AABBNoHeight aabb;


};

std::map<std::string, Mesh*>* getMeshCache();
