//
// Created by martin on 04.10.20.
//

#include "Font.h"
#include "ImageImporter.h"
#include "texture.h"
#include "texture_cache.h"
#include <sstream>
#include <fstream>
#include <vector>
#include <cstring>
#include <SDL2/SDL_surface.h>

std::vector<std::string> split(const std::string& input, const std::string& delim) {
    std::vector<std::string> tokens;
    auto start = 0U;
    auto end = input.find(delim);
    tokens.push_back(input.substr(start, end-start));
    start = end + delim.length();
    tokens.push_back(input.substr(start, end-start));
    return tokens;
}

Font::Font(const std::string &fontName, const std::string& texName) {
    SDL_Log("in ctr. of Font");
    
    _texture = (*getTextureCache())[texName];
    std::ifstream file("../test_assets/"+ fontName+".fnt");
    std::string line;
    while (std::getline(file, line))
    {
        FontInfo fi;
        std::istringstream iss(line);
        std::string t1;
        iss >> t1;

        if (t1 == "info") {
            readSize(iss);
            continue;
        }

        if (t1 == "common") {
            readLineHeight(iss);
            readTextureWidth(iss);
            continue;
        }

        if (t1 != "char") {
            continue;
        }

        std::string id; iss >> id;
        fi.id = stoi(split(id, "=")[1]);

        std::string x; iss >> x;
        fi.x = stoi(split(x, "=")[1]);

        std::string y; iss >> y;
        fi.y = stoi(split(y, "=")[1]);

        std::string w; iss >> w;
        fi.width = stoi(split(w, "=")[1]);
        std::string height; iss >> height;
        fi.height = stoi(split(height, "=")[1]);
        std::string xo; iss >> xo;
        fi.xoffset = stoi(split(xo, "=")[1]);

        std::string yo; iss >> yo;
        fi.yoffset = stoi(split(yo, "=")[1]);

        std::string xa; iss >> xa;
        fi.xadvance = stoi(split(xa, "=")[1]);
        
        _fontInfoMap[fi.id] = fi;
    }

    file.close();
   
}

void::Font::readSize(std::istringstream& iss) {
    std::string w;
    while (strncmp(w.c_str(), "size", 4) != 0) {
        iss >> w;
    }
    this->_size = stoi(split(w, "=")[1]);
}

void Font::readTextureWidth(std::istringstream& iss) {
    std::string w;
    while (strncmp(w.c_str(), "scaleW", 6) != 0) {
        iss >> w;
    }
    this->textureWidth = stoi(split(w, "=")[1]);

}

void Font::readLineHeight(std::istringstream& iss) {
    std::string w;
    while (strncmp(w.c_str(), "lineHeight", 6) != 0) {
        iss >> w;
    }
    this->_lineHeight = stoi(split(w, "=")[1]);

}

FontInfo Font::getFontInfo(int charNum) const {
    if (charNum < 0) {
        return _fontInfoMap.at(0);
    }
    return _fontInfoMap.at(charNum);
}
