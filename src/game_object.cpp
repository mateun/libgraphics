#include "game_object.h"
#include "game_object.h"
#include <SDL2/SDL.h>
#include <algorithm>
#include "render.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "shader_cache.h"
#include "mesh.h"
#include "texture_cache.h"
#include "camera.h"
#include <string>



GameObject::GameObject(uint8_t id, const std::string& name, glm::vec3 worldPosition) : GameObject(id, name, worldPosition, nullptr)
{

}

GameObject::GameObject(uint8_t id, const std::string& name, glm::vec3 worldPosition, Mesh* mesh) :
    _id(id), _name(name),_position(worldPosition), _mesh(mesh)
{
	//SDL_Log("in ctr. of game object");
	_renderInfo = new RenderInfo();
	_shaderCache = getShaderCache();
	_rotationEuler = glm::vec3(0);
	_scale = glm::vec3(1);
    if (_mesh) {
        _aabb = _mesh->aabb;
        updateAABB();
    }
    else {
        _aabb = { 0, 0, 0, 0 };
    }
    
}


void GameObject::updateAABB() {

    _aabb.left = _mesh->aabb.left * _scale.x;
    _aabb.right = _mesh->aabb.right * _scale.x;
    _aabb.top = _mesh->aabb.top * _scale.z;
    _aabb.top = _mesh->aabb.bottom * _scale.z;

    _aabb.left = _mesh->aabb.left + _position.x;
    _aabb.right = _mesh->aabb.right + _position.x;
    _aabb.top = _mesh->aabb.top - _position.z;
    _aabb.bottom = _mesh->aabb.bottom + _position.x;


}

void GameObject::update(float dt) {
	for (auto uc : _updateComponents)	{
		uc->update(dt);	
	}

}

void GameObject::renderTextured(const Camera* camera, const Camera* lightCamera)
{
    
}

void GameObject::renderWithColorUnlit(const Camera* camera, glm::vec3 color)
{
    if (!_mesh) {
        return;
    }
    _renderInfo->vaodata.vao = _mesh->vao;
    _renderInfo->shader = _shaderCache->get("default");
    _renderInfo->nrOfVertices = _mesh->numberOfVertices;
    _renderInfo->viewMatrix = camera->view();
    _renderInfo->projectionMatrix = camera->proj();

    //_renderInfo->projectionMatrixLight = lightCamera->proj();
    //_renderInfo->viewMatrixLight = lightCamera->view();

    _renderInfo->texture = 0;
    _renderInfo->shadowMapTexture = 0;

    /*_renderInfo->modelMatrix = glm::scale(glm::mat4(1), _scale);
    _renderInfo->modelMatrix = glm::translate(_renderInfo->modelMatrix, _position);
    _renderInfo->modelMatrix = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.x, glm::vec3(1, 0, 0));
    _renderInfo->modelMatrix = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.y, glm::vec3(0, 1, 0));
    _renderInfo->modelMatrix = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.z, glm::vec3(0, 0, 1));
    */

    glm::mat4 scale = glm::scale(glm::mat4(1), _scale);
    glm::mat4 trans = glm::translate(_renderInfo->modelMatrix, _position);
    glm::mat4 rotX = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.x, glm::vec3(1, 0, 0));
    glm::mat4 rotY = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.y, glm::vec3(0, 1, 0));
    glm::mat4 rotZ = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.z, glm::vec3(0, 0, 1));

    _renderInfo->modelMatrix = trans * rotY * scale;

    // TODO derive from lightCamera
    //_renderInfo->sunDirection = sunDirection;

    _renderInfo->fixColored = true;
    _renderInfo->tint = color;   
    _renderInfo->lit = false;
    _renderInfo->alpha = 1;
    
    drawVAO(_renderInfo);
    
}



void GameObject::setEulerRotationX(float val) {
	_rotationEuler.x = val;

}

void GameObject::setEulerRotationY(float val) {
	_rotationEuler.y = val;

}

void GameObject::setEulerRotationZ(float val) {
	_rotationEuler.z = val;

}
void GameObject::render(const Camera* camera, const Camera* lightCamera, glm::vec3 fixColor, bool fixColored) {
    render(camera, lightCamera, _shaderCache->get("default"), glm::vec3(-0.2, -0.5, -0.2), fixColor, fixColored);
}

void GameObject::addUpdateComponent(UpdateComponent* uc) {
	_updateComponents.push_back(uc);
}

void GameObject::removeUpdateComponent(UpdateComponent* uc) {
	_updateComponents.erase(std::remove(_updateComponents.begin(), _updateComponents.end(), uc), _updateComponents.end());
}

void GameObject::translate(glm::vec3 vec)
{
    this->_position += vec;
    updateAABB();
}

void GameObject::scale(glm::vec3 scaleVal)
{
    this->_scale = scaleVal;
    updateAABB();
    
    
}

bool GameObject::collides(const GameObject* other)
{
    if (!_mesh) {
        return false;
    }

    // Use the aabb
    if (_aabb.left >= other->_aabb.left &&  _aabb.left <= other->_aabb.right) {
        if (_aabb.top > other->_aabb.top  && _aabb.top <= other-> _aabb.bottom) {
            return true;
        }
    }
    else {
        return false;
    }
        
    
}

GameObject::~GameObject() {
    SDL_Log("in dtr. of GameObject!\n");
    if (_renderInfo) {
        delete (_renderInfo);
        _renderInfo = nullptr;
    }
    if (_mesh) {
        _mesh = nullptr;
    }
    for (auto uc : _updateComponents) {
        if (uc) {
            delete(uc);
            uc = nullptr;
        }
    }
}

void GameObject::render(const Camera *camera, const Camera *lightCamera, const GLuint shaderId, glm::vec3 sunDirection, glm::vec3 fixColor, bool fixColored, bool transparent) {
    if (!_mesh) {
        return;
    }
    _renderInfo->vaodata.vao = _mesh->vao;
    _renderInfo->shader = _shaderCache->get("default");
    _renderInfo->nrOfVertices = _mesh->numberOfVertices;
    //_renderInfo->viewMatrix = glm::lookAt(glm::vec3(0, 20, 7), glm::vec3(0, 0, 0), glm::vec3(0, 1,0));
    _renderInfo->viewMatrix = camera->view();
    //_renderInfo->projectionMatrix = glm::perspective(90.0f, (float)(800.0f/600.0f), 0.1f, 300.0f);
    _renderInfo->projectionMatrix = camera->proj();

    _renderInfo->projectionMatrixLight = lightCamera->proj();
    _renderInfo->viewMatrixLight = lightCamera->view();

    _renderInfo->texture = (*getTextureCache())[_textureName];
    _renderInfo->shadowMapTexture = (*getTextureCache())["shadowDepth"];

    /*_renderInfo->modelMatrix = glm::scale(glm::mat4(1), _scale);
    _renderInfo->modelMatrix = glm::translate(_renderInfo->modelMatrix, _position);
    _renderInfo->modelMatrix = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.x, glm::vec3(1, 0, 0));
    _renderInfo->modelMatrix = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.y, glm::vec3(0, 1, 0));
    _renderInfo->modelMatrix = glm::rotate(_renderInfo->modelMatrix, _rotationEuler.z, glm::vec3(0, 0, 1));
    */

    glm::mat4 scale = glm::scale(glm::mat4(1), _scale);
    glm::mat4 trans = glm::translate(glm::mat4(1), _position);
    glm::mat4 rotX = glm::rotate(glm::mat4(1), _rotationEuler.x, glm::vec3(1, 0, 0));
    glm::mat4 rotY = glm::rotate(glm::mat4(1), _rotationEuler.y, glm::vec3(0, 1, 0));
    glm::mat4 rotZ = glm::rotate(glm::mat4(1), _rotationEuler.z, glm::vec3(0, 0, 1));

    _renderInfo->modelMatrix = trans * rotY * scale;
    
    _renderInfo->sunDirection = sunDirection;
    

    _renderInfo->fixColored = fixColored;
    if (fixColored) {
        _renderInfo->tint = fixColor;
    }
    _renderInfo->lit = true;

    if (transparent) {
        glEnable(GL_BLEND);
    }
    
    drawVAO(_renderInfo);

    if (transparent) {
        glDisable(GL_BLEND);
    }
    
}



void GameObject::renderShadowMap(const Camera* camera, const std::string& shaderName) {
    if (!_mesh) {
        return;
    }
    _renderInfo->vaodata.vao = _mesh->vao;
    _renderInfo->shader = _shaderCache->get(shaderName);
    _renderInfo->nrOfVertices = _mesh->numberOfVertices;
    _renderInfo->viewMatrix = camera->view();
    _renderInfo->projectionMatrix = camera->proj();

    

    glm::mat4 scale = glm::scale(glm::mat4(1), _scale);
    glm::mat4 trans = glm::translate(glm::mat4(1), _position);
    glm::mat4 rotX = glm::rotate(glm::mat4(1), _rotationEuler.x, glm::vec3(1, 0, 0));
    glm::mat4 rotY = glm::rotate(glm::mat4(1), _rotationEuler.y, glm::vec3(0, 1, 0));
    glm::mat4 rotZ = glm::rotate(glm::mat4(1), _rotationEuler.z, glm::vec3(0, 0, 1));

    _renderInfo->modelMatrix = trans * rotY * scale;

    drawVAO(_renderInfo);

}


void GameObject::render(const Camera *camera, const Camera* lightCamera, const std::string &shaderName, glm::vec3 fixColor, bool fixColored) {
    render(camera, lightCamera, _shaderCache->get(shaderName), glm::vec3(-0.2, -0.5, -0.2), fixColor, fixColored);
}
