#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <GL/glew.h>
#include "math.h"

class Mesh;
class RenderInfo;
class ShaderCache;
class Camera;

// TODO (GRU) probably make this lambdas
class UpdateComponent {

public:
	virtual void update(float dt) = 0;

};




class GameObject {

public:
	GameObject(uint8_t id, const std::string& name, glm::vec3 worldPosition);
	GameObject(uint8_t id, const std::string& name, glm::vec3 worldPosition, Mesh* mesh);
	virtual ~GameObject();
	void setEulerRotationX(float val);
	void setEulerRotationY(float val);
	void setEulerRotationZ(float val);
	void update(float time);
	void renderTextured(const Camera* camera, const Camera* lightCamera);
	void renderWithColorUnlit(const Camera* camera, glm::vec3 color);
	void render(const Camera* camera, const Camera* lightCamera, glm::vec3 fixColor, bool fixColored=false);
    void render(const Camera* camera, const Camera* lightCamera, const std::string& shaderName, glm::vec3 fixColor, bool fixColored=false);
    void render(const Camera* camera, const Camera* lightCamera, const GLuint shaderId, glm::vec3 sunDirection, glm::vec3 fixColor, bool fixColored=false, bool transparent = false );
	void renderShadowMap(const Camera* camera, const std::string& shaderName);
	void addUpdateComponent(UpdateComponent* uc);
	void removeUpdateComponent(UpdateComponent* uc);
	void translate(glm::vec3 vec);
	void scale(glm::vec3 scaleVal);
	void setMesh(Mesh* m) { this->_mesh = m; updateAABB(); }
	Mesh* getMesh() { return this->_mesh; }
	glm::vec3 position() { return _position; }
	void setPosition(glm::vec3 pos) { this->_position = pos; }
	uint8_t id() { return _id;  }
	std::string name() { return _name; }
	void setCollidable(bool val) { _collidable = val; }
	bool collidable() {
		return _collidable;
	}
	bool collides(const GameObject* other);
	AABBNoHeight getAABB() { return _aabb; }


// Properties
public:

	uint8_t _id;
	std::string _name;
	
	
	glm::vec3 _rotationEuler;
	glm::vec3 _forward;
	
	std::string _textureName;
	std::vector<UpdateComponent*> _updateComponents;
	RenderInfo* _renderInfo = nullptr;
	ShaderCache* _shaderCache = nullptr;
	bool _collidable = false;
	AABBNoHeight _aabb;

	

private:
	Mesh* _mesh = nullptr;
	glm::vec3 _scale;
	glm::vec3 _position;

	void updateAABB();


};
