//
// Created by martin on 04.10.20.
//

#ifndef GRAPHICSLIB_FONT_H
#define GRAPHICSLIB_FONT_H

#include <GL/glew.h>
#include <string>
#include <map>

struct FontInfo {
    int id;
    int x;
    int y;
    int width;
    int height;
    int xoffset;
    int yoffset;
    int xadvance;


};

class Font {
public:
    Font(const std::string& fontName, const std::string& textureName);
    int textureWidth;
    FontInfo getFontInfo(int charNum) const;
    GLuint texture() const { return _texture;  }
    int size() const { return _size;  }
    int lineHeight() const { return _lineHeight; }

private:
    std::map<int, FontInfo> _fontInfoMap;
    GLuint _texture;
    int _size;
    int _lineHeight;

    void readTextureWidth(std::istringstream& istringstream);
    void readLineHeight(std::istringstream& iss);
    void readSize(std::istringstream& iss);
};


#endif //GRAPHICSLIB_FONT_H
