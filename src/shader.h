#pragma once
#include <GL/glew.h>
#include <string>



GLuint createVertexShader(const std::string& vsFileName);
GLuint createFragmentShader(const std::string& psFileName);
GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader);

GLuint createVertexShaderFromCode(const std::string& vsCode);
GLuint createFragmentShaderFromCode(const std::string& psCode);

