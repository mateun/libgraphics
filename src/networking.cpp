#pragma once
#include "networking.h"
#include <SDL_net.h>
#include <vector>

static std::vector<std::function< void(const UDPMessage&)>> messageListeners;
static uint32_t msgId = 0;


void startNetworking() {
	if (SDLNet_Init() == -1) {
		exit(2);
	}


}

UDPNode* startUDPNode(int port) {
	UDPNode* server = new UDPNode();
	UDPsocket serverSocket = SDLNet_UDP_Open(port);
	if (!serverSocket) {
		SDL_Log("udp error: %s\n", SDLNet_GetError());
		exit(3);
	}
	server->udpSocket = serverSocket;
	return server;
}

void udpSend(UDPNode* server, UDPMessage msg) {

}


bool udpPoll(UDPNode* server, UDPMessage& message) {
	UDPpacket* packet = SDLNet_AllocPacket(256);
	int numrec = SDLNet_UDP_Recv(server->udpSocket, packet);
	if (numrec > 0) {
		memcpy(message.data, packet->data, packet->len);
		message.senderReceiver = packet->address;

		for (auto l : messageListeners) {
			l({ packet->data, packet->address });
		}
		return true;
	}
	else {
		return false;
	}

	SDLNet_FreePacket(packet);
}

void udpSend(UDPNode* sender, IPaddress target, uint8_t* data, int len)
{
	msgId++;

	UDPpacket* packet = SDLNet_AllocPacket(256);
	packet->address = target;
	memcpy(packet->data,data, len);
	packet->len = len;
	if (!SDLNet_UDP_Send(sender->udpSocket, -1, packet)) {
		SDL_Log("ERROR sending packet!!");
	}
	SDLNet_FreePacket(packet);
}

void registerMessageListener(std::function<void(UDPMessage)> msgListener)
{
	messageListeners.push_back(msgListener);
}
