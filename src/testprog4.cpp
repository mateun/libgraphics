#include "../include/graphics.h"
#include "stdio.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "math.h"
#include <vector>
#include "game_object.h"
#include "texture_cache.h"
#include "camera.h"
#include "ImageImporter.h"
#include "Font.h"
#include "gui.h"
#include <set>
#include "game_lib.h"
#include "scene_management.h"
#include "sound.h"
#include <tgmath.h>
#include <algorithm>

#define PRIV static
#define MeshCache (*getMeshCache())

PRIV enum GameState {
	GS_None,
	GS_Splash,
	GS_Menu,
	GS_InGame
};


// Drag/Drop
PRIV enum DragState {
	DS_None,
	DS_DRAGOK,
	DS_NOK
};
// End drag/drop

struct Bullet {
	GameObject* gameObj;
	int maxLifeTime = 5;
	float lifeTime = 0;
	int damage = 10;
};


PRIV int ScreenWidth = 1024;
PRIV int ScreenHeight = 768;

PRIV SoundMachine* soundMachine = nullptr;
PRIV Sound* shotSound = nullptr;
PRIV Sound* deepSound = nullptr;
PRIV Font* notoFont = nullptr;
PRIV Font* consolasFont = nullptr;
PRIV ShaderCache* shaderCache = nullptr;
PRIV std::vector<GameObject*> gameObjects;
PRIV std::vector<Bullet*> bullets;
PRIV std::map<std::string, GameObject*> gameObjectNameMap;
PRIV QuadTree* quadTree = nullptr;
PRIV GLuint shadowFbo;
PRIV GLuint pickMapFbo;
PRIV Camera* topDownCamera = nullptr;
PRIV Camera* shadowMapCamera = nullptr;
PRIV Camera* debugCamera = nullptr;
PRIV Camera* activeMainCam = nullptr;
PRIV float camPanX = 0.0f;
PRIV float camPanZ = 0;
PRIV float camPanY = 0;
PRIV float camMoveFwd = 0;
PRIV bool debugMode = false;
PRIV int frames = 0;
PRIV GameState gameState = GS_None;
PRIV glm::vec3 sunDirection = glm::vec3(-0.5, -0.6, -.3);
PRIV bool gameRunning = true;
PRIV DragState dragState = DS_None;
PRIV Ray dragRay;
PRIV glm::vec3 intersectionPoint;
PRIV GameObject* dummyDragGameObject;
PRIV float cameraYaw = 0;
PRIV float cameraPitch = 0;
PRIV const float camSpeed = 0.5f;
PRIV float droneHMov = 0;
PRIV float droneVMov = 0;
PRIV VAOData gridVAOData;
PRIV float droneSpeed = 10;

PRIV bool showDebugGui() {
	return debugMode == true;
}

PRIV VAOData setupGrid();

PRIV void initFramebuffers() {
	shadowFbo = createFramebuffer();
	GLuint shadowDepthTex = createDepthTextureInMem(1024, 1024);
	(*getTextureCache())["shadowDepth"] = shadowDepthTex;
	attachDepthTextureToFramebuffer(shadowFbo, shadowDepthTex);

	pickMapFbo = createFramebuffer();
	GLuint pickmapColTex = createColorTextureInMem(ScreenWidth, ScreenHeight);
	GLuint pickmapDepthText = createDepthTextureInMem(ScreenWidth, ScreenHeight);
	(*getTextureCache())["pickMapCol"] = pickmapColTex;
	attachColorAndDepthTexturesToFramebuffer(pickMapFbo, pickmapColTex, pickmapDepthText);
}

PRIV void initShaders() {
	GLuint vs = createVertexShader("../test_shaders/vs.glsl");
	GLuint fs = createFragmentShader("../test_shaders/fs.glsl");
	GLuint shaderProg = createShaderProgram(vs, fs);

	GLuint shadowmap_vs = createVertexShader("../test_shaders/shadowmap-vs.glsl");
	GLuint shadowmap_fs = createFragmentShader("../test_shaders/shadowmap-fs.glsl");
	GLuint shadowMapshaderProg = createShaderProgram(shadowmap_vs, shadowmap_fs);

	GLuint fsText = createFragmentShader("../test_shaders/fs-text.glsl");
	GLuint textRenderShaderProg = createShaderProgram(vs, fsText);

	shaderCache = getShaderCache();
	shaderCache->put("default", shaderProg);
	shaderCache->put("shadowmap", shadowMapshaderProg);
	shaderCache->put("text", textRenderShaderProg);
}

PRIV void initCameras() {
	glm::mat4 vTopDown = glm::lookAt(glm::vec3(0, 30, 0), glm::vec3(0, 2, 0), glm::vec3(0, 1, 0));
	glm::mat4 pTopDown = glm::perspective(1.04f, (float)(ScreenWidth / ScreenHeight), 0.01f, 300.0f);

	glm::mat4 vDebugCam = glm::lookAt(glm::vec3(0, 1.8f, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pDebugCam = glm::perspective(1.04f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);

	// Matrices for rendering the shadow map.
	// We use an orthographic projection to simulate sunlight with parallel rays.
	glm::mat4 vShadowMap = glm::lookAt(sunDirection * -20.0f, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pShadowMap = glm::ortho<float>(-65, 65, -65, 65, 0.1, 1000);

	//topDownCamera = new Camera(vTopDown, pTopDown);
	topDownCamera = new Camera(glm::vec3(0, 35, 0), glm::vec3(0, -1, 0), { 1, 0,0 }, pTopDown);
	shadowMapCamera = new Camera(vShadowMap, pShadowMap);
	debugCamera = new Camera(vDebugCam, pDebugCam);

	activeMainCam = topDownCamera;
}

PRIV void initTextures() {
	GLuint stoneTexture = createTexture("../test_assets/generic_texture.bmp");
	(*getTextureCache())["stone"] = stoneTexture;

	GLuint redTexture = createTexture("../test_assets/red.bmp");
	(*getTextureCache())["red"] = redTexture;

	GLuint blueTexture = createTexture("../test_assets/blue.bmp");
	(*getTextureCache())["blue"] = blueTexture;

	ImageData* notoSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/noto.png", notoSurface);
	GLuint notoTexture = createTextureFromSurface(notoSurface, true);
	(*getTextureCache())["noto-font"] = notoTexture;
	notoFont = new Font("noto", "noto-font");
	delete(notoSurface->pixels);
	delete(notoSurface);

	ImageData* consolasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/consolas.png", consolasSurface);
	GLuint consolasTexture = createTextureFromSurface(consolasSurface, true);
	(*getTextureCache())["consolas-font"] = consolasTexture;
	consolasFont = new Font("consolas", "consolas-font");
	delete(consolasSurface->pixels);
	delete(consolasSurface);

	ImageData* spriteSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/sprite.png", spriteSurface);
	GLuint sprite = createTextureFromSurface(spriteSurface, true);
	(*getTextureCache())["sprite"] = sprite;
	delete(spriteSurface->pixels);
	delete(spriteSurface);

	ImageData* engineSplashSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/piglet-engine-splash.png", engineSplashSurf);
	GLuint engineSplash = createTextureFromSurface(engineSplashSurf, true);
	(*getTextureCache())["engine-splash"] = engineSplash;
	delete(engineSplashSurf->pixels);
	delete(engineSplashSurf);

	ImageData* mainMenuSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/drone_wars_title.png", mainMenuSurface);
	GLuint mainMenuTexture = createTextureFromSurface(mainMenuSurface, true);
	(*getTextureCache())["title"] = mainMenuTexture;
	delete(mainMenuSurface->pixels);
	delete(mainMenuSurface);

	ImageData* uiAtlasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/ui-sprite-atlas.png", uiAtlasSurface);
	GLuint uiAtlasTexture = createTextureFromSurface(uiAtlasSurface, true);
	(*getTextureCache())["ui-atlas"] = uiAtlasTexture;
	delete(uiAtlasSurface->pixels);
	delete(uiAtlasSurface);

	ImageData* lpPalSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/lp-palette.png", lpPalSurface);
	GLuint lpPalTexture = createTextureFromSurface(lpPalSurface, true);
	(*getTextureCache())["lp-palette"] = lpPalTexture;
	delete(lpPalSurface->pixels);
	delete(lpPalSurface);


	ImageData* testFigureSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/figure_diffuse.png", testFigureSurface);
	GLuint fdTexture = createTextureFromSurface(testFigureSurface, true);
	(*getTextureCache())["testfigure-diffuse"] = fdTexture;
	delete(testFigureSurface->pixels);
	delete(testFigureSurface);

	ImageData* greenSemiSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/green_semitransparent.png", greenSemiSurf);
	GLuint greenSemiTexture = createTextureFromSurface(greenSemiSurf, true);
	(*getTextureCache())["green-semitransparent"] = greenSemiTexture;
	delete(greenSemiSurf->pixels);
	delete(greenSemiSurf);

}

int bulletCount = 0;
PRIV std::string spawnBullet() {
	std::string name = "bullet_" + std::to_string(bulletCount++);
	GameObject* bullet_gm = new GameObject(bulletCount, name, glm::vec3(-1, 3, 4), MeshCache["bullet"]);
	bullet_gm->_textureName = "lp-palette";
	bullet_gm->scale(glm::vec3(1, 1, 1));
	bullet_gm->setPosition({ 0, 0, 0 });
	gameObjectNameMap[name] = bullet_gm;
	gameObjects.push_back(bullet_gm);
	Bullet* b = new Bullet();
	b->gameObj = bullet_gm;
	bullets.push_back(b);
	return name;
}

PRIV void initGameObjects() {
	
	GameObject* drone_body_gm = new GameObject(1, "droneBody", glm::vec3(-1, 3, 4), MeshCache["droneBody"]);
	drone_body_gm->_textureName = "lp-palette";
	drone_body_gm->scale(glm::vec3(1, 1, 1));
	drone_body_gm->setPosition({ 0, 0, 0 });
	gameObjectNameMap[drone_body_gm->name()] = drone_body_gm;

	GameObject* drone_turret_gm = new GameObject(2, "droneTurret", glm::vec3(-1, 3, 4), MeshCache["droneTurret"]);
	drone_turret_gm->_textureName = "lp-palette";
	drone_turret_gm->scale(glm::vec3(1, 1, 1));
	drone_turret_gm->setPosition({ 0, 0, 0 });
	gameObjectNameMap[drone_turret_gm->name()] = drone_turret_gm;

	GameObject* ag_gm = new GameObject(3, "arenaGround", glm::vec3(-1, 3, 4), MeshCache["arenaGround"]);
	ag_gm->_textureName = "lp-palette";
	ag_gm->scale(glm::vec3(10, 1, 10));
	ag_gm->setPosition({ 0, -0.4, 0 });
	gameObjectNameMap[ag_gm->name()] = ag_gm;

	GameObject* wall_gm = new GameObject(4, "wall", glm::vec3(-1, 3, 4), MeshCache["wall"]);
	wall_gm->_textureName = "lp-palette";
	wall_gm->scale(glm::vec3(1, 4, 1));
	wall_gm->setPosition({ -7, 0, -5 });
	wall_gm->setCollidable(true);
	gameObjectNameMap[wall_gm->name()] = wall_gm;

	GameObject* wall_gm2 = new GameObject(4, "wall2", glm::vec3(-1, 3, 4), MeshCache["wall"]);
	wall_gm2->_textureName = "lp-palette";
	wall_gm2->scale(glm::vec3(1, 4, 1));
	//wall_gm2->setEulerRotationY(glm::pi<float>() / 2);
	wall_gm2->setPosition({ -10, 0, -15 });
	wall_gm2->setCollidable(true);
	gameObjectNameMap[wall_gm2->name()] = wall_gm2;

	GameObject* wall_gm3 = new GameObject(4, "wall3", glm::vec3(-1, 3, 4), MeshCache["wall"]);
	wall_gm3->_textureName = "lp-palette";
	wall_gm3->scale(glm::vec3(1, 4, 3));
	wall_gm3->setPosition({ 7, 0, 9 });
	wall_gm3->setCollidable(true);
	gameObjectNameMap[wall_gm3->name()] = wall_gm3;

	
	gameObjects.push_back(drone_body_gm);
	gameObjects.push_back(drone_turret_gm);
	gameObjects.push_back(ag_gm);
	gameObjects.push_back(wall_gm);
	gameObjects.push_back(wall_gm2);
	gameObjects.push_back(wall_gm3);

	Timer t;
	t.start();
	quadTree = new QuadTree(100, 100, -50, 50, gameObjects);
	t.stop();
	int ms = t.get_last_measure(microseconds);
	SDL_Log("tree createion: %d", ms);
	
}

PRIV void init() {

	initTextures();

	Mesh* arenaMesh = importToVAO("../test_assets/arena.obj");
	Mesh* tankMesh = importToVAO("../test_assets/tank1.obj");
	Mesh* palmMesh = importToVAO("../test_assets/palmtree.obj");
	Mesh* houseMesh = importToVAO("../test_assets/house_lp1.obj");
	Mesh* testfigureMesh = importToVAO("../test_assets/plane.obj");
	Mesh* terrainMesh = importToVAO("../test_assets/terrain.obj");
	Mesh* droneBodyMesh = importToVAO("../test_assets/drone_body.obj");
	Mesh* droneTurretMesh = importToVAO("../test_assets/drone_turret.obj");
	Mesh* arenaGroundMesh = importToVAO("../test_assets/arena_ground.obj");
	Mesh* bulletMesh = importToVAO("../test_assets/bullet.obj");
	Mesh* wallMesh = importToVAO("../test_assets/wall.obj");

	MeshCache["arena"] = arenaMesh;
	MeshCache["tank"] = tankMesh;
	MeshCache["palm"] = palmMesh;
	MeshCache["house"] = houseMesh;
	MeshCache["testFigure"] = testfigureMesh;
	MeshCache["droneBody"] = droneBodyMesh;
	MeshCache["droneTurret"] = droneTurretMesh;
	MeshCache["arenaGround"] = arenaGroundMesh;
	MeshCache["bullet"] = bulletMesh;
	MeshCache["wall"] = wallMesh;

	initShaders();

	initFramebuffers();

	initCameras();

	initGameObjects();

	gridVAOData = setupGrid();

}


PRIV void updateMouseRay() {
	int mx, my;
	mouseXY(mx, my);
	dragRay = createRayFromScreenCoordinates(mx, my, topDownCamera, ScreenWidth, ScreenHeight);
	rayIntersectsPlane({ 0, 1, 0 }, { 10, 0, 0 }, dragRay, intersectionPoint);

}

PRIV bool checkInTreeNode(Bullet* bullet, QTNode* node) {
	if (node->doesObjectFitIntoNode(bullet->gameObj) && node->isLeaf()) {
		if (GameObject* go = node->getGameObjec()) {
			bool collides = bullet->gameObj->collides(go);
			if (collides) {
				SDL_Log("bullet %s hit object %s", bullet->gameObj->name().c_str(), node->getGameObjec()->name().c_str());
			}
			return collides;
				
		} else {
			return false;
		}

	} else {
		bool collision = false;
		for (auto n : node->children()) {
			if (checkInTreeNode(bullet, n)) {
				
				collision = true;
				break;
			}
		}
		return collision;
	}
}

PRIV bool testBulletCollision(Bullet* bullet) {
	return checkInTreeNode(bullet, quadTree->root());
}

PRIV void updateBullets(float dt) {

	std::vector<Bullet*> removeList;
	for (auto b : bullets) {
		b->gameObj->translate(b->gameObj->_forward * 0.2f);
		b->lifeTime += dt;
		if (b->lifeTime >= b->maxLifeTime) {
			removeList.push_back(b);
		}
		else {
			if (testBulletCollision(b)) {
				soundMachine->playSound(deepSound, false);
				removeList.push_back(b);
			}
		}
	}

	for (auto b : removeList) {
		std::vector<Bullet*>::iterator position = std::find(bullets.begin(), bullets.end(), b);
		std::vector<GameObject*>::iterator indexInGameObjects = std::find(gameObjects.begin(), gameObjects.end(), b->gameObj);
		if (position != bullets.end()) {
			bullets.erase(position);
			gameObjectNameMap[b->gameObj->name()] = nullptr;
			gameObjects.erase(indexInGameObjects);
			delete(b->gameObj);
			delete(b);
		}
		
	}
}


PRIV float turretAngle = 0;

PRIV void updateDrone(std::vector<SDL_Event>& frameEvents, float dt) {

	dt = dt / 1000 / 1000;

	const Uint8* state = SDL_GetKeyboardState(nullptr);
	if (state[SDL_SCANCODE_RIGHT] || state[SDL_SCANCODE_D]) {
		droneHMov = droneSpeed * dt;
	}
	else if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_A]) {
		droneHMov = -droneSpeed * dt;
	}
	else {
		droneHMov = 0;
	}

	if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_W]) {
		droneVMov = -droneSpeed * dt;
	}
	else if (state[SDL_SCANCODE_DOWN] || state[SDL_SCANCODE_S]) {
		droneVMov = droneSpeed * dt;
	}
	else {
		droneVMov = 0;
	}

	gameObjectNameMap["droneBody"]->translate ({ droneHMov, 0, droneVMov });
	gameObjectNameMap["droneTurret"]->setPosition(gameObjectNameMap["droneBody"]->position());
	glm::vec3 turretPos = gameObjectNameMap["droneTurret"]->position();

	updateMouseRay();

	glm::vec3 turretDir = glm::normalize(glm::vec3{ intersectionPoint.x, intersectionPoint.y, intersectionPoint.z } - turretPos);

	float cosAngle = glm::dot(turretDir, glm::vec3{ 1, 0, 0 });
	float cosAngle2 = glm::dot(glm::normalize(glm::vec3{ intersectionPoint.x, intersectionPoint.y, intersectionPoint.z } - turretPos), glm::vec3{ -1, 0, 0 });
	turretAngle =  acos(cosAngle);

	// This is necessary to also enable rotation in the lower (positive z in our righthanded coordinate system) 
	// halfspace. Otherweise rotation is limited to 0 - 180 degrees.
	// If we point below the object (from top down perspective), 
	// we subtract the calculated angle from 2PI, which is full 360 degrees.
	if (turretDir.z > 0) {
		turretAngle = 2 *glm::pi<float>() - acos(cosAngle);
	}
	gameObjectNameMap["droneTurret"]->setEulerRotationY(turretAngle + glm::pi<float>() / 2);
	
	for (auto fe : frameEvents) {
		if (fe.type == SDL_MOUSEBUTTONDOWN) {
			if (fe.button.button == SDL_BUTTON_LEFT) {
				if (gameState == GameState::GS_InGame) {
					std::string bulletId = spawnBullet();
					soundMachine->playSound(shotSound);
					gameObjectNameMap[bulletId]->setPosition(gameObjectNameMap["droneBody"]->position() + glm::vec3{ 0, 0.5, 0 } + turretDir*1.2f);
					gameObjectNameMap[bulletId]->_forward = turretDir;
				}
			}
		}
	}
	
	updateBullets(dt);
	
}

PRIV void moveGamePlayCamera(std::vector<SDL_Event>& frameEvents) {

	const Uint8* state = SDL_GetKeyboardState(nullptr);
	if (state[SDL_SCANCODE_RIGHT] || state[SDL_SCANCODE_D]) {
		camPanX = camSpeed;
	}
	else if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_A]) {
		camPanX = -camSpeed;
	}
	else {
		camPanX = 0;
	}

	if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_W]) {
		camPanZ = camSpeed;
	}
	else if (state[SDL_SCANCODE_DOWN] || state[SDL_SCANCODE_S]) {
		camPanZ = -camSpeed;
	}
	else {
		camPanZ = 0;
	}

	if (state[SDL_SCANCODE_KP_PLUS]) {
		camPanY = camSpeed;
	}
	else if (state[SDL_SCANCODE_KP_MINUS]) {
		camPanY = -camSpeed;
	}
	else {
		camPanY = 0;
	}

	for (auto fe : frameEvents) {
		if (fe.type == SDL_MOUSEWHEEL) {
			if (fe.wheel.y > 0) {
				camPanY += camSpeed;
			}
			if (fe.wheel.y < 0) {
				camPanY -= camSpeed;
			}
		}
	}
}

PRIV void moveDebugCamera(std::vector<SDL_Event>& frameEvents) {

	const Uint8* state = SDL_GetKeyboardState(nullptr);
	if (state[SDL_SCANCODE_RIGHT] || state[SDL_SCANCODE_D]) {
		camPanX = camSpeed;
	}
	else if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_A]) {
		camPanX = -camSpeed;
	}
	else {
		camPanX = 0;
	}

	if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_W]) {
		camMoveFwd = camSpeed;
	}
	else if (state[SDL_SCANCODE_DOWN] || state[SDL_SCANCODE_S]) {
		camMoveFwd = -camSpeed;
	}
	else {
		camMoveFwd = 0;
	}

	if (state[SDL_SCANCODE_KP_8]) {
		cameraPitch = camSpeed  * 0.01;
	}
	else if (state[SDL_SCANCODE_KP_2]) {
		cameraPitch = -camSpeed * 0.01;
	}
	else {
		cameraPitch = 0;
	}


	if (state[SDL_SCANCODE_KP_PLUS]) {
		camPanY = camSpeed;
	}
	else if (state[SDL_SCANCODE_KP_MINUS]) {
		camPanY = -camSpeed;
	}
	else {
		camPanY = 0;
	}

	if (state[SDL_SCANCODE_E]) {
		cameraYaw = -0.004f;;
	}
	else if (state[SDL_SCANCODE_Q]) {
		cameraYaw = 0.004f;;
	}
	else {
		cameraYaw = 0;
	}

	for (auto fe : frameEvents) {
		if (fe.type == SDL_MOUSEWHEEL) {
			if (fe.wheel.y > 0) {
				camPanY += camSpeed;
			}
			if (fe.wheel.y < 0) {
				camPanY -= camSpeed;
			}
		}
	}

}

PRIV uint32_t readPixel(int x, int y) {
	uint32_t data = 0;
	unsigned char* pixels = new unsigned char[3];

	glBindFramebuffer(GL_FRAMEBUFFER, pickMapFbo);
	glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixels);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	data = pixels[0];
	data <<= 8;
	data += pixels[1];
	data <<= 8;
	data += pixels[2];
	delete[](pixels);
	return data;
}

PRIV void update(float dt, std::vector<SDL_Event>& frameEvents) {

	for (auto fe : frameEvents) {
		if (fe.type == SDL_KEYDOWN) {
			// if F10 is pressed, we toggle the debug mode

			// Not used now: listen for CTRL-0
			// using the key modifiers.
			//SDL_Keymod mod = SDL_GetModState();
			//if (mod & KMOD_RCTRL && fe.key.keysym.sym == SDLK_0) {
			if (fe.key.keysym.sym == SDLK_F10) {
				debugMode = !debugMode;
				//activeMainCam = debugCamera;
			}
		}

		if (fe.type == SDL_MOUSEBUTTONDOWN) {
			if (fe.button.button == SDL_BUTTON_LEFT) {
				if (gameState == GameState::GS_InGame) {
					int mx, my;
					mouseXY(mx, my);
					uint32_t data = readPixel(mx, my);
					uint8_t x = (data >> 8) & 0xff;
					
				}
			}
			else if (fe.button.button == SDL_BUTTON_RIGHT) {
				if (gameState == GameState::GS_InGame) {
					if (dragState == DS_OK) {
						dragState = DS_None;
						dummyDragGameObject->_textureName = "lp-palette";
						gameObjects.push_back(dummyDragGameObject);
						dummyDragGameObject = nullptr;
					}
				}
			}

		}
	}

	if (debugMode) {
		moveDebugCamera(frameEvents);
	}
	else {
		//moveGamePlayCamera(frameEvents);
		updateDrone(frameEvents, dt);
	}

	if (gameState == GameState::GS_InGame) {
		// todo(gru)
	}

}

PRIV double splashTimePassed = 0;
PRIV double splashTimeDuration = 2;
PRIV float splashAlpha = 1;
PRIV void renderEngineSplash(Uint64 frameTimeGross) {
	// We just wait some frames to warm up
	if (frames < 10) {
		return;
	}
	double ftInSeconds = (double)frameTimeGross / 1000 / 1000;

	(double)splashTimePassed += ftInSeconds;
	if (splashTimePassed >= splashTimeDuration) {
		gameState = GameState::GS_Menu;
		return;
	}

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(400, 300, -1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(800, 600, 1));
	splashAlpha -= 0.2 * ftInSeconds;
	ri.alpha = splashAlpha;

	drawVAO(&ri);

}

PRIV void renderMainMenu() {

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["title"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(512, 384, -2));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(1024, 768, 1));
	ri.alpha = 1;

	drawVAO(&ri);

	if (doButton("btnStart", "ui-atlas", 300, 250, 3 * 64, 1 * 64, { 512, 512, 0, 7 * 64, 3 * 64, 1 * 64 })) {
		gameState = GameState::GS_InGame;
	}

	if (doButton("btnExit", "ui-atlas", 300, 180, 3 * 64, 1 * 64, { 512, 512, 3 * 64, 7 * 64, 3 * 64, 1 * 64 }) == true) {
		gameRunning = false;
	}

}

PRIV void renderPickMap() {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);

	// For this test we use a texture called offsreen
	// which is an in memory texture used as a render target for 
	// offscreen rendering, hence the name :) 
	ri.texture = (*getTextureCache())["pickMapCol"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(100, 50, 1.5f));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(300, 200, 1));

	ri.lit = false;
	ri.alpha = 1;
	drawVAO(&ri);
}

PRIV void renderCameraAxes() {
	drawText2D("camPos: " + std::to_string(activeMainCam->position().x) + "/" + std::to_string(activeMainCam->position().y) + "/" + std::to_string(activeMainCam->position().z), 5, 55, *consolasFont, 1);
	drawText2D("camFwd: " + std::to_string(activeMainCam->fwd().x) + "/" + std::to_string(activeMainCam->fwd().y) + "/" + std::to_string(activeMainCam->fwd().z), 5, 40, *consolasFont, 1, { 0, 0, 1 });
	drawText2D("camRight: " + std::to_string(activeMainCam->right().x) + "/" + std::to_string(activeMainCam->right().y) + "/" + std::to_string(activeMainCam->right().z), 5, 25, *consolasFont, 1, { 1, 0, 0 });
	drawText2D("camUp: " + std::to_string(activeMainCam->up().x) + "/" + std::to_string(activeMainCam->up().y) + "/" + std::to_string(activeMainCam->up().z), 5, 10, *consolasFont, 1, { 0, 1, 0 });

}

PRIV void renderDebugInfo(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	if (showDebugGui()) {

		// frame statistics
		drawText2D("ft gross:" + std::to_string((double)frameTimeGross), 5, ScreenHeight - 30, *consolasFont, 1, { 0, 0.7, 0 });
		drawText2D("ft net:" + std::to_string((double)frameTimeNet), 5, ScreenHeight - 15, *consolasFont, 1);

		// draw mouse coords
		int mx, my;
		mouseXY(mx, my);
		drawText2D(std::to_string(mx) + "/" + std::to_string(my), 5, ScreenHeight - 60, *consolasFont, 1);

		// selected game object
		//std::string goName = lastSelectedGameObject != nullptr ? lastSelectedGameObject->name() : " -- none --";
		drawText2D("Sel.: --" , 5, ScreenHeight - 75, *consolasFont, 1);

		renderCameraAxes();

		if (gameState == GS_InGame) {
			if (dragState != DS_None) {

				std::string rayString = "ray: "
					+ std::to_string(dragRay.direction.x)
					+ "/"
					+ std::to_string(dragRay.direction.y)
					+ "/"
					+ std::to_string(dragRay.direction.z);
				drawText2D(rayString, 700, 720, *consolasFont);

				std::string originString = "rayOrigin: "
					+ std::to_string(dragRay.origin.x)
					+ "/"
					+ std::to_string(dragRay.origin.y)
					+ "/"
					+ std::to_string(dragRay.origin.z);
				drawText2D(originString, 700, 700, *consolasFont);

				std::string intersectionString = "ip: "
					+ std::to_string(intersectionPoint.x)
					+ "/"
					+ std::to_string(intersectionPoint.y)
					+ "/"
					+ std::to_string(intersectionPoint.z);
				drawText2D(intersectionString, 700, 680, *consolasFont);
			}
		}

	}
}


PRIV void renderColoredRect(SDL_Rect dest, glm::vec3 color, float zOrder = -1) {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.fixColored = true;
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(dest.x, dest.y, zOrder));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(dest.w, dest.h, 1));
	ri.alpha = 1;
	ri.tint = color;

	drawVAO(&ri);

}


PRIV void renderTopStatusBar() {
	renderColoredRect({ ScreenWidth / 2, ScreenHeight - 24, ScreenWidth, 48 }, { 0.6, 0.3, 0.1 }, -3);
	Image image((*getTextureCache())["ui-atlas"], 32, 32);
	drawImage2D({ 550, 730, 32, 32 }, image, { 512, 512, 12 * 32, 15 * 32, 32, 32 });
	drawImage2D({ 700, 730, 32, 32 }, image, { 512, 512, 13 * 32, 15 * 32, 32, 32 });
	drawImage2D({ 850, 730, 32, 32 }, image, { 512, 512, 14 * 32, 15 * 32, 32, 32 });
}

PRIV void renderInGameHud() {
	//renderTopStatusBar();
	drawText2D("turret angle:" + std::to_string((double)turretAngle), 5, ScreenHeight - 80, *consolasFont, 1, { 0.2, 0.1, 0 });
	std::string ipstr = std::to_string(intersectionPoint.x) + "/" + std::to_string(intersectionPoint.y) + "/" + std::to_string(intersectionPoint.z);
	drawText2D("intersection point:" + ipstr, 5, ScreenHeight - 100, *consolasFont, 1, { 0.1, 0.2, 0 });

}

PRIV void render2D(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	if (gameState == GameState::GS_Splash) {
		renderEngineSplash(frameTimeGross);
	}

	if (gameState == GameState::GS_Menu) {
		renderMainMenu();
	}

	if (gameState == GameState::GS_InGame) {
		//	renderPickMap();
		renderInGameHud();
	}

	renderDebugInfo(frameTimeGross, frameTimeNet);

}

// 100 lines grid
PRIV VAOData setupGrid() {
	std::vector<float> vertices;
	std::vector<int> indices;
	
	
	int startH = -50;
	int endH = 50;
	int startV = 50;
	int endV = -50;

	int indexCounter = 0;
	for (int i = 0; i < (endH - startH); i++) {
		vertices.push_back(startH + i);
		vertices.push_back(0);
		vertices.push_back(startV);
		vertices.push_back(startH + i);
		vertices.push_back(0);
		vertices.push_back(endV);

		indices.push_back(indexCounter);
		indices.push_back(indexCounter + 1);
		indexCounter += 2;
	}
 

	return createVAO(vertices, {}, {}, indices);
}

PRIV void renderGrid() {
	
	
	RenderInfo ri;
	ri.vaodata = gridVAOData;
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = gridVAOData.number_of_indices;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = activeMainCam->view();
	ri.projectionMatrix = activeMainCam->proj();
	ri.texture = 0;
	ri.fixColored = true;
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(5, 1, 1));
	ri.alpha = 1;
	ri.tint = { 0.7, 0, 0 };
	ri.drawMode = GL_LINES;

	drawVAO(&ri);
	ri.modelMatrix = glm::scale(glm::mat4(1), glm::vec3(1, 1, 1));
	ri.tint = { 0.3, 0.1, 0.1 };
	drawVAO(&ri);

	ri.modelMatrix = glm::rotate(glm::mat4(1), glm::pi<float>()/2.0f, glm::vec3(0, 1, 0));
	ri.tint = { 0.1, 0.3, 0.1 };
	drawVAO(&ri);


}

PRIV void render3D() {
	//glm::vec3 pos = activeMainCam->position() + glm::vec3(0, camPanY, 0) + activeMainCam->right() * camPanX +
	//		glm::vec3(activeMainCam->fwd().x, 0, activeMainCam->fwd().z) * camPanZ + activeMainCam->fwd() * camMoveFwd;

	// Make the camera follow the player drone
	glm::vec3 pos = gameObjectNameMap["droneBody"]->position() + glm::vec3 {0, 35, 0.1};

	// Pitch
	glm::mat4 rotMatPitch = glm::rotate(glm::mat4(1), cameraPitch, glm::vec3(activeMainCam->right()));
	glm::vec3 fwd = glm::vec3(rotMatPitch * glm::vec4(activeMainCam->fwd(), 1.0));

	// Yaw
	glm::mat4 rotMat = glm::rotate(glm::mat4(1), cameraYaw, { 0, 1, 0 }/*glm::vec3(activeMainCam->up())*/);
	fwd = glm::vec3(rotMat * glm::vec4(fwd, 1.0));
	glm::vec3 right = glm::vec3(rotMat * glm::vec4(activeMainCam->right(), 1.0));

	activeMainCam->updateTransform(pos, fwd, right);

	// Render our shadow map
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFbo);
	glViewport(0, 0, 1024, 1024);
	GLfloat depthClear = 1;
	glClearBufferfv(GL_DEPTH, 0, &depthClear);
	for (auto gameObject : gameObjects) {
		gameObject->render(shadowMapCamera, shadowMapCamera, "shadowmap", glm::vec3(0), false);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, pickMapFbo);
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	GLfloat c[] = { 0, 0, 0, 1 };
	glClearBufferfv(GL_COLOR, 0, c);
	GLfloat d = 1;
	glClearBufferfv(GL_DEPTH, 0, &d);
	for (auto gameObject : gameObjects) {
		float val = (gameObject->id() / 255.0f);
		gameObject->renderWithColorUnlit(activeMainCam, glm::vec3(val, val, val));
	}

	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.05, 0.01, 0.01);

	for (auto gameObject : gameObjects) {
		gameObject->render(activeMainCam, shadowMapCamera, getShaderCache()->get("default"), sunDirection, glm::vec3(0), false);
	}

	if (dragState != DS_None) {
		dummyDragGameObject->setPosition(intersectionPoint);
		dummyDragGameObject->
			//renderWithColorUnlit(activeMainCam, { 0.3, 1.0, 0.4 });
			render(activeMainCam, shadowMapCamera, getShaderCache()->get("default"), sunDirection, glm::vec3(0), false, true);
	}

	if (debugMode || !debugMode) {
		renderGrid();
	}

}

PRIV void render(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.07, 0.03, 0.03);
	if (gameState == GameState::GS_InGame) {
		render3D();
	}
	render2D(frameTimeGross, frameTimeNet);
}

PRIV void mathTests() {
	glm::vec3 planeNormal(0, 1, 0);
	glm::vec3 planePoint(100, 0, -50);

	glm::vec3 rayOrigin = { -5, 10, 2 };
	Ray ray = { {rayOrigin.x , rayOrigin.y, rayOrigin.z}, { 0, -1,  0 }, 100.0f };

	glm::vec3 ip;
	if (!rayIntersectsPlane(planeNormal, planePoint, ray, ip)) {
		printf("rayintersection test failed!\n");
		exit(1);
	}

	if (ip.x != rayOrigin.x || ip.y != 0 || ip.z != rayOrigin.z) {
		printf("ray intersection point wrong: %f/%f/%f\n", ip.x, ip.y, ip.z);
		exit(1);
	}

	ray.direction = { 0, 0, -1 };
	if (rayIntersectsPlane(planeNormal, planePoint, ray, ip)) {
		printf("rayintersection test failed!\n");
		exit(1);
	}
}

int main4(int argc, char** args) {
	
	createWindow(ScreenWidth, ScreenHeight, false);
	HWND hwnd = getNativeWindow();
	soundMachine = new SoundMachine(hwnd);
	shotSound = soundMachine->createDummySound(0.15, 440);
	deepSound = soundMachine->createDummySound(0.30, 110);
	soundMachine->playSound(deepSound);

	init();

	SDL_Event event;

	std::vector<SDL_Event> frameEvents;

	gameState = GameState::GS_Splash;

	Timer timer_gross;
	Timer timer_net;

	
	while (gameRunning) {
		timer_net.start();
		frameEvents.clear();
		SDL_Event event;

		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				gameRunning = false;
				break;
			}


			if (event.type == SDL_KEYDOWN) {
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					gameRunning = false;
					break;
				}
			}

			frameEvents.push_back(event);
		}

		updateUIState(frameEvents);
		update(timer_gross.get_last_measure(microseconds), frameEvents);
		render(timer_gross.get_last_measure(microseconds), timer_net.get_last_measure(microseconds));
		timer_net.stop();

		flipBuffer();
		frames++;
		timer_gross.stop();
		timer_gross.start();

	}

	return 0;
}


