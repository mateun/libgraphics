#pragma once
#include <wtypes.h>
#include <string>

enum Event {
	QUIT, 
	KEYPRESS,
	MOUSEMOVE, 
	NONE
};

int getScreenWidth();
int getScreenHeight();
HWND getNativeWindow();

void createWindow(int w, int h, bool fullScreen, bool vSync = true);
void createWindow(int w, int h, bool fullScreen, const std::string& title, bool vSync = true);
void destroyWindow();
Event pollEvent();
void flipBuffer();
	
