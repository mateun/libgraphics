#pragma once
#include <vector>
#include "game_object.h"

enum ChildType {
	TopLeft, 
	TopRight,
	BottomLeft, 
	BottomRight
};

class QTNode {

public:
	QTNode(float w, float h, float x, float y, GameObject* gameObject);
	QTNode(float w, float h, float x, float y);

	bool doesObjectFitIntoNode(GameObject* go);
	void setGameObject(GameObject* gameObject) { _gameObject = gameObject; }
	bool hasGameObject() { return _gameObject != nullptr; }
	GameObject* getGameObjec() { return _gameObject; }
	QTNode* getParent() { return _parent; }
	void setParent(QTNode* p) { _parent = p; }
	std::vector<QTNode*>& children() { return _children; }
	bool isLeaf() { return _children.empty(); }

	float w() { return _w; }
	float h() { return _h; }
	float x() { return _x; }
	float y() { return _y; }

private:
	std::vector<QTNode*> _children;
	QTNode* _parent = nullptr;
	float _w; 
	float _h;
	float _x; 
	float _y;
	GameObject* _gameObject = nullptr;

};

class QuadTree {

public: 
	QuadTree(float w, float h, float x, float y, std::vector<GameObject*> initialGameObjects);
	void addGameObject(GameObject* go, QTNode* entryNode);
	void removeGameObject(GameObject* go);
	void recompute();
	QTNode* root() { return _root; }

private: 
	QTNode* _root;
};