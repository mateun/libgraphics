//
// Created by martin on 20.09.20.
//

#include "render.h"
#include "image.h"
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL.h>
#include "window.h"
#include "shader.h"
#include "shader_cache.h"
#include "texture_cache.h"
#include "texture.h"

extern int ScreenWidth, ScreenHeight;

void drawImage2D(SDL_Rect targetRect, const Image& image, SpriteAtlasItemDescriptor sad) {

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAOInstance(sad.getUVs());
	ri.shader = getShaderCache()->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture =image._texture;
		

	int posX = (double)targetRect.w / 2 + targetRect.x;
	int posY = (double)targetRect.h / 2 + targetRect.y;

	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(posX, posY, -1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(targetRect.w, targetRect.h, 1));
	ri.alpha = 1;

	glEnable(GL_BLEND);
	drawVAO(&ri);
	glDisable(GL_BLEND);
	deleteVAOData(ri.vaodata);

}

