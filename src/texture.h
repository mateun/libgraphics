#pragma once
#include "SDL2/SDL.h"
#include "GL/glew.h"
#include <string>

class ImageData;

GLuint createTextureFromSurface(ImageData* surface, bool transparent);
GLuint createTexture(const std::string& fileName);
GLuint createColorTextureInMem(int w, int h);
GLuint createDepthTextureInMem(int w, int h);
void attachColorAndDepthTexturesToFramebuffer(GLuint fbo, GLuint colorTexture, GLuint depthTexture);
void attachDepthTextureToFramebuffer(GLuint fbo, GLuint depthTexture);
