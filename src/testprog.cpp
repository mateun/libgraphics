#include "../include/graphics.h"
#include "stdio.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "math.h"
#include <vector>
#include "game_object.h"
#include "texture_cache.h"
#include "camera.h"
#include "ImageImporter.h"
#include "Font.h"

class DGO {

public:
    DGO() {
        SDL_Log("in ctr. of DGO\n");
    }
    DGO(const DGO& d) {
        SDL_Log("in cp ctr. of DGO\n");
    }
    ~DGO() {
        SDL_Log("in dtr. of DGO\n");
    }

};

std::vector<GameObject*>* gameObjects;
ShaderCache* shaderCache;
GLuint fbo;
GLuint shadowFbo;
GameObject* enemy1 = nullptr;
GameObject* enemy2 = nullptr;
GameObject* ground = nullptr;
const int ScreenWidth = 1024;
const int ScreenHeight = 768;
Font* notoFont = nullptr;



void mathTests() {
	glm::vec3 planeNormal(0,1,0);
	glm::vec3 planePoint(100, 0, -50);

	Ray ray = { {0,-5, -1 }, { 0, 10, -1 }, 100.0f };

	glm::vec3 rayOrigin(20, 10, 5);
	glm::vec3 intersectionPoint;
	if (!rayIntersectsPlane(planeNormal, planePoint, ray, intersectionPoint)) {
		printf("rayintersection test failed!\n");
		exit(1);
	}
	
	ray.direction = { 0, 0, -1 };
	if (rayIntersectsPlane(planeNormal, planePoint, ray, intersectionPoint)) {
		printf("rayintersection test failed!\n");
		exit(1);
	}	
}

void init() {
    gameObjects = new std::vector<GameObject*>();
	createWindow(ScreenWidth, ScreenHeight, false);
	
	GLuint stoneTexture = createTexture ("../test_assets/generic_texture.bmp");
	(*getTextureCache())["stone"] = stoneTexture;
	
	GLuint flowerTexture = createTexture ("../test_assets/flowers.bmp");
	(*getTextureCache())["flowers"] = flowerTexture;
	
	GLuint grassTexture = createTexture ("../test_assets/grass_texture.bmp");
	(*getTextureCache())["grass"] = grassTexture;

	ImageData* notoSurface= new ImageData();
	ImageImporter::importPNGFile("../test_assets/noto.png", notoSurface);
	GLuint notoTexture = createTextureFromSurface(notoSurface, true);
	(*getTextureCache())["noto-font"] = notoTexture;
	notoFont = new Font("noto", "noto-font");

	ImageData* spriteSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/sprite.png", spriteSurface);
	GLuint sprite = createTextureFromSurface(spriteSurface, true);
	(*getTextureCache())["sprite"] = sprite;
	
	delete(notoSurface->pixels);
	delete(notoSurface);
	delete(spriteSurface->pixels);
	delete(spriteSurface);
	
    GLuint redTexture = createTexture ("../test_assets/red.bmp");
    (*getTextureCache())["red"] = redTexture;

    GLuint blueTexture = createTexture ("../test_assets/blue.bmp");
    (*getTextureCache())["blue"] = blueTexture;

	Mesh* boxMesh = importToVAO("../test_assets/box4.obj");
	Mesh* boxMesh2 = importToVAO("../test_assets/box2.obj");
	Mesh* wall = importToVAO("../test_assets/wall.obj");
    Mesh* arenaMesh = importToVAO("../test_assets/arena.obj");
    Mesh* tm = importToVAO("../test_assets/tank1.obj");

	GLuint vs = createVertexShader("../test_shaders/vs.glsl");
	GLuint fs = createFragmentShader("../test_shaders/fs.glsl");
 	GLuint shaderProg = createShaderProgram(vs, fs);

    GLuint shadowmap_vs = createVertexShader("../test_shaders/shadowmap-vs.glsl");
    GLuint shadowmap_fs = createFragmentShader("../test_shaders/shadowmap-fs.glsl");
    GLuint shadowMapshaderProg = createShaderProgram(shadowmap_vs, shadowmap_fs);

	shaderCache = getShaderCache();
	shaderCache->put("default", shaderProg);
    shaderCache->put("shadowmap", shadowMapshaderProg);

	enemy1 = new GameObject(0, "go", glm::vec3(8,0, 5), tm);
	enemy2 = new GameObject(0, "go", glm::vec3(-15, 0, -6), tm);
	ground = new GameObject(0, "go", glm::vec3(0, -10, 0), arenaMesh);

	enemy1->_textureName = "flowers";
	enemy2->_textureName = "blue";
	ground->_textureName = "stone";
	
	gameObjects->push_back(enemy1);
	gameObjects->push_back(enemy2);
	gameObjects->push_back(ground);

	fbo = createFramebuffer();
	GLuint ct = createColorTextureInMem(512, 512);
	GLuint dt = createDepthTextureInMem(512, 512);
    (*getTextureCache())["offscreen"] = ct;
	attachColorAndDepthTexturesToFramebuffer(fbo, ct, dt);

	shadowFbo = createFramebuffer();
	GLuint shadowDepthTex = createDepthTextureInMem(1024, 1024);
    (*getTextureCache())["shadowDepth"] = shadowDepthTex;
	attachDepthTextureToFramebuffer(shadowFbo, shadowDepthTex);

	
}


float rot = 0;

static void update() {

		rot += 0.1f;
		enemy1->setEulerRotationY(rot);
		enemy2->scale( glm::vec3(5));
		enemy1->translate(glm::vec3(10, 0, 0));
		//ground->setEulerRotationX(90);
		ground->scale(glm::vec3(10, 10, 10));
		for (auto gameObject : *gameObjects) {
			gameObject->update(0.16f);
		}

}

void render2D() {

	// To render in 2D we can use the default shader, 
	// which is the same we use for 3D rendering.
	// In fact we just need to change the projection matrix
	// for an orthographic projection.
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.viewMatrix = glm::lookAt(glm::vec3(0,0,5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(-1, 1, -1, 1, .01, 100);
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);

	// For this test we use a texture called offsreen
	// which is an in memory texture used as a render target for 
	// offscreen rendering, hence the name :) 
	ri.texture = (*getTextureCache())["offscreen"];
	ri.modelMatrix = glm::scale(glm::mat4(1), glm::vec3(0.4));
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(-2, -2.5, 0));
	ri.lit = false;
	drawVAO(&ri);

	// Draw the shadow/depth texture
    ri.texture = (*getTextureCache())["shadowDepth"];
    ri.modelMatrix = glm::scale(glm::mat4(1), glm::vec3(0.4));
    ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(1, -2.5, 0));
    ri.lit = false;
    drawVAO(&ri);

	// We need transparency for text and sprite rendering
	glEnable(GL_BLEND);
	ri.texture = (*getTextureCache())["noto-font"];
	ri.modelMatrix = glm::translate(glm::mat4(1), glm::vec3(1, -0, 0));
    ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(0.8));
    drawVAO(&ri);

    ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
    ri.texture = (*getTextureCache())["sprite"];
    ri.modelMatrix = glm::translate(glm::mat4(1), glm::vec3(102, ScreenHeight/2, -2));
    ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(64, 64, 1));
    drawVAO(&ri);
    glDisable(GL_BLEND);

    drawText2D("Monospaced font!", 100, 300, *notoFont);
    drawText2D("yeah! ft: ", 100, 280, *notoFont);


}

void render3D() {

		// Setup some basic view and projection matrices which we use in
		// different cameras.
		glm::mat4 vTop = glm::lookAt(glm::vec3(0, 50, 7), glm::vec3(0, 0, 0), glm::vec3(0, 1,0));
		glm::mat4 vFps = glm::lookAt(glm::vec3(0, 1, 47), glm::vec3(0, 1, -1), glm::vec3(0, 1,0));
		glm::mat4 pTop = glm::perspective(1.65f, (float)(ScreenWidth/ScreenHeight), 0.1f, 300.0f);
		glm::mat4 pFps = glm::perspective(1.35f, (float)(ScreenWidth/ScreenHeight), 0.1f, 300.0f);

		// Matrices for rendering the shadow map.
		// We use an orthographic projection to simulate sunlight with parallel rays.
        glm::mat4 vShadowMap = glm::lookAt(glm::vec3(-100, 200, 100), glm::vec3(0, 0, 0), glm::vec3(0, 1,0));
        glm::mat4 pShadowMap = glm::ortho<float>(-70, 70, -70, 70, 0.1, 1000);

		const Camera *topDownCamera = new Camera(vTop, pTop);
		const Camera *fpsCamera = new Camera(vFps, pFps);
		const Camera *shadowMapCamera = new Camera(vShadowMap, pShadowMap);

		// First we do an offscreen render pass
		// from a top down view for test purposes.
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		glViewport(0, 0, 512, 512);
		GLfloat c[] = {0.5f, 0, 0, 1};
		glClearBufferfv(GL_COLOR, 0, c);
		GLfloat d = 1;
		glClearBufferfv(GL_DEPTH, 0, &d);
		for (auto gameObject : *gameObjects) {
			gameObject->render(fpsCamera, shadowMapCamera, glm::vec3(0), false);
		}

		// Next we render our shadow map
		glBindFramebuffer(GL_FRAMEBUFFER, shadowFbo);
		glViewport(0, 0, 1024, 1024);
		glClearBufferfv(GL_DEPTH, 0, &d);
		for (auto gameObject : *gameObjects) {
		    gameObject->render(shadowMapCamera, shadowMapCamera, "shadowmap", glm::vec3(0), false);

		}

		
		// Next we render everything to the backbuffer
		// as normal.
		// The offscreen texture will be used in the 2D render pass.
		glViewport(0 , 0, ScreenWidth, ScreenHeight);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		clearBackBuffer(0.0, 0, 0.0);
		for (auto gameObject : *gameObjects) {
			gameObject->render(topDownCamera, shadowMapCamera, glm::vec3(0), false);
		}

		delete(topDownCamera);
		delete(fpsCamera);
		

}


static void render() {

		render3D();
		render2D();	
		
		GLenum err = glGetError();
		if (err != 0) {
			printf("alarm!! %d\n", err);
			exit(100);
		}
		flipBuffer();
}


int _main(int argc, char** args) {
	init();

	bool gameRunning = true;
	while (gameRunning) {
		SDL_Event event;
		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				gameRunning = false;
				break;
			}


			if (event.type == SDL_KEYDOWN) {
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					gameRunning = false;
				}
			}
			

		}		

		update();
		render();

	}

	printf("ending game!\n");
	for (auto go : *gameObjects) {
	    if (go) {
	       delete(go);
	    }
	}
	delete(gameObjects);
	destroyWindow();
	SDL_Quit();

	return 0;
}
