//
// Created by martin on 04.10.20.
//

#include "ImageImporter.h"
#include <png.h>
#include <SDL2/SDL_surface.h>

void ImageImporter::importPNGFile(const std::string &fileName, ImageData* imageData) {
    FILE *fp = fopen(fileName.c_str(),"rb");
    if (!fp) {
        printf("error reading in png file.\n");
        exit(1);
    }

    uint8_t header[8];
    if (fread(header, 1, 8, fp) != 8) {
       printf("error reading png header. \n") ;
       exit(1);

    }

    bool isPng = !png_sig_cmp(header, 0, 8);
    if (!isPng) {
        printf("this is not a png file!\n");
        exit(1);
    }

    fclose(fp);

    png_structp  png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (!png_ptr) {
        exit(2);
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) exit(3);

    fp = fopen(fileName.c_str(), "rb");
    png_init_io(png_ptr, fp);

    uint32_t w, h;
    int bit_depth, color_type, interlace_type, comp_type, filter_method;
    //png_read_info(png_ptr, info_ptr);
    uint8_t** row_pointers;
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);
    png_get_IHDR(png_ptr, info_ptr, &w, &h, &bit_depth, &color_type, &interlace_type, &comp_type, &filter_method);
    printf("widht/height: %u/%u bit-depth: %d colorType: %d \n", w, h, bit_depth, color_type);
    row_pointers = png_get_rows(png_ptr, info_ptr);
    int comps = color_type == PNG_COLOR_TYPE_RGB ? 3 : 4;
    uint8_t *surfacePixelPointer = (uint8_t*)malloc(comps * w * h * 8);
    int index = 0;
    for (int r=h-1; r >= 0; r--) {
        int pixelIndex = 0;
        for (int x = 0; x < w; x++) {
            uint8_t* rowBytes = row_pointers[r];
            surfacePixelPointer[index] = rowBytes[comps * x];
            surfacePixelPointer[index+1] = rowBytes[comps * x+1];
            surfacePixelPointer[index+2] = rowBytes[comps * x+2];
            if (comps == 4) {
                surfacePixelPointer[index+3] = rowBytes[comps * x+3];
            }
            index += comps;
        }
    }

    fclose(fp);

    imageData->pixels = surfacePixelPointer;
    imageData->w = w;
    imageData->h = h;

}
