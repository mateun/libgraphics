#include "../include/graphics.h"
#include "stdio.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "math.h"
#include <vector>
#include "game_object.h"
#include "texture_cache.h"
#include "camera.h"
#include "ImageImporter.h"
#include "Font.h"
#include "gui.h"
#include <set>

#define PRIV static
#define MeshCache (*getMeshCache())

PRIV enum BuildingType {
	House, 
	Barrack, 
	Mine, 
	Woodcamp
};

PRIV std::string BTName(BuildingType bt) {
	switch (bt) {
	case House: return "House";
	case Barrack: return "Barrack";
	case Mine: return "Mine";
	case Woodcamp: return "Woodcamp";
	}
}

PRIV struct BuildingCost {
	int wood;
	int stone;
	int iron;
	int gold;
};

PRIV BuildingCost getCostForBuildingType(BuildingType bt) {
	switch (bt) {
	case House: return { 10, 20, 5, 10 };
	case Barrack: return { 5, 45, 4, 3 };
	default: return { 4, 6, 6, 2 };
	}
}

PRIV enum GameState {
	GS_None,
	GS_Splash,
	GS_Menu,
	GS_InGame
};


// Drag/Drop
PRIV enum DragState {
	DS_None, 
	DS_OK, 
	DS_NOK
};

// End drag/drop




int ScreenWidth = 1024;
int ScreenHeight = 768;

PRIV Font* notoFont = nullptr;
PRIV Font* consolasFont = nullptr;
PRIV ShaderCache* shaderCache = nullptr;
PRIV std::vector<GameObject*>* gameObjects = nullptr;
PRIV GLuint shadowFbo;
PRIV GLuint pickMapFbo;
PRIV Camera* topDownCamera = nullptr;
PRIV Camera* shadowMapCamera = nullptr;
PRIV Camera* debugCamera = nullptr;
PRIV Camera* activeMainCam = nullptr;
PRIV float camPanX = 0.0f;
PRIV float camPanZ = 0;
PRIV float camPanY = 0;
PRIV float camMoveFwd = 0;
PRIV bool debugMode = false;
PRIV int frames = 0;
PRIV GameState gameState = GS_None;
PRIV glm::vec3 sunDirection = glm::vec3(-0.5, -0.6, -.3);
PRIV bool gameRunning = true;
PRIV DragState dragState = DS_None;
PRIV Ray dragRay;
PRIV glm::vec3 intersectionPoint;
PRIV GameObject* dummyDragGameObject;
PRIV float cameraYaw = 0;
PRIV float cameraPitch = 0;
PRIV const float camSpeed = 0.5f;

// Hud variables
PRIV bool showBuildMenu = false;

// This global id is incremented each time a game object is created
PRIV uint8_t gameObjectId = 1;
PRIV std::map<uint8_t, GameObject*> gameObjectMap;
PRIV GameObject* lastSelectedGameObject = nullptr;


// End raypicking

PRIV void updateDragOperation() {
	int mx, my;
	mouseXY(mx, my);
	dragRay = createRayFromScreenCoordinates(mx, my, topDownCamera, ScreenWidth, ScreenHeight);
	rayIntersectsPlane({ 0, 1, 0 }, { 10, 0, 0 }, dragRay, intersectionPoint);
	
}

bool showDebugGui() {
	return debugMode == true;
}



void initFramebuffers() {
	shadowFbo = createFramebuffer();
	GLuint shadowDepthTex = createDepthTextureInMem(1024, 1024);
	(*getTextureCache())["shadowDepth"] = shadowDepthTex;
	attachDepthTextureToFramebuffer(shadowFbo, shadowDepthTex);

	pickMapFbo = createFramebuffer();
	GLuint pickmapColTex = createColorTextureInMem(ScreenWidth, ScreenHeight);
	GLuint pickmapDepthText = createDepthTextureInMem(ScreenWidth, ScreenHeight);
	(*getTextureCache())["pickMapCol"] = pickmapColTex;
	attachColorAndDepthTexturesToFramebuffer(pickMapFbo, pickmapColTex, pickmapDepthText);
}

void initShaders() {
	GLuint vs = createVertexShader("../test_shaders/vs.glsl");
	GLuint fs = createFragmentShader("../test_shaders/fs.glsl");
	GLuint shaderProg = createShaderProgram(vs, fs);

	GLuint shadowmap_vs = createVertexShader("../test_shaders/shadowmap-vs.glsl");
	GLuint shadowmap_fs = createFragmentShader("../test_shaders/shadowmap-fs.glsl");
	GLuint shadowMapshaderProg = createShaderProgram(shadowmap_vs, shadowmap_fs);

	GLuint fsText = createFragmentShader("../test_shaders/fs-text.glsl");
	GLuint textRenderShaderProg = createShaderProgram(vs, fsText);
	
	shaderCache = getShaderCache();
	shaderCache->put("default", shaderProg);
	shaderCache->put("shadowmap", shadowMapshaderProg);
	shaderCache->put("text", textRenderShaderProg);
}

void initCameras() {
	glm::mat4 vTopDown = glm::lookAt(glm::vec3(0, 30, 5), glm::vec3(0, 2, -1), glm::vec3(0, 1, 0));
	glm::mat4 pTopDown = glm::perspective(1.04f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);

	glm::mat4 vDebugCam = glm::lookAt(glm::vec3(0, 1.8f, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pDebugCam = glm::perspective(1.65f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);

	// Matrices for rendering the shadow map.
	// We use an orthographic projection to simulate sunlight with parallel rays.
	glm::mat4 vShadowMap = glm::lookAt(sunDirection * -20.0f, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pShadowMap = glm::ortho<float>(-65, 65, -65, 65, 0.1, 1000);

	//topDownCamera = new Camera(vTopDown, pTopDown);
	topDownCamera = new Camera(glm::vec3(-10, 25, 15), glm::vec3(0.5, -0.8, -1), glm::cross(glm::vec3{ -10, -25,15 }, { 0.5, -0.8f, -1 }), pTopDown);
	shadowMapCamera = new Camera(vShadowMap, pShadowMap);
	debugCamera = new Camera(vDebugCam, pDebugCam);

	activeMainCam = topDownCamera;
}

PRIV void initTextures() {
	GLuint stoneTexture = createTexture("../test_assets/generic_texture.bmp");
	(*getTextureCache())["stone"] = stoneTexture;

	GLuint redTexture = createTexture("../test_assets/red.bmp");
	(*getTextureCache())["red"] = redTexture;

	GLuint blueTexture = createTexture("../test_assets/blue.bmp");
	(*getTextureCache())["blue"] = blueTexture;

	ImageData* notoSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/noto.png", notoSurface);
	GLuint notoTexture = createTextureFromSurface(notoSurface, true);
	(*getTextureCache())["noto-font"] = notoTexture;
	notoFont = new Font("noto", "noto-font");
	delete(notoSurface->pixels);
	delete(notoSurface);

	ImageData* consolasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/consolas.png", consolasSurface);
	GLuint consolasTexture = createTextureFromSurface(consolasSurface, true);
	(*getTextureCache())["consolas-font"] = consolasTexture;
	consolasFont = new Font("consolas", "consolas-font");
	delete(consolasSurface->pixels);
	delete(consolasSurface);

	ImageData* spriteSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/sprite.png", spriteSurface);
	GLuint sprite = createTextureFromSurface(spriteSurface, true);
	(*getTextureCache())["sprite"] = sprite;
	delete(spriteSurface->pixels);
	delete(spriteSurface);

	ImageData* engineSplashSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/piglet-engine-splash.png", engineSplashSurf);
	GLuint engineSplash = createTextureFromSurface(engineSplashSurf, true);
	(*getTextureCache())["engine-splash"] = engineSplash;
	delete(engineSplashSurf->pixels);
	delete(engineSplashSurf);

	ImageData* mainMenuSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/koenig-title.png", mainMenuSurface);
	GLuint mainMenuTexture = createTextureFromSurface(mainMenuSurface, true);
	(*getTextureCache())["koenig-title"] = mainMenuTexture;
	delete(mainMenuSurface->pixels);
	delete(mainMenuSurface);

	ImageData* uiAtlasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/ui-sprite-atlas.png", uiAtlasSurface);
	GLuint uiAtlasTexture = createTextureFromSurface(uiAtlasSurface, true);
	(*getTextureCache())["ui-atlas"] = uiAtlasTexture;
	delete(uiAtlasSurface->pixels);
	delete(uiAtlasSurface);

	ImageData* lpPalSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/lp-palette.png", lpPalSurface);
	GLuint lpPalTexture = createTextureFromSurface(lpPalSurface, true);
	(*getTextureCache())["lp-palette"] = lpPalTexture;
	delete(lpPalSurface->pixels);
	delete(lpPalSurface);


	ImageData* testFigureSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/figure_diffuse.png", testFigureSurface);
	GLuint fdTexture = createTextureFromSurface(testFigureSurface, true);
	(*getTextureCache())["testfigure-diffuse"] = fdTexture;
	delete(testFigureSurface->pixels);
	delete(testFigureSurface);

	ImageData* greenSemiSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/green_semitransparent.png", greenSemiSurf);
	GLuint greenSemiTexture = createTextureFromSurface(greenSemiSurf, true);
	(*getTextureCache())["green-semitransparent"] = greenSemiTexture;
	delete(greenSemiSurf->pixels);
	delete(greenSemiSurf);

}

PRIV void createNewDummyDragGameObject() {
	dummyDragGameObject = new GameObject(gameObjectId++, "dummyDrag" + std::to_string(gameObjectId), glm::vec3(0, 0, 0), nullptr);
	dummyDragGameObject->_textureName = "green-semitransparent";
}


PRIV void init() {

	initTextures();

	Mesh* arenaMesh = importToVAO("../test_assets/arena.obj");
	Mesh* tankMesh = importToVAO("../test_assets/tank1.obj");
	Mesh* palmMesh = importToVAO("../test_assets/palmtree.obj");
	Mesh* houseMesh = importToVAO("../test_assets/house_lp1.obj");
	Mesh* testfigureMesh = importToVAO("../test_assets/plane.obj");
	Mesh* terrainMesh = importToVAO("../test_assets/terrain.obj");

	MeshCache["arena"] = arenaMesh;
	MeshCache["tank"] = tankMesh;
	MeshCache["palm"] = palmMesh;
	MeshCache["house"] = houseMesh;
	MeshCache["testFigure"] = testfigureMesh;

	initShaders();

	//Mesh* tankMesh = new Mesh{ tm.vao, tm.numberOfVertices };
	//Mesh* arenaMesh = new Mesh{ am.vao, am.numberOfVertices };
	//Mesh* palmMesh = new Mesh{ palmM.vao, palmM.numberOfVertices };
	//Mesh* houseMesh = new Mesh{ houseM.vao, houseM.numberOfVertices };
	//Mesh* testfigureMesh = new Mesh{ testfigureM.vao, testfigureM.numberOfVertices };
	//Mesh* terrainMesh = new Mesh{ terrainM.vao, terrainM.numberOfVertices };

	GameObject* enemy1 = new GameObject(gameObjectId++, "enemy1", glm::vec3(-1, 3, 4), tankMesh);
	GameObject* enemy2 = new GameObject(gameObjectId++, "enemy2", glm::vec3(-8, 3, 0), tankMesh);
	GameObject* arena = new GameObject(gameObjectId++, "arena", glm::vec3(0, 0, 0), arenaMesh);
	GameObject* arenaLeft = new GameObject(gameObjectId++, "arenaLeft", glm::vec3(-10, 0, 0), arenaMesh);
	GameObject* arenaTop = new GameObject(gameObjectId++, "arenaTop", glm::vec3(0, 0, -10), arenaMesh);
	GameObject* palmGo = new GameObject(gameObjectId++, "palmGo", glm::vec3(-5, 0, -12), palmMesh);
	GameObject* houseGo = new GameObject(gameObjectId++, "house", glm::vec3(-6, 0, -10), houseMesh);
	GameObject* testFigureGo = new GameObject(gameObjectId++, "testfigure", glm::vec3(2, 1, 4), testfigureMesh);
	GameObject* terrainGo = new GameObject(gameObjectId++, "terrain", glm::vec3(0, 0, 0), terrainMesh);
	

	enemy1->_textureName = "red";
	enemy2->_textureName = "blue";
	arena->_textureName = "stone";
	arenaLeft->_textureName = "stone";
	arenaTop->_textureName = "stone";
	palmGo->_textureName = "lp-palette";
	houseGo->_textureName = "lp-palette";
	testFigureGo->_textureName = "lp-palette";
	terrainGo->_textureName = "stone";
	
	
	arena->scale(glm::vec3(4));
	arenaLeft->scale(glm::vec3(4));
	arenaTop->scale(glm::vec3(4));
	houseGo->scale(glm::vec3(1));
	testFigureGo->scale(glm::vec3(2));
	terrainGo->scale( glm::vec3(2,1,2));

	gameObjects = new std::vector<GameObject*>();
	gameObjects->push_back(enemy1);
	gameObjects->push_back(enemy2);
	//gameObjects->push_back(arena);
	//gameObjects->push_back(arenaLeft);
	//gameObjects->push_back(arenaTop);
	gameObjects->push_back(palmGo);
	gameObjects->push_back(houseGo);
	gameObjects->push_back(testFigureGo);
	gameObjects->push_back(terrainGo);

	// Fill in the map with the game object ids
	gameObjectMap[enemy1->id()] = enemy1;
	gameObjectMap[enemy2->id()] = enemy2;
	gameObjectMap[arena->id()] = arena;
	gameObjectMap[arenaLeft->id()] = arenaLeft;
	gameObjectMap[arenaTop->id()] = arenaTop;
	gameObjectMap[palmGo->id()] = palmGo;
	gameObjectMap[houseGo->id()] = houseGo;
	gameObjectMap[testFigureGo->id()] = testFigureGo;
	gameObjectMap[terrainGo->id()] = terrainGo;

	initFramebuffers();

	initCameras();

}

void moveGamePlayCamera(std::vector<SDL_Event>& frameEvents) {
	
	const Uint8* state = SDL_GetKeyboardState(nullptr);
	if (state[SDL_SCANCODE_RIGHT] || state[SDL_SCANCODE_D]) {
		camPanX = camSpeed;
	} else if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_A]) {
		camPanX = -camSpeed;
	} else {
		camPanX = 0;
	}

	if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_W]) {
		camPanZ = camSpeed;
	} else if (state[SDL_SCANCODE_DOWN] || state[SDL_SCANCODE_S]) {
		camPanZ = -camSpeed;
	} else {
		camPanZ = 0;
	}

	//if (state[SDL_SCANCODE_KP_8]) {
	//	cameraPitch = camSpeed;
	//} else if (state[SDL_SCANCODE_KP_2]) {
	//	cameraPitch = -camSpeed;
	//} else {
	//	cameraPitch = 0;
	//}


	if (state[SDL_SCANCODE_KP_PLUS]) {
		camPanY = camSpeed;
	} else if (state[SDL_SCANCODE_KP_MINUS]) {
		camPanY = -camSpeed;
	} else {
		camPanY = 0;
	}

	/*if (state[SDL_SCANCODE_E]) {
		cameraYaw = -0.004f;;
	} else if (state[SDL_SCANCODE_Q]) {
		cameraYaw = 0.004f;;
	} else {
		cameraYaw = 0;
	}*/

	for (auto fe : frameEvents) {
		if (fe.type == SDL_MOUSEWHEEL) {
			if (fe.wheel.y > 0) {
				camPanY += camSpeed;
			}
			if (fe.wheel.y < 0) {
				camPanY -= camSpeed;
			}
		}
	}
}

void moveDebugCamera(std::vector<SDL_Event>& frameEvents) {
	
	const Uint8* state = SDL_GetKeyboardState(nullptr);
	if (state[SDL_SCANCODE_RIGHT] || state[SDL_SCANCODE_D]) {
		camPanX = camSpeed;
	}
	else if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_A]) {
		camPanX = -camSpeed;
	}
	else {
		camPanX = 0;
	}

	if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_W]) {
		camMoveFwd = camSpeed;
	}
	else if (state[SDL_SCANCODE_DOWN] || state[SDL_SCANCODE_S]) {
		camMoveFwd = -camSpeed;
	}
	else {
		camMoveFwd = 0;
	}

	if (state[SDL_SCANCODE_KP_8]) {
		cameraPitch = camSpeed;
	}
	else if (state[SDL_SCANCODE_KP_2]) {
		cameraPitch = -camSpeed;
	}
	else {
		cameraPitch = 0;
	}


	if (state[SDL_SCANCODE_KP_PLUS]) {
		camPanY = camSpeed;
	}
	else if (state[SDL_SCANCODE_KP_MINUS]) {
		camPanY = -camSpeed;
	}
	else {
		camPanY = 0;
	}

	if (state[SDL_SCANCODE_E]) {
		cameraYaw = -0.004f;;
	}
	else if (state[SDL_SCANCODE_Q]) {
		cameraYaw = 0.004f;;
	}
	else {
		cameraYaw = 0;
	}

	for (auto fe : frameEvents) {
		if (fe.type == SDL_MOUSEWHEEL) {
			if (fe.wheel.y > 0) {
				camPanY += camSpeed;
			}
			if (fe.wheel.y < 0) {
				camPanY -= camSpeed;
			}
		}
	}

}

PRIV uint32_t readPixel(int x, int y) {
	uint32_t data = 0;
	unsigned char *pixels = new unsigned char[3];

	glBindFramebuffer(GL_FRAMEBUFFER, pickMapFbo);
	glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixels);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	data = pixels[0];
	data <<= 8;
	data += pixels[1];
	data <<= 8;
	data += pixels[2];
	delete[](pixels);
	return data;
}

PRIV void update(float dt, std::vector<SDL_Event>& frameEvents) {
	
	for (auto fe : frameEvents) {
		if (fe.type == SDL_KEYDOWN) {
			// if F10 is pressed, we toggle the debug mode

			// Not used now: listen for CTRL-0
			// using the key modifiers.
			//SDL_Keymod mod = SDL_GetModState();
			//if (mod & KMOD_RCTRL && fe.key.keysym.sym == SDLK_0) {
			if (fe.key.keysym.sym == SDLK_F10) {
				debugMode = !debugMode;
				//activeMainCam = debugCamera;
			}
		}

		if (fe.type == SDL_MOUSEBUTTONDOWN) {
			if (fe.button.button == SDL_BUTTON_LEFT) {
				if (gameState == GameState::GS_InGame) {
					int mx, my;
					mouseXY(mx, my);
					uint32_t data = readPixel(mx, my);
					uint8_t x = (data >> 8) & 0xff;
					lastSelectedGameObject = gameObjectMap[x];
				}
			} 
			else if (fe.button.button == SDL_BUTTON_RIGHT) {
				if (gameState == GameState::GS_InGame) {
					if (dragState == DS_OK) {
						dragState = DS_None;
						dummyDragGameObject->_textureName = "lp-palette";
						gameObjects->push_back(dummyDragGameObject);
						dummyDragGameObject = nullptr;
					}
				}
			}
			
		}

		
	}


	if (debugMode) {
		moveDebugCamera(frameEvents);
	}
	else {
		moveGamePlayCamera(frameEvents);
	}

	if (gameState == GameState::GS_InGame) {
		if (dragState != DS_None) {
			updateDragOperation();
		}
		
	}

}

PRIV double splashTimePassed = 0;
PRIV double splashTimeDuration = 2;
PRIV float splashAlpha = 1;
PRIV void renderEngineSplash(Uint64 frameTimeGross) {
	// We just wait some frames to warm up
	if (frames < 10) {
		return;
	}
	double ftInSeconds = (double) frameTimeGross / 1000 / 1000;
	
	(double) splashTimePassed += ftInSeconds;
	if (splashTimePassed >= splashTimeDuration) {
		gameState = GameState::GS_Menu;
		return;
	}

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(400, 300, -1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(800, 600, 1));
	splashAlpha -= 0.2 * ftInSeconds;
	ri.alpha = splashAlpha;

	drawVAO(&ri);
	
}

PRIV void renderMainMenu() {
	
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["koenig-title"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(400, 300, -2));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(800, 600, 1));
	ri.alpha = 1;

	drawVAO(&ri);

	if (doButton("btnStart", "ui-atlas", 300, 250, 3 * 64, 1 * 64, { 512, 512, 0, 7 * 64, 3 * 64, 1 * 64 })) {
		gameState = GameState::GS_InGame;
	}

	if (doButton("btnExit", "ui-atlas", 300, 180, 3 * 64, 1 * 64, { 512, 512, 3 * 64, 7 * 64, 3 * 64, 1 * 64 }) == true) {
		gameRunning = false;
	}

}

PRIV void renderPickMap() {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);

	// For this test we use a texture called offsreen
	// which is an in memory texture used as a render target for 
	// offscreen rendering, hence the name :) 
	ri.texture = (*getTextureCache())["pickMapCol"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(100, 50, 1.5f));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(300, 200, 1));
	
	ri.lit = false;
	ri.alpha = 1;
	drawVAO(&ri);
}

PRIV void renderCameraAxes() {
	drawText2D("camPos: " + std::to_string(activeMainCam->position().x) + "/" + std::to_string(activeMainCam->position().y) + "/" + std::to_string(activeMainCam->position().z), 5, 55, *consolasFont,1);
	drawText2D("camFwd: " + std::to_string(activeMainCam->fwd().x) + "/" + std::to_string(activeMainCam->fwd().y) + "/" + std::to_string(activeMainCam->fwd().z), 5, 40, *consolasFont, 1, { 0, 0, 1 });
	drawText2D("camRight: " + std::to_string(activeMainCam->right().x) + "/" + std::to_string(activeMainCam->right().y) + "/" + std::to_string(activeMainCam->right().z), 5, 25, *consolasFont, 1, { 1, 0, 0 });
	drawText2D("camUp: " + std::to_string(activeMainCam->up().x) + "/" + std::to_string(activeMainCam->up().y) + "/" + std::to_string(activeMainCam->up().z), 5, 10, *consolasFont, 1, { 0, 1, 0 });
	
}

PRIV void renderDebugInfo(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	if (showDebugGui()) {

		// frame statistics
		drawText2D("ft gross:" + std::to_string((double)frameTimeGross), 5, ScreenHeight - 30, *consolasFont, 1, { 0, 0.7, 0 });
		drawText2D("ft net:" + std::to_string((double)frameTimeNet), 5, ScreenHeight - 15, *consolasFont, 1);
		
		// draw mouse coords
		int mx, my;
		mouseXY(mx, my);
		drawText2D(std::to_string(mx) + "/" + std::to_string(my), 5, ScreenHeight - 60, *consolasFont, 1);

		// selected game object
		std::string goName = lastSelectedGameObject != nullptr ? lastSelectedGameObject-> name() : " -- none --";
		drawText2D("Sel.: " + goName, 5, ScreenHeight - 75, *consolasFont, 1);

		renderCameraAxes();

		if (gameState == GS_InGame) {
			if (dragState != DS_None) {
				
				std::string rayString = "ray: " 
					+ std::to_string(dragRay.direction.x)
					+ "/" 
					+ std::to_string(dragRay.direction.y)
					+ "/"
					+ std::to_string(dragRay.direction.z);
				drawText2D(rayString , 700, 720, *consolasFont);

				std::string originString = "rayOrigin: "
					+ std::to_string(dragRay.origin.x)
					+ "/"
					+ std::to_string(dragRay.origin.y)
					+ "/"
					+ std::to_string(dragRay.origin.z);
				drawText2D(originString, 700, 700, *consolasFont);

				std::string intersectionString = "ip: "
					+ std::to_string(intersectionPoint.x)
					+ "/"
					+ std::to_string(intersectionPoint.y)
					+ "/"
					+ std::to_string(intersectionPoint.z);
				drawText2D(intersectionString , 700, 680, *consolasFont);
			}
		}
		
	}
}


void renderColoredRect(SDL_Rect dest, glm::vec3 color, float zOrder = -1) {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.fixColored = true;
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(dest.x, dest.y, zOrder));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(dest.w, dest.h, 1));
	ri.alpha = 1;
	ri.tint = color;

	drawVAO(&ri);

}

PRIV void renderBuildingMenuItem(int x, int y, BuildingType bt) {
	std::string buttonName = "buildingMenu_" + BTName(bt);
	if (doButton(buttonName, "ui-atlas", x, y, 64, 32, { 512, 512, 3 * 64, 6 * 64, 3 * 64, 1 * 64 }) == true) {
		dragState = DS_OK;
		createNewDummyDragGameObject();
		dummyDragGameObject->setMesh(MeshCache["house"]);
	}

	BuildingCost cost = getCostForBuildingType(bt);
	if (isHovering(buttonName)) {
		renderColoredRect({ ScreenWidth - 355, 155, 128, 92 }, { 1, 0, 0 });
		drawText2D(BTName(bt), ScreenWidth - 410, 180, *consolasFont, 1.2);
		drawText2D("Wood: " + std::to_string(cost.wood) , ScreenWidth - 410, 165, *consolasFont, 1);
		drawText2D("Iron: " + std::to_string(cost.iron), ScreenWidth - 410, 150, *consolasFont, 1);
		drawText2D("Stone: " + std::to_string(cost.stone), ScreenWidth - 410, 135, *consolasFont, 1);
		drawText2D("Gold: " + std::to_string(cost.gold), ScreenWidth - 410, 120, *consolasFont, 1);
	}

}

PRIV void getBuildMenuItemCoords(int indexX, int indexY, int& x, int& y) {
	x = indexX * 65 + ScreenWidth - 220;
	y = 260 - indexY * 50;

}

PRIV void renderTopStatusBar() {
	renderColoredRect({ScreenWidth/2, ScreenHeight - 24, ScreenWidth, 48 }, { 0.6, 0.3, 0.1 }, -3);
	Image image((*getTextureCache())["ui-atlas"], 32, 32);
	drawImage2D({ 550, 730, 32, 32 }, image, { 512, 512, 12*32, 15 * 32, 32, 32 });
	drawImage2D({ 700, 730, 32, 32 }, image, { 512, 512, 13 * 32, 15 * 32, 32, 32 });
	drawImage2D({ 850, 730, 32, 32 }, image, { 512, 512, 14 * 32, 15 * 32, 32, 32 });
}


PRIV void renderBuildingMenus() {

	int buttonX;
	int buttonY;

	getBuildMenuItemCoords(0, 0, buttonX, buttonY);
	renderBuildingMenuItem(buttonX, buttonY, BuildingType::House);

	getBuildMenuItemCoords(1, 0, buttonX, buttonY);
	renderBuildingMenuItem(buttonX, buttonY, BuildingType::Barrack);

	getBuildMenuItemCoords(2, 0, buttonX, buttonY);
	renderBuildingMenuItem(buttonX, buttonY, BuildingType::Mine);

	getBuildMenuItemCoords(0, 1, buttonX, buttonY);
	renderBuildingMenuItem(buttonX, buttonY, BuildingType::Woodcamp);

}

PRIV void renderInGameHud() {

	if (doButton("btnBuild", "ui-atlas", ScreenWidth - 200, 10, 3 * 64, 1 * 64, { 512, 512, 0, 6 * 64, 3 * 64, 1 * 64 })) {
		showBuildMenu = !showBuildMenu;
	}

	if (showBuildMenu) {

		renderBuildingMenus();
	}

	renderTopStatusBar();
	

	
}

PRIV void render2D(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	if (gameState == GameState::GS_Splash) {
		renderEngineSplash(frameTimeGross);
	}

	if (gameState == GameState::GS_Menu) {
		renderMainMenu();
	}

	if (gameState == GameState::GS_InGame) {
	//	renderPickMap();
		renderInGameHud();
	}

	renderDebugInfo(frameTimeGross, frameTimeNet);

}

PRIV void render3D() {
	glm::vec3 pos = activeMainCam->position() +  glm::vec3(0, camPanY, 0) + activeMainCam->right() * camPanX + 
		glm::vec3(activeMainCam->fwd().x, 0, activeMainCam->fwd().z) * camPanZ + activeMainCam->fwd() * camMoveFwd;
	
	// Pitch
	glm::mat4 rotMatPitch = glm::rotate(glm::mat4(1), cameraPitch, glm::vec3(activeMainCam->right()));
	glm::vec3 fwd = glm::vec3(rotMatPitch * glm::vec4(activeMainCam->fwd(), 1.0));

	// Yaw
	glm::mat4 rotMat = glm::rotate(glm::mat4(1), cameraYaw, { 0, 1, 0 } /*glm::vec3(activeMainCam->up())*/);
	fwd = glm::vec3(rotMat * glm::vec4(fwd, 1.0));
	glm::vec3 right = glm::vec3(rotMat * glm::vec4(activeMainCam->right(), 1.0));

	activeMainCam->updateTransform(pos, fwd, right);

	// Render our shadow map
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFbo);
	glViewport(0, 0, 1024, 1024);
	GLfloat depthClear = 1;
	glClearBufferfv(GL_DEPTH, 0, &depthClear);
	for (auto gameObject : *gameObjects) {
		gameObject->render(shadowMapCamera, shadowMapCamera, "shadowmap", glm::vec3(0), false);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, pickMapFbo);
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	GLfloat c[] = { 0, 0, 0, 1 };
	glClearBufferfv(GL_COLOR, 0, c);
	GLfloat d = 1;
	glClearBufferfv(GL_DEPTH, 0, &d);
	for (auto gameObject : *gameObjects) {
		float val = (gameObject->id() / 255.0f);
		gameObject->renderWithColorUnlit(activeMainCam, glm::vec3(val, val, val));
	}

	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.05, 0.01, 0.01);
	
	for (auto gameObject : *gameObjects) {
		gameObject->render(activeMainCam, shadowMapCamera, getShaderCache()->get("default"), sunDirection, glm::vec3(0), false);
	}

	if (dragState != DS_None) {
		dummyDragGameObject->setPosition(intersectionPoint);
		dummyDragGameObject->
			//renderWithColorUnlit(activeMainCam, { 0.3, 1.0, 0.4 });
		render(activeMainCam, shadowMapCamera, getShaderCache()->get("default"), sunDirection, glm::vec3(0), false, true);
	}
	
}

PRIV void render(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.07, 0.03, 0.03);
	if (gameState == GameState::GS_InGame) {
		render3D();
	}
	render2D(frameTimeGross, frameTimeNet);
}

void mathTests() {
	glm::vec3 planeNormal(0, 1, 0);
	glm::vec3 planePoint(100, 0, -50);

	glm::vec3 rayOrigin = { -5, 10, 2 };
	Ray ray = { {rayOrigin.x , rayOrigin.y, rayOrigin.z}, { 0, -1,  0 }, 100.0f };

	glm::vec3 ip;
	if (!rayIntersectsPlane(planeNormal, planePoint, ray, ip)) {
		printf("rayintersection test failed!\n");
		exit(1);
	}

	if (ip.x != rayOrigin.x || ip.y != 0 || ip.z != rayOrigin.z) {
		printf("ray intersection point wrong: %f/%f/%f\n", ip.x, ip.y, ip.z);
		exit(1);
	}

	ray.direction = { 0, 0, -1 };
	if (rayIntersectsPlane(planeNormal, planePoint, ray, ip)) {
		printf("rayintersection test failed!\n");
		exit(1);
	}
}

int main__(int argc, char** args) {
	mathTests();

	createWindow(ScreenWidth, ScreenHeight, false);

	init();

	SDL_Event event;
	
	std::vector<SDL_Event> frameEvents;

	gameState = GameState::GS_Splash;

	Uint64 frameTimeGross = 0;
	Uint64 frameTimeNet = 0;
	Uint64 startCounterGross = 0;
	while (gameRunning) {
		Uint64 startCounterNet = SDL_GetPerformanceCounter();
		frameEvents.clear();
		SDL_Event event;

		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				gameRunning = false;
				break;
			}


			if (event.type == SDL_KEYDOWN) {
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					gameRunning = false;
					break;
				}
			}

			frameEvents.push_back(event);
		}

		updateUIState(frameEvents);
		update(frameTimeGross, frameEvents);
		Uint64 renderTimeStart = SDL_GetPerformanceCounter();
		render(frameTimeGross, frameTimeNet);
		Uint64 renderTimeMicros = ((double)(SDL_GetPerformanceCounter() - renderTimeStart) / SDL_GetPerformanceFrequency()) * 1000 * 1000;
		frameTimeNet = ((double)(SDL_GetPerformanceCounter() - startCounterNet) / SDL_GetPerformanceFrequency()) * 1000 * 1000;


		flipBuffer();
		frames++;
		frameTimeGross = ((double)(SDL_GetPerformanceCounter() - startCounterGross) / SDL_GetPerformanceFrequency()) * 1000 * 1000;
		startCounterGross = SDL_GetPerformanceCounter();
		
	}

	return 0;
}


