#include "../include/graphics.h"
#include "stdio.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "math.h"
#include <vector>
#include "game_object.h"
#include "texture_cache.h"
#include "camera.h"
#include "ImageImporter.h"
#include "Font.h"
#include "gui.h"
#include <set>
#include "game_lib.h"
#include "scene_management.h"
#include "sound.h"
#include <tgmath.h>
#include <algorithm>

#define PRIV static
#define MeshCache (*getMeshCache())




PRIV int ScreenWidth = 1024;
PRIV int ScreenHeight = 768;

PRIV SoundMachine* soundMachine = nullptr;
PRIV Sound* shotSound = nullptr;
PRIV Sound* deepSound = nullptr;
PRIV Font* notoFont = nullptr;
PRIV Font* consolasFont = nullptr;
PRIV ShaderCache* shaderCache = nullptr;
PRIV Camera* topDownCamera = nullptr;
PRIV bool debugMode = false;
PRIV int frames = 0;

PRIV bool showDebugGui() {
	return true;
}



PRIV void initShaders() {
	GLuint vs = createVertexShader("../test_shaders/vs.glsl");
	GLuint fs = createFragmentShader("../test_shaders/fs.glsl");
	GLuint shaderProg = createShaderProgram(vs, fs);

	GLuint shadowmap_vs = createVertexShader("../test_shaders/shadowmap-vs.glsl");
	GLuint shadowmap_fs = createFragmentShader("../test_shaders/shadowmap-fs.glsl");
	GLuint shadowMapshaderProg = createShaderProgram(shadowmap_vs, shadowmap_fs);

	GLuint fsText = createFragmentShader("../test_shaders/fs-text.glsl");
	GLuint textRenderShaderProg = createShaderProgram(vs, fsText);

	shaderCache = getShaderCache();
	shaderCache->put("default", shaderProg);
	shaderCache->put("shadowmap", shadowMapshaderProg);
	shaderCache->put("text", textRenderShaderProg);
}

PRIV void initCameras() {
	glm::mat4 vTopDown = glm::lookAt(glm::vec3(0, 30, 0), glm::vec3(0, 2, 0), glm::vec3(0, 1, 0));
	glm::mat4 pTopDown = glm::perspective(1.04f, (float)(ScreenWidth / ScreenHeight), 0.01f, 300.0f);

	glm::mat4 vDebugCam = glm::lookAt(glm::vec3(0, 1.8f, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 pDebugCam = glm::perspective(1.04f, (float)(ScreenWidth / ScreenHeight), 0.1f, 300.0f);

	topDownCamera = new Camera(glm::vec3(0, 35, 0), glm::vec3(0, -1, 0), { 1, 0,0 }, pTopDown);
}

PRIV void initTextures() {
	GLuint stoneTexture = createTexture("../test_assets/generic_texture.bmp");
	(*getTextureCache())["stone"] = stoneTexture;

	GLuint redTexture = createTexture("../test_assets/red.bmp");
	(*getTextureCache())["red"] = redTexture;

	GLuint blueTexture = createTexture("../test_assets/blue.bmp");
	(*getTextureCache())["blue"] = blueTexture;

	ImageData* notoSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/noto.png", notoSurface);
	GLuint notoTexture = createTextureFromSurface(notoSurface, true);
	(*getTextureCache())["noto-font"] = notoTexture;
	notoFont = new Font("noto", "noto-font");
	delete(notoSurface->pixels);
	delete(notoSurface);

	ImageData* consolasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/consolas.png", consolasSurface);
	GLuint consolasTexture = createTextureFromSurface(consolasSurface, true);
	(*getTextureCache())["consolas-font"] = consolasTexture;
	consolasFont = new Font("consolas", "consolas-font");
	delete(consolasSurface->pixels);
	delete(consolasSurface);

	ImageData* spriteSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/sprite.png", spriteSurface);
	GLuint sprite = createTextureFromSurface(spriteSurface, true);
	(*getTextureCache())["sprite"] = sprite;
	delete(spriteSurface->pixels);
	delete(spriteSurface);

	ImageData* engineSplashSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/piglet-engine-splash.png", engineSplashSurf);
	GLuint engineSplash = createTextureFromSurface(engineSplashSurf, true);
	(*getTextureCache())["engine-splash"] = engineSplash;
	delete(engineSplashSurf->pixels);
	delete(engineSplashSurf);

	ImageData* mainMenuSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/drone_wars_title.png", mainMenuSurface);
	GLuint mainMenuTexture = createTextureFromSurface(mainMenuSurface, true);
	(*getTextureCache())["title"] = mainMenuTexture;
	delete(mainMenuSurface->pixels);
	delete(mainMenuSurface);

	ImageData* uiAtlasSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/ui-sprite-atlas.png", uiAtlasSurface);
	GLuint uiAtlasTexture = createTextureFromSurface(uiAtlasSurface, true);
	(*getTextureCache())["ui-atlas"] = uiAtlasTexture;
	delete(uiAtlasSurface->pixels);
	delete(uiAtlasSurface);

	ImageData* lpPalSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/lp-palette.png", lpPalSurface);
	GLuint lpPalTexture = createTextureFromSurface(lpPalSurface, true);
	(*getTextureCache())["lp-palette"] = lpPalTexture;
	delete(lpPalSurface->pixels);
	delete(lpPalSurface);


	ImageData* testFigureSurface = new ImageData();
	ImageImporter::importPNGFile("../test_assets/figure_diffuse.png", testFigureSurface);
	GLuint fdTexture = createTextureFromSurface(testFigureSurface, true);
	(*getTextureCache())["testfigure-diffuse"] = fdTexture;
	delete(testFigureSurface->pixels);
	delete(testFigureSurface);

	ImageData* greenSemiSurf = new ImageData();
	ImageImporter::importPNGFile("../test_assets/green_semitransparent.png", greenSemiSurf);
	GLuint greenSemiTexture = createTextureFromSurface(greenSemiSurf, true);
	(*getTextureCache())["green-semitransparent"] = greenSemiTexture;
	delete(greenSemiSurf->pixels);
	delete(greenSemiSurf);

}



PRIV void init() {
	initTextures();
	initShaders();
	initCameras();

}


uint8_t reg_acc = 0;
uint8_t reg_x = 0;
uint8_t reg_y = 0;
bool flag_carry = false;

int cycles = 0;
uint8_t* ram;
uint16_t pc = 0;
uint8_t data_bus = 0;
uint16_t address_bus = 0;
uint8_t current_opcode = 0;
uint8_t constant_low = 0;
uint8_t constant_hi = 0;
int current_op_cycles = 0;
int current_op_cycles_done = 0;

uint8_t cmd_ADC_Imm() {
	current_op_cycles_done++;
	switch (current_op_cycles_done) {
	case 1: 
		return 2;
		case 2: 
		{
		
			pc++; constant_low = ram[pc]; address_bus = pc; data_bus = constant_low;
			if ((reg_acc + constant_low) > 255) {
				flag_carry = 1;
			}
			reg_acc += constant_low;
			pc++;
			current_op_cycles_done = 0;
			return 0;
		}
		  break;
	}
}

uint8_t get_op_cycles() {
	switch (current_opcode) {
	case 0x69: return cmd_ADC_Imm();
	}
}

uint8_t fetchOpcode() {
	
	current_opcode = ram[pc];
	current_op_cycles = get_op_cycles();
	return current_opcode;
}

void doCpuCycle() {
	cycles++;
	uint8_t opcode = 0;
	if (current_op_cycles_done < current_op_cycles) {
		opcode = current_opcode;
	}
	else {
		opcode = fetchOpcode();
		current_opcode = opcode;
		return;
	}
	
	
	switch (opcode) {
	case (0x69): current_op_cycles = cmd_ADC_Imm(); break;
	default:/* printf("ERROR: unknown command: %x\n", opcode); */break;
	}
}

float gameTime = 0;
int cps_count = 0;
int last_cps_count = 0;
float second_counter = 0;
Timer vm_frame_time;

PRIV void doVicCycle() {

}



PRIV void update(float dt, std::vector<SDL_Event>& frameEvents) {
	vm_frame_time.start();
	for (auto fe : frameEvents) {
		if (fe.type == SDL_KEYDOWN) {
			// if F10 is pressed, we toggle the debug mode

			// Not used now: listen for CTRL-0
			// using the key modifiers.
			//SDL_Keymod mod = SDL_GetModState();
			//if (mod & KMOD_RCTRL && fe.key.keysym.sym == SDLK_0) {
			if (fe.key.keysym.sym == SDLK_F10) {
				debugMode = !debugMode;
				//activeMainCam = debugCamera;
			}
		}

		if (fe.type == SDL_MOUSEBUTTONDOWN) {
			if (fe.button.button == SDL_BUTTON_LEFT) {
			
			}
			else if (fe.button.button == SDL_BUTTON_RIGHT) {
				
			}

		}
	}

	// Do all the cycles of a frame
	// ~986000 cycles
	for (int i = 0; i < 19720; i++) {
		doCpuCycle();
		doVicCycle();
	}
	uint32_t frametime = vm_frame_time.stop();

	float wait = (18000 - frametime) / 1000;
	printf("ft: %u wait: %f \n", frametime, wait * 1000);
	if (wait > 0) {
		Sleep(wait);
	}

	// Wait for end of frame, 50Hz for PAL compatible screen.
	
}



PRIV double splashTimePassed = 0;
PRIV double splashTimeDuration = 2;
PRIV float splashAlpha = 1;
PRIV void renderEngineSplash(Uint64 frameTimeGross) {
	// We just wait some frames to warm up
	if (frames < 10) {
		return;
	}
	double ftInSeconds = (double)frameTimeGross / 1000 / 1000;

	(double)splashTimePassed += ftInSeconds;
	if (splashTimePassed >= splashTimeDuration) {
		return;
	}

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(400, 300, -1));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(800, 600, 1));
	splashAlpha -= 0.2 * ftInSeconds;
	ri.alpha = splashAlpha;

	drawVAO(&ri);

}

PRIV void renderMainMenu() {

	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["title"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(512, 384, -2));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(1024, 768, 1));
	ri.alpha = 1;

	drawVAO(&ri);

	if (doButton("btnStart", "ui-atlas", 300, 250, 3 * 64, 1 * 64, { 512, 512, 0, 7 * 64, 3 * 64, 1 * 64 })) {
	
	}

	if (doButton("btnExit", "ui-atlas", 300, 180, 3 * 64, 1 * 64, { 512, 512, 3 * 64, 7 * 64, 3 * 64, 1 * 64 }) == true) {

	}

}

PRIV void renderPickMap() {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);

	// For this test we use a texture called offsreen
	// which is an in memory texture used as a render target for 
	// offscreen rendering, hence the name :) 
	ri.texture = (*getTextureCache())["pickMapCol"];
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(100, 50, 1.5f));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(300, 200, 1));

	ri.lit = false;
	ri.alpha = 1;
	drawVAO(&ri);
}


PRIV void renderDebugInfo(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	if (showDebugGui()) {

		// frame statistics
		drawText2D("ft gross:" + std::to_string((double)frameTimeGross), 5, ScreenHeight - 30, *consolasFont, 1, { 0, 0.7, 0 });
		drawText2D("ft net:" + std::to_string((double)frameTimeNet), 5, ScreenHeight - 15, *consolasFont, 1);

		// draw mouse coords
		int mx, my;
		mouseXY(mx, my);
		drawText2D(std::to_string(mx) + "/" + std::to_string(my), 5, ScreenHeight - 60, *consolasFont, 1);

		// selected game object
		//std::string goName = lastSelectedGameObject != nullptr ? lastSelectedGameObject->name() : " -- none --";
		drawText2D("Sel.: --", 5, ScreenHeight - 75, *consolasFont, 1);


	}
}


PRIV void renderColoredRect(SDL_Rect dest, glm::vec3 color, float zOrder = -1) {
	RenderInfo ri;
	ri.vaodata = getUnitQuadVAO();
	ri.shader = shaderCache->get("default");
	ri.nrOfVertices = 6;
	ri.shadowMapTexture = 0;
	ri.projectionMatrixLight = glm::mat4(1);
	ri.viewMatrixLight = glm::mat4(1);
	ri.lit = false;
	ri.viewMatrix = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	ri.projectionMatrix = glm::ortho<float>(0, ScreenWidth, 0, ScreenHeight, .01, 100);
	ri.texture = (*getTextureCache())["engine-splash"];
	ri.fixColored = true;
	ri.modelMatrix = glm::mat4(1);
	ri.modelMatrix = glm::translate(ri.modelMatrix, glm::vec3(dest.x, dest.y, zOrder));
	ri.modelMatrix = glm::scale(ri.modelMatrix, glm::vec3(dest.w, dest.h, 1));
	ri.alpha = 1;
	ri.tint = color;

	drawVAO(&ri);

}


PRIV void renderTopStatusBar() {
	renderColoredRect({ ScreenWidth / 2, ScreenHeight - 24, ScreenWidth, 48 }, { 0.6, 0.3, 0.1 }, -3);
	Image image((*getTextureCache())["ui-atlas"], 32, 32);
	drawImage2D({ 550, 730, 32, 32 }, image, { 512, 512, 12 * 32, 15 * 32, 32, 32 });
	drawImage2D({ 700, 730, 32, 32 }, image, { 512, 512, 13 * 32, 15 * 32, 32, 32 });
	drawImage2D({ 850, 730, 32, 32 }, image, { 512, 512, 14 * 32, 15 * 32, 32, 32 });
}

PRIV void renderVMInfo() {
	renderColoredRect({ ScreenWidth / 2, ScreenHeight - 24, ScreenWidth, 48 }, { 0.6, 0.3, 0.1 }, -3);
	drawText2D("cycles:" + std::to_string(cycles), 5, ScreenHeight - 235, *consolasFont, 1);
	drawText2D("cycles/second:" + std::to_string(last_cps_count), 5, ScreenHeight - 265, *consolasFont, 1);
}

PRIV void render2D(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	renderVMInfo();
	renderDebugInfo(frameTimeGross, frameTimeNet);
}

PRIV void render(Uint64 frameTimeGross, Uint64 frameTimeNet) {
	glViewport(0, 0, ScreenWidth, ScreenHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	clearBackBuffer(0.07, 0.03, 0.03);
	render2D(frameTimeGross, frameTimeNet);
}

void writeProgram() {
	ram[0] = 0x69;
	ram[1] = 255;
	ram[2] = 0x69;
	ram[3] = 4;
}


int main_vm(int argc, char** args) {

	createWindow(ScreenWidth, ScreenHeight, false, false);
	HWND hwnd = getNativeWindow();
	soundMachine = new SoundMachine(hwnd);
	shotSound = soundMachine->createDummySound(0.15, 440);
	deepSound = soundMachine->createDummySound(0.30, 110);
	soundMachine->playSound(deepSound);

	init();

	SDL_Event event;

	std::vector<SDL_Event> frameEvents;

	Timer timer_gross;
	Timer timer_net;

	ram =(uint8_t*) malloc(0xFFFFF);
	writeProgram();

	timer_gross.start();
	bool vmRunning = true;
	
	while (vmRunning) {
		timer_net.start();
		frameEvents.clear();
		SDL_Event event;

		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				vmRunning = false;
				break;
			}

			if (event.type == SDL_KEYDOWN) {
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					vmRunning = false;
					break;
				}
			}

			frameEvents.push_back(event);
		}

		updateUIState(frameEvents);
		update(timer_gross.get_last_measure(microseconds), frameEvents);
		render(timer_gross.get_last_measure(microseconds), timer_net.get_last_measure(microseconds));
		timer_net.stop();

		flipBuffer();
		frames++;
		timer_gross.stop();
		timer_gross.start();

	}

	return 0;
}


