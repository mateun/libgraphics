#include "math.h"
#include <stdio.h>
#include "camera.h"



// This function produces a ray from the camera into the
// world, through the screen coordinates.
Ray createRayFromScreenCoordinates(int sx, int sy, Camera* camera, int ScreenWidth, int ScreenHeight) {
	float x = (2.0f * sx) / ScreenWidth - 1.0f;
	float y = (2.0f * sy) / ScreenHeight - 1.0f;
	float z = 1.0f;
	glm::vec3 ray_nds = glm::vec3(x, y, z);
	glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
	glm::vec4 ray_eye = inverse(camera->proj()) * ray_clip;
	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
	glm::vec3 ray_wor = inverse(camera->view()) * ray_eye;
	ray_wor = normalize(ray_wor);
	return { camera->position(), ray_wor, 100 };

}

bool rayIntersectsPlane(glm::vec3 planeNormal, glm::vec3 planePoint, Ray ray, glm::vec3& intersectionPoint) {

	float rayPlaneOrientation = glm::dot(ray.direction, planeNormal);	
	if (abs(rayPlaneOrientation) <=  0.0001f) {
		printf("ray plane orientation too small. no collision! %f\n", rayPlaneOrientation);
		return false;
	}

	float t = (glm::dot((planePoint - ray.origin), planeNormal))/rayPlaneOrientation;
	intersectionPoint = ray.origin + t * ray.direction;
	//printf("hitpoint: %f,%f,%f\n", intersectionPoint.x, intersectionPoint.y, intersectionPoint.z);

	return t > 0;
}
