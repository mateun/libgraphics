//
// Created by martin on 20.09.20.
//

#ifndef GRAPHICSLIB_IMAGE_H
#define GRAPHICSLIB_IMAGE_H
#include "render.h"

//
// This struct represents a 2D image which can be
// rendered on the screen by defining its 2D screen coordinates
// x and y.
// Images also have a layer which allows to order Images by depth.
// Lower layers are in front of higher layers, e.g. 1 is in front of 5.

struct Image {
public:
    Image(GLuint textureId, int w, int h);
    Image(const std::string& fileName, int width, int height);

    GLuint _texture;
    int _widthInPixels;
    int _heightInPixels;

};

#endif //GRAPHICSLIB_IMAGE_H
