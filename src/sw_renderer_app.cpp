#include <SDL2/SDL.h>
#include <cmath>
#include <string.h>
#include "game_lib.h"

// Globals
// Screen and backbuffer dimensions
int win_w = 1024;
int win_h = 768;
int bb_w = 800;
int bb_h = 600;

void setPixel(int x, int y, uint8_t r, uint8_t g, uint8_t b, uint8_t* bb, int pitch) {
	int pixelsPerRow = pitch / 1;
	bb[x * 4 + y * pixelsPerRow] = 255;
	bb[x * 4 + y * pixelsPerRow + 1] = b;
	bb[x * 4 + y * pixelsPerRow + 2] = g;
	bb[x * 4 + y * pixelsPerRow + 3] = r;
}

void drawLine(int x1, int y1, int x2, int y2, uint8_t* backBufferPtr, long pitch) {
	int dx = x2 - x1;
	int dy = y2 - y1;

	int dirX = dx < 0 ? -1 : 1;
	int dirY = dy < 0 ? -1 : 1;

	float d = (float)dy / (float)dx;

	float x = x1;
	float y = y1;

	if (abs(d) > 1) {
		// y dominant
		d = (float)dx / (float)dy;
		while (y != y2) {
			setPixel(x, y, 25, 230, 23, backBufferPtr, pitch);
			if (dx < 0) {
				x = x - abs(d);
			}
			else {
				x = x + abs(d);
			}

			y = y + dirY;
		}
	}
	else
	{
		// x dominant
		while (x != x2) {
			setPixel(x, y, 25, 230, 23, backBufferPtr, pitch);
			if (dy < 0) {
				y = y - abs(d);
			}
			else {
				y = y + abs(d);
			}
			x = x + dirX;

		}
	}

}

void rasterizeTriangle(Vec3 p1, Vec3 p2, Vec3 p3, uint8_t* surfacePointer, long pitch, uint8_t r, uint8_t g, uint8_t b) {
	// find highest point
	Vec3 points[3];
	points[0] = p1;
	points[1] = p2;
	points[2] = p3;

	if (p2.y < p1.y) {
		points[0] = p2;
		points[1] = p1;
	}

	if (p3.y < p2.y) {
		points[0] = p3;
		points[1] = p2;
		points[2] = p1;
	}


	Vec3 leftSide = points[1] - points[0];
	Vec3 rightSide = points[2] - points[0];

	int dxLeft = points[1].x - points[0].x;
	int dyLeft = points[1].y - points[0].y;

	// right side
}


void drawTriangle(Vec3 p1, Vec3 p2, Vec3 p3, uint8_t* surfacePointer, long pitch) {
	drawLine(p1.x, p1.y, p2.x, p2.y, surfacePointer, pitch);
	drawLine(p1.x, p1.y, p3.x, p3.y, surfacePointer, pitch);
	drawLine(p2.x, p2.y, p3.x, p3.y, surfacePointer, pitch);
	rasterizeTriangle(p1, p2, p3, surfacePointer, pitch, 255, 0, 0);
}


double PI = 3.1415926535897932384626433;

// rename to main to start up
int main_sw_renderer(int argc, char** args) {

	SDL_Init(SDL_INIT_VIDEO);


	SDL_Window* window = SDL_CreateWindow("koenig swr 0.0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, win_w, win_h, 0);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);
	SDL_RenderSetLogicalSize(renderer, bb_w, bb_h);
	//SDL_RenderSetIntegerScale(renderer, SDL_TRUE);
	SDL_Texture* screen_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, bb_w, bb_h);

	uint32_t * pixels = (uint32_t*) malloc(bb_w * bb_h * 4);
	uint8_t* pixels8 = (uint8_t*)pixels;
	int pitch = bb_w * 4;

	float moveH = 0;

	Uint64 perf_freq = SDL_GetPerformanceFrequency();

	while (1) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) exit(0);
		}

		Uint64 start = SDL_GetPerformanceCounter();

		uint32_t* row = pixels;
		for (int y = 0; y < bb_h; y++) {
			//for (int x = 0; x < bb_w; x++) {
				//pixels[x + y * bb_w] = 0x090302ff;
				memset(row, 0, pitch);
				row = row + bb_w;

			//}
		}

		drawLine(10, 10, 100, 10, pixels8, pitch);

		drawLine(10, 10, 100, 200, pixels8, pitch);
		drawLine(100, 200, 100, 10, pixels8, pitch);

		moveH += 0.06;
		Vec3 projDummy1 = { -0.1, 0.1, -0.0 };
		Vec3 projDummy2 = { 0.1 , 0.1, -0.0 };
		Vec3 projDummy3 = { 0, -0.1, -0.0 };

		Vec3 vr1 = rotateAroundY(projDummy1, moveH);
		Vec3 vr2 = rotateAroundY(projDummy2, moveH);
		Vec3 vr3 = rotateAroundY(projDummy3, moveH);

		vr1 = vr1 + Vec3{ 0, 0, -1};
		vr2 = vr2 + Vec3{ 0, 0, -1};
		vr3 = vr3 + Vec3{ 0, 0, -1};

		vr1 = perspectiveProject(vr1, (90 * PI / 180), 1.33, 1);
		vr2 = perspectiveProject(vr2, (90 * PI / 180), 1.33, 1);
		vr3 = perspectiveProject(vr3, (90 * PI / 180), 1.33, 1);

		Vec3 s1 = toScreenCoordinates(vr1, bb_w, bb_h);
		Vec3 s2 = toScreenCoordinates(vr2, bb_w, bb_h);
		Vec3 s3 = toScreenCoordinates(vr3, bb_w, bb_h);

		drawTriangle(s1, s2, s3, pixels8, pitch);

		setPixel(400, 300, 255, 0, 0, pixels8, pitch);
		
		SDL_UpdateTexture(screen_texture, NULL, pixels, pitch);

		SDL_RenderCopy(renderer, screen_texture, NULL, NULL);

		Uint64 end = SDL_GetPerformanceCounter();
		float diff = ((float)(end - start) / (float)perf_freq) * 1000 * 1000;
		SDL_Log("diff in micros: %f", diff);

		SDL_RenderPresent(renderer);

		
	}

	return 0;
}