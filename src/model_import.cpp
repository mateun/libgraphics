#include "model_import.h"
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <GL/glew.h>
#include <vector>
#include "render.h"
#include "mesh.h"

AABBNoHeight calculateAABB(const std::vector<float>positions) {
	// The most left vertex
	float leftX = 0;
	for (int i = 0; i < positions.size(); i+=3) {
		float x = positions[i];
		if (x < leftX) {
			leftX = x;
		}
	}

	// The rightmost vertex x value
	float rightX = 0;
	for (int i = 0; i < positions.size(); i += 3) {
		float x = positions[i];
		if (x > rightX) {
			rightX = x;
		}
	}

	// The topmost Z value
	// is the most negative one.
	float topZ = 0;
	for (int i = 0; i < positions.size(); i += 3) {
		float z = positions[i+2];
		if (z < topZ) {
			topZ = z;
		}
	}

	// The bottom Z is the most positive Z value.
	float bottomZ = 0;
	for (int i = 0; i < positions.size(); i += 3) {
		float z = positions[i + 2];
		if (z > bottomZ) {
			bottomZ = z;
		}
	}


	// todo(gru) implement!!
	AABBNoHeight aabb;
	aabb.left = leftX;
	aabb.right = rightX;
	aabb.top = topZ;
	aabb.bottom = bottomZ;

	return aabb;
}

Mesh* importToVAO(const std::string& modelName) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(modelName, 
		aiProcess_Triangulate
	//	| aiProcess_FlipUVs
	);

	if (scene == nullptr) {
		printf("error in import of file %s\n", modelName.c_str());
		exit(1);
	}

	std::vector<float> positions;
	std::vector<float> uvs;
	std::vector<float> normals;
	std::vector<int> indices;

	auto* mesh = scene->mMeshes[0];

	for (int x = 0; x < mesh->mNumVertices;x++)
	{
		aiVector3D pos = mesh->mVertices[x];
		positions.push_back(pos.x);
		positions.push_back(pos.y);
		positions.push_back(pos.z);

		if (mesh->HasNormals())
		{
			aiVector3D normal = mesh->mNormals[x];
			normals.push_back(normal.x);
			normals.push_back(normal.y);
			normals.push_back(normal.z);
		}

		if (mesh->HasTextureCoords(0))
		{
			aiVector3D uv = mesh->mTextureCoords[0][x];
			uvs.push_back(uv.x);
			uvs.push_back(uv.y);
		}
	}

	for (int f = 0; f < mesh->mNumFaces; f++)
	{
		aiFace face = mesh->mFaces[f];
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}
			

	// Material and texture import
	/*aiMaterial* aimat = scene->mMaterials[mesh->mMaterialIndex];
	ggraphics::Material* material = new ggraphics::Material();
	material->name = std::string(aimat->GetName().data);
	aiString texturePath;
	std::string basePath = getModelBasePath(fileName);
	aiReturn ret = aimat->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &texturePath, NULL, NULL, NULL, NULL, NULL);
	material->texture = new ggraphics::Texture(std::string(basePath + "/" + texturePath.C_Str()), vec3(0));
	myMesh.material = material;

	model.meshes.push_back(myMesh);*/

	Mesh* rmesh = new Mesh();
	rmesh->numberOfVertices = mesh->mNumVertices;
	rmesh->vao = createVAO(positions, uvs, normals, indices).vao;
	rmesh->aabb = calculateAABB(positions);
	return rmesh;


}


