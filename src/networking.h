#pragma once
#include <SDL_net.h>
#include <functional>

enum NetworkRole {
	NoRole,
	ListenServer,
	StandaloneServer,
	Client,

};

enum NetworkState {
	NoState,
	JoiningGame,
	JoinedGame,
};

struct UDPNode {
	int port;
	UDPsocket udpSocket;

};

struct UDPMessage {
	uint8_t* data;
	IPaddress senderReceiver;
	int len;
};

void startNetworking();

UDPNode* startUDPNode(int port=0);

bool udpPoll(UDPNode* server, UDPMessage& msg);
void udpSend(UDPNode* sender, IPaddress target, uint8_t* data, int len);

void registerMessageListener(std::function< void(UDPMessage)> msgListener);