//
// Created by martin on 04.10.20.
//

#ifndef GRAPHICSLIB_IMAGEIMPORTER_H
#define GRAPHICSLIB_IMAGEIMPORTER_H


#include <string>
#include <SDL2/SDL_surface.h>

struct ImageData {
    int w;
    int h;
    uint8_t* pixels;
};

class ImageImporter {

public:
    static void importPNGFile(const std::string& fileName, ImageData* imageData);

};


#endif //GRAPHICSLIB_IMAGEIMPORTER_H
