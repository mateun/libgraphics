#include "mesh.h"
#include <map>
#include <string>


static std::map<std::string, Mesh*>* _meshCache;

std::map<std::string, Mesh*>* getMeshCache() {
	if (!_meshCache) {
		_meshCache = new std::map<std::string, Mesh*>();
	}

	return _meshCache;
}
