#include "scene_management.h"
#include "game_object.h"


QTNode::QTNode(float w, float h, float x, float y) : QTNode(w, h, x, y, nullptr){

}


QTNode::QTNode(float w, float h, float x, float y, GameObject* go) : _w(w), _h(h), _x(x), _y(y) {
	_gameObject = go;
}




QuadTree::QuadTree(float w, float h, float x, float y, std::vector<GameObject*> initialGameObjects) {
	_root = new QTNode(w, h, x, y);

	// Go through all gameobjects and for each one create the correct
	// leaf node to reside in.
	// todo(gru): do this in the recompute function?

	for (auto go : initialGameObjects) {
		if (go->collidable()) {
			addGameObject(go, _root);
		}
	}
}

bool doesObjectFitIntoNode(GameObject* go, QTNode* node) {
	return go->position().x >= node->x() &&
		go->position().x <= (node->x() + node->w()) &&
		go->position().z <= node->y() &&
		go->position().z >= (node->y() - node->h());
}

bool QTNode::doesObjectFitIntoNode(GameObject* go) {
	return go->position().x >= this->x() &&
		go->position().x <= (this->x() + this->w()) &&
		go->position().z <= this->y() &&
		go->position().z >= (this->y() - this->h());
}


QTNode* createChildNode(QTNode* parent, ChildType childType) {
	QTNode* childNode;
	if (childType == TopLeft) {
		childNode = new QTNode(parent->w() / 2, parent->h() / 2, parent->x(), parent->y() - parent->h() / 2);
	}

	else if (childType == TopRight) {
		childNode = new QTNode(parent->w() / 2, parent->h() / 2, parent->x() + parent->w() / 2, parent->y() - parent->h() / 2);
	}

	else if (childType == BottomLeft) {
		childNode = new QTNode(parent->w() / 2, parent->h() / 2, parent->x(), parent->y());
	}


	else if (childType == BottomRight) {
		childNode = new QTNode(parent->w() / 2, parent->h() / 2, parent->x() + parent->w() / 2, parent->y());
	}
	
	parent->children().push_back(childNode);
	childNode->setParent(parent);

	return childNode;
}


void QuadTree::addGameObject(GameObject* go, QTNode* currentNode) {

	
		if (!currentNode->hasGameObject() && currentNode->children().empty()) {
			currentNode->setGameObject(go);
			
		}
		else {

			// We need to redistribute this old game object, 
			// save it for later.
			// In any case, we need to remove the existing gameObject from the 
			// current node, as this will no longer be valid. 
			GameObject* previousGameObject = currentNode->getGameObjec();
			currentNode->setGameObject(nullptr);


			if (currentNode->children().empty()) {

				
				// create the 4 child nodes
				// and fit the current and the previous game object in one of them
				QTNode* childNodeTL = createChildNode(currentNode, TopLeft);
				QTNode* childNodeTR = createChildNode(currentNode, TopRight);
				QTNode* childNodeBL = createChildNode(currentNode, BottomLeft);
				QTNode* childNodeBR = createChildNode(currentNode, BottomRight);

				if (doesObjectFitIntoNode(go, childNodeTL)) {
					childNodeTL->setGameObject(go);
					if (childNodeTL->doesObjectFitIntoNode(previousGameObject)) {
						addGameObject(previousGameObject, childNodeTL);
						return;
					}
				}

				if (childNodeTL->doesObjectFitIntoNode(previousGameObject)) {
					childNodeTL->setGameObject(previousGameObject);
				}

				
				if (childNodeTR->doesObjectFitIntoNode(go)) {
					childNodeTR->setGameObject(go);
					if (childNodeTR->doesObjectFitIntoNode(previousGameObject)) {
						addGameObject(previousGameObject, childNodeTR);
						return;
					}
				}
				if (childNodeTR->doesObjectFitIntoNode(previousGameObject)) {
					childNodeTR->setGameObject(previousGameObject);
				}

				
				if (childNodeBL->doesObjectFitIntoNode(go)) {
					childNodeBL->setGameObject(go);
					if (childNodeBL->doesObjectFitIntoNode(previousGameObject)) {
						addGameObject(previousGameObject, childNodeBL);
						return;
					}
				}
				if (childNodeBL->doesObjectFitIntoNode(previousGameObject)) {
					childNodeBL->setGameObject(previousGameObject);
				}

				
				if (childNodeBR->doesObjectFitIntoNode(go)) {
					childNodeBR->setGameObject(go);
					if (childNodeBR->doesObjectFitIntoNode(previousGameObject)) {
						addGameObject(previousGameObject, childNodeBR);
						return;
					}
				}
				if (childNodeBR->doesObjectFitIntoNode(previousGameObject)) {
					childNodeBR->setGameObject(previousGameObject);
				}
				
			} 
			else {
				for (auto n : currentNode->children()) {
					if (doesObjectFitIntoNode(go, n) && n->children().empty()) {
						n->setGameObject(go);
						break;
					} 
					else if (doesObjectFitIntoNode(go, n) && !n->children().empty()) {
						addGameObject(go, n);
					}
					
				}
			}
		}
		
}