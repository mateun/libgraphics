#pragma once
#include <string>
#include <vector>
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

// For text based uis, e.g. a textbox
struct TextData {
	int cursor;
	std::string* text;
};


// UI stuff
struct WidgetData {
	int x;
	int y;
	int w;
	int h;
	TextData* textData;
	std::string id;
	float depth;
};

class SpriteAtlasItemDescriptor;

bool isHovering(const std::string& id);
void updateUIState(std::vector<SDL_Event> frameEvents, uint64_t dt = 0);
void doText(const std::string id, std::string text, int x, int y, float depth = -1.0f, glm::vec3 color = { 1, 1, 1 });
void doTextInput(const std::string id, std::string* text, int x, int y, int w, int h, float depth = -1.0f);
bool doMenu(const std::string id, const std::string& text, int x, int y, int w, int h, float depth = -1.0f, glm::vec3 color = { 0.2, 0.2, 0.2 });
bool doButton(const std::string id, const std::string& texture, int x, int y, 
	int w, int h, SpriteAtlasItemDescriptor sad, 
	glm::vec3 normalColor = { 1, 1, 1 }, 
	glm::vec3 activeColor = { 0.8, 0.8, 0.8 }, 
	float depth = -1.0f);
void mouseXY(int& x, int& y);
