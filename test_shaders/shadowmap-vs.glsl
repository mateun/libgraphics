#version 430 core

layout (location = 0) in vec3 position;

layout(location = 3) uniform mat4 mat_model;
layout(location = 4) uniform mat4 mat_view;
layout(location = 5) uniform mat4 mat_proj;

void main() {
  gl_Position = mat_proj * mat_view * mat_model  * (vec4(position, 1));


}

