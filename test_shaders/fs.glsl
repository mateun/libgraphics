#version 430 core

layout(binding = 0) uniform sampler2D sampler;
layout(binding = 1) uniform sampler2D shadowMapSampler;
uniform bool isLit;
uniform bool isFixColored;
uniform float alpha;
uniform vec3 tint;
uniform vec3 sunDirection;

out vec4 color;
in vec2 fs_uvs;
in vec3 fs_normals;
in vec4 fs_frag_pos_light;

float calc_shadow_factor(vec4 fragpos) {
	vec3 fpos_ndc = fragpos.xyz / fragpos.w;
	fpos_ndc = fpos_ndc * 0.5 + 0.5;
	float depth_value = texture(shadowMapSampler, fpos_ndc.xy).r;
	if (depth_value + 0.0001 < fpos_ndc.z) {
		return 0.6;
	} else {
		return 1.0;
	}

}

// This is working render code 
// for textured and lit
void renderTextured() {
	vec3 lightVector = normalize(sunDirection * -1);
	float diffuse = max(dot(fs_normals, lightVector), 0.1);
	color = texture(sampler, fs_uvs);
	color += vec4(tint, 0);
	color *= alpha;

	if (isLit) {
		float shadowFactor = calc_shadow_factor(fs_frag_pos_light);
		color = color * diffuse * shadowFactor;
	}
	

}

void main() {

	if (isFixColored) {
		if (isLit) {
			vec3 lightVector = normalize(sunDirection * -1);
			float diffuse = max(dot(fs_normals, lightVector), 0.1);
			color = texture(sampler, fs_uvs);
			color += vec4(tint, 0);
			color *= alpha;
			color = color * diffuse;

		}
		else {
			color = vec4(tint, 1);
		}

	} else {
		renderTextured();
	}

	
}
