#version 430 core

layout(binding = 0) uniform sampler2D sampler;
layout(location = 1) uniform bool isLit;

out vec4 color;
in vec2 fs_uvs;
in vec3 fs_normals;


void main() {
	vec3 lightVector = normalize(vec3(.4, 0.7, .1));
	float diffuse = max(dot(fs_normals, lightVector), 0.1);
	color = texture(sampler, fs_uvs) ;

	if (isLit) {
		color = color * diffuse;
	}
		
	//color = vec4(0.1, 0.9, 0.2, 1) * diffuse;
	//color = vec4(fs_normals, 1);
	
	//color = vec4(0, 1, 1, 1);
}
